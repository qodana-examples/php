<?php
declare(strict_types=1);

namespace LessPlateTest\Integration\Render;

use LessPlate\Context\ArrayContext;
use LessPlate\Context\Context;
use LessPlate\DefaultRenderer;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Input\Input;
use LessPlate\Input\StringInput;
use LessPlate\Lexer\Exception\TokenizeError;
use LessPlate\Parser\Exception\ParseError;
use PHPUnit\Framework\TestCase;

/**
 * Test render setup
 */
final class RenderTest extends TestCase
{
    /**
     * Test render
     *
     * @param Input $input
     * @param Context $context
     * @param string $expected
     *
     * @return void
     *
     * @throws ExecuteError
     * @throws TokenizeError
     * @throws ParseError
     *
     * @dataProvider getRenderData
     */
    public function testRender(Input $input, Context $context, string $expected): void
    {
        $renderer = new DefaultRenderer();

        self::assertSame(
            rtrim($expected),
            $renderer->render($input, $context)
        );
    }

    /**
     * Returns data to use for render test
     *
     * @return iterable|mixed[]
     */
    public function getRenderData(): iterable
    {
        foreach ($this->getDirectories(__DIR__ . '/data') as $directory) {
            if (file_exists($directory . '/input.plate')) {
                $array = file_exists($directory . '/context.json')
                    ? json_decode(file_get_contents($directory . '/context.json'), true)
                    : [];

                $context = new ArrayContext($array);

                yield [
                    StringInput::withMd5Checksum(file_get_contents($directory . '/input.plate')),
                    $context,
                    file_get_contents($directory . '/output.plate'),
                ];
            }
        }
    }

    /**
     * Returns directories from the given path
     *
     * @param string $path
     *
     * @return iterable|string[]
     */
    public function getDirectories(string $path): iterable
    {
        foreach (glob($path . '/*', GLOB_ONLYDIR) as $match) {
            yield $match;

            yield from $this->getDirectories($match);
        }
    }
}
