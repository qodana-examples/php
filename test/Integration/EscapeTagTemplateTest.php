<?php
declare(strict_types=1);

namespace LessPlateTest\Integration;

use LessPlate\Context\ArrayContext;
use LessPlate\DefaultRenderer;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Input\StringInput;
use LessPlate\Lexer\Exception\TokenizeError;
use LessPlate\Parser\Exception\ParseError;
use PHPUnit\Framework\TestCase;

/**
 * Test escape template capability
 */
class EscapeTagTemplateTest extends TestCase
{
    public const INPUT = <<<'HTML'
\{{ test }}
{{ test }}
HTML;

    public const RESULT = <<<'HTML'
{{ test }}
foobar
HTML;

    public const CONTEXT = [
        'test' => 'foobar',
    ];

    /**
     * Test render
     *
     * @return void
     *
     * @throws ExecuteError
     * @throws TokenizeError
     * @throws ParseError
     */
    public function testRender(): void
    {
        $renderer = new DefaultRenderer();

        $input = StringInput::withMd5Checksum(trim(static::INPUT));
        $context = new ArrayContext(static::CONTEXT);

        $result = $renderer->render($input, $context);

        static::assertSame(
            static::RESULT,
            $result
        );
    }
}
