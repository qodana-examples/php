<?php
declare(strict_types=1);

namespace LessPlateTest\Integration\Transformer;

use LessPlate\Context\ArrayContext;
use LessPlate\DefaultRenderer;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Input\StringInput;
use LessPlate\Lexer\Exception\TokenizeError;
use LessPlate\Parser\Exception\ParseError;
use PHPUnit\Framework\TestCase;

/**
 * Test ceil template integration
 */
final class CeilTemplateTest extends TestCase
{
    /**
     * Test render
     *
     * @param string $input
     * @param string $expected
     * @param array|mixed[] $context
     *
     * @return void
     *
     * @throws ExecuteError
     * @throws TokenizeError
     * @throws ParseError
     *
     * @dataProvider getInput
     */
    public function testRender(string $input, string $expected, array $context): void
    {
        $renderer = new DefaultRenderer();

        $input = StringInput::withMd5Checksum($input);
        $context = new ArrayContext($context);

        $result = $renderer->render($input, $context);
        self::assertSame(
            $expected,
            $result
        );
    }

    /**
     * Returns input to test with
     *
     * @return iterable|mixed[]
     */
    public function getInput(): iterable
    {
        return [
            [
                '{{ foo | ceil(2) }} - {{ bar | ceil(2) }} = {{ baz | ceil }}',
                '2.22 - 1.22 = 1',
                [
                    'foo' => 2.212,
                    'bar' => 1.2101,
                    'baz' => 0.9999999,
                ],
            ],
            [
                '{{ foo | ceil(bar + baz) }}',
                '1.235',
                [
                    'foo' => 1.23456789,
                    'bar' => 1,
                    'baz' => 2,
                ],
            ],
            [
                '{{ foo | ceil(bar | ceil + baz | ceil) }}',
                '1.23457',
                [
                    'foo' => 1.23456789,
                    'bar' => 1.2,
                    'baz' => 2.3,
                ],
            ],
        ];
    }
}