<?php
declare(strict_types=1);

namespace LessPlateTest\Integration;

use LessPlate\Context\ArrayContext;
use LessPlate\DefaultRenderer;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Input\StringInput;
use LessPlate\Lexer\Exception\TokenizeError;
use LessPlate\Parser\Exception\ParseError;
use PHPUnit\Framework\TestCase;

/**
 * Test basic template
 */
final class BasicTemplateTest extends TestCase
{
    public const INPUT = <<<'TXT'
{{ ref[key] }} {{ ref.second }}
TXT;

    public const RESULT = <<<'TXT'
Hello World
TXT;

    public const CONTEXT = [
        'key' => 'first',
        'ref' => [
            'first' => 'Hello',
            'second' => 'World',
        ],
    ];

    /**
     * Test render
     *
     * @return void
     *
     * @throws ExecuteError
     * @throws TokenizeError
     * @throws ParseError
     */
    public function testRender(): void
    {
        $renderer = new DefaultRenderer();

        $input = StringInput::withMd5Checksum(self::INPUT);
        $context = new ArrayContext(self::CONTEXT);

        $result = $renderer->render($input, $context);

        self::assertSame(
            self::RESULT,
            $result
        );
    }
}