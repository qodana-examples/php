<?php
declare(strict_types=1);

namespace LessPlateTest\Integration;

use LessPlate\Context\ArrayContext;
use LessPlate\DefaultRenderer;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Input\StringInput;
use LessPlate\Lexer\Exception\TokenizeError;
use LessPlate\Parser\Exception\ParseError;
use PHPUnit\Framework\TestCase;

/**
 * Test escape template capability
 */
final class EscapeTemplateTest extends TestCase
{
    public const INPUT = <<<'HTML'
Hello <strong>{{ name }}</strong>

Show raw {{ message | raw }}
HTML;

    public const RESULT = <<<'HTML'
Hello <strong>&lt;i&gt;t&lt;/i&gt;</strong>

Show raw <strong>message</strong>
HTML;

    public const CONTEXT = [
        'name' => '<i>t</i>',
        'message' => '<strong>message</strong>',
    ];

    /**
     * Test render
     *
     * @return void
     *
     * @throws ExecuteError
     * @throws TokenizeError
     * @throws ParseError
     */
    public function testRender(): void
    {
        $renderer = new DefaultRenderer();

        $input = StringInput::withMd5Checksum(self::INPUT);
        $context = new ArrayContext(self::CONTEXT);

        $result = $renderer->render($input, $context);

        self::assertSame(
            self::RESULT,
            $result
        );
    }
}
