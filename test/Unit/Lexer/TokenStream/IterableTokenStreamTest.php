<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Lexer\TokenStream;

use LessPlate\Lexer\Token\Token;
use LessPlate\Lexer\TokenStream\IterableTokenStream;
use PHPUnit\Framework\TestCase;

/**
 * Tests IterableTokenStream
 */
final class IterableTokenStreamTest extends TestCase
{
    /**
     * Test iterate
     *
     * @return void
     */
    public function testIterate(): void
    {
        $first = $this->createMock(Token::class);
        $second = $this->createMock(Token::class);

        $fn = [$first, $second];

        $stream = new IterableTokenStream($fn);

        self::assertTrue($stream->valid());
        self::assertSame($first, $stream->getCurrent());
        $stream->consume();

        self::assertTrue($stream->valid());
        self::assertSame($second, $stream->getCurrent());
        $stream->consume();

        self::assertFalse($stream->valid());
    }

    /**
     * Test lookahead
     *
     * @return void
     */
    public function testLookahead(): void
    {
        $first = $this->createMock(Token::class);
        $second = $this->createMock(Token::class);

        $fn = [$first, $second];

        $stream = new IterableTokenStream($fn);

        self::assertSame($second, $stream->lookahead(1));
    }
}
