<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Executor\Util\Value;

use LessPlate\Context\Context;
use LessPlate\Context\Exception\ContextError;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Executor\Util\Value\DynamicValueUtilExecutor;
use LessPlate\Parser\Token\Util\AccessorToken;
use LessPlate\Parser\Token\Value\DynamicValueToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests DynamicValueUtilExecutor
 *
 * @covers \LessPlate\Executor\Util\Value\DynamicValueUtilExecutor
 */
final class DynamicValueUtilExecutorTest extends TestCase
{
    /**
     * Tests null accessor key returns null
     *
     * @param object|array|mixed[] $container
     *
     * @return void
     *
     * @throws ContextError
     * @throws UnexpectedToken
     *
     * @dataProvider getNullAccessorKeyContainers
     */
    public function testNullAccessorKey($container): void
    {
        $executor = new DynamicValueUtilExecutor([]);

        $token = new DynamicValueToken(
            'foo',
            false,
            [
                new AccessorToken(
                    new DynamicValueToken(
                        'bar',
                        false,
                        []
                    ),
                    false
                ),
            ]
        );

        $context = $this->createMock(Context::class);
        $context
            ->method('getValue')
            ->withConsecutive(
                ['foo'],
                ['bar']
            )
            ->willReturnOnConsecutiveCalls(
                $container,
                null
            );
        $context
            ->method('hasValue')
            ->withConsecutive(
                ['foo'],
                ['bar']
            )
            ->willReturnOnConsecutiveCalls(
                true,
                true
            );

        $mainExecutor = $this->createMock(Executor::class);

        $result = $executor->execute($token, $context, $mainExecutor);

        self::assertNull($result);
    }

    /**
     * Returns test values for testNullAccessorKey
     *
     * @return iterable|array[]|object[]
     */
    public function getNullAccessorKeyContainers(): iterable
    {
        return [
            [[]],
            [(object)[]],
        ];
    }
}
