<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Block\ValueObject;

use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\ValueObject\Condition;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Test Condition
 */
final class ConditionTest extends TestCase
{
    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters(): void
    {
        $expression = $this->createMock(Token::class);
        $content = $this->createMock(SyntaxTree::class);

        $condition = new Condition($expression, $content);

        self::assertSame($expression, $condition->getExpression());
        self::assertSame($content, $condition->getContent());
    }
}
