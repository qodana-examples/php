<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Block;

use LessPlate\Parser\Token\Block\IfBlockToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests IfBlockToken
 */
final class IfBlockTokenTest extends TestCase
{
    /**
     * Test name is same for method as for constant
     *
     * @return void
     */
    public function testNameIsSameForConstantAsGetter(): void
    {
        $token = new IfBlockToken([], null);

        self::assertSame(
            IfBlockToken::NAME,
            $token->getName()
        );
    }
}
