<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token;

use LessPlate\Parser\Token\Tag\Expression\TagExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests TagToken
 */
final class TagTokenTest extends TestCase
{
    /**
     * Test constant is same as method
     *
     * @return void
     */
    public function testNameConstantsIsSameAsGetter(): void
    {
        $expression = $this->createMock(Token::class);

        $token = new TagExpressionToken($expression);

        self::assertSame(
            TagExpressionToken::NAME,
            $token->getName()
        );
    }

    /**
     * Test get method
     *
     * @return void
     */
    public function testGetters(): void
    {
        $expression = $this->createMock(Token::class);

        $token = new TagExpressionToken($expression);

        self::assertSame($expression, $token->getExpression());
    }
}
