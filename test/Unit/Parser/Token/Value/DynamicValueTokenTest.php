<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Value;

use LessPlate\Parser\Token\Value\DynamicValueToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests DynamicValueToken
 */
final class DynamicValueTokenTest extends TestCase
{
    /**
     * Test constant is same as method result
     *
     * @return void
     */
    public function testNameIsSameAsConstant(): void
    {
        $token = new DynamicValueToken('foo', false, []);

        self::assertSame(
            DynamicValueToken::NAME,
            $token->getName()
        );
    }

    /**
     * Test reference is same as method result
     *
     * @return void
     */
    public function testReferenceIsSameAsConstruct(): void
    {
        $token = new DynamicValueToken('foo', false, []);

        self::assertSame(
            'foo',
            $token->getReference()
        );
    }
}
