<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Value;

use LessPlate\Parser\Token\Value\StringValueToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests StringValueToken
 */
final class StringValueTokenTest extends TestCase
{
    /**
     * Test constant is same as method result
     *
     * @return void
     */
    public function testNameIsSameAsConstant(): void
    {
        $token = new StringValueToken('foo');

        self::assertSame(
            StringValueToken::NAME,
            $token->getName()
        );
    }

    /**
     * Test construct is same as method
     *
     * @return void
     */
    public function testStringIsSameAsConstruct(): void
    {
        $token = new StringValueToken('foo');

        self::assertSame(
            'foo',
            $token->getString()
        );
    }
}
