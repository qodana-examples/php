<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Value\Number;

use LessPlate\Parser\Token\Value\Number\FloatValueToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests FloatToken
 */
final class FloatTokenTest extends TestCase
{
    /**
     * Test constant is same as method result
     *
     * @return void
     */
    public function testNameIsSameAsConstant(): void
    {
        $token = new FloatValueToken('1.1');

        self::assertSame(
            FloatValueToken::NAME,
            $token->getName()
        );
    }

    /**
     * Test construct is same as method result
     *
     * @return void
     */
    public function testFloatIsSameAsConstruct(): void
    {
        $token = new FloatValueToken('1.1');

        self::assertSame(
            '1.1',
            $token->getFloat()
        );
    }
}
