<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Value\Number;

use LessPlate\Parser\Token\Value\Number\IntegerValueToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests IntegerToken
 */
final class IntegerTokenTest extends TestCase
{
    /**
     * Test constant is same as method result
     *
     * @return void
     */
    public function testNameIsSameAsConstant(): void
    {
        $token = new IntegerValueToken('1');

        self::assertSame(
            IntegerValueToken::NAME,
            $token->getName()
        );
    }

    /**
     * Test construct is same as method result
     *
     * @return void
     */
    public function testIntegerIsSameAsConstruct(): void
    {
        $token = new IntegerValueToken('1');

        self::assertSame(
            '1',
            $token->getInteger()
        );
    }
}
