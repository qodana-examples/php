<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\AbstractArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests AbstractArithmeticExpressionToken
 */
final class AbstractArithmeticExpressionTokenTest extends TestCase
{
    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = $this->getMockForAbstractClass(
            AbstractArithmeticExpressionToken::class,
            [$left, $right]
        );

        self::assertSame($left, $token->getLeft());
        self::assertSame($right, $token->getRight());
    }
}
