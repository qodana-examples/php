<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\MultiplyArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests MultiplyArithmeticExpressionToken
 */
final class MultiplyArithmeticExpressionTokenTest extends TestCase
{
    /**
     * Test constant is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new MultiplyArithmeticExpressionToken(
            $left, $right
        );

        self::assertSame(
            MultiplyArithmeticExpressionToken::NAME,
            $token->getName()
        );
    }
}
