<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\DivideArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests DivideArithmeticExpressionToken
 */
final class DivideArithmeticExpressionTokenTest extends TestCase
{
    /**
     * Test name constant is same as method
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new DivideArithmeticExpressionToken(
            $left, $right
        );

        self::assertSame(
            DivideArithmeticExpressionToken::NAME,
            $token->getName()
        );
    }
}
