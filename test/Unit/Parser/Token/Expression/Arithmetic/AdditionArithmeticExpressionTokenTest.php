<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\AdditionArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests AdditionArithmeticExpressionToken
 */
final class AdditionArithmeticExpressionTokenTest extends TestCase
{
    /**
     * Test name constant is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new AdditionArithmeticExpressionToken(
            $left, $right
        );

        self::assertSame(
            AdditionArithmeticExpressionToken::NAME,
            $token->getName()
        );
    }
}
