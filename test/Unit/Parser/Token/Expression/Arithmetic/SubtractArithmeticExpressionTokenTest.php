<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\SubtractArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests SubtractArithmeticExpressionToken
 */
final class SubtractArithmeticExpressionTokenTest extends TestCase
{
    /**
     * Test name constant is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new SubtractArithmeticExpressionToken(
            $left, $right
        );

        self::assertSame(
            SubtractArithmeticExpressionToken::NAME,
            $token->getName()
        );
    }
}
