<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\PowerArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests PowerArithmeticExpressionToken
 */
final class PowerArithmeticExpressionTokenTest extends TestCase
{
    /**
     * Test name constant is same as method
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new PowerArithmeticExpressionToken(
            $left, $right
        );

        self::assertSame(
            PowerArithmeticExpressionToken::NAME,
            $token->getName()
        );
    }
}
