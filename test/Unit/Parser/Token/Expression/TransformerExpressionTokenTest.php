<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression;

use LessPlate\Parser\Token\Expression\TransformerExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests TransformerExpressionToken
 */
final class TransformerExpressionTokenTest extends TestCase
{
    /**
     * Test constant is same as method
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $value = $this->createMock(Token::class);

        $token = new TransformerExpressionToken(
            $value, []
        );

        self::assertSame(
            TransformerExpressionToken::NAME,
            $token->getName()
        );
    }
}
