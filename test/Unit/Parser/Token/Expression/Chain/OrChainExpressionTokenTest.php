<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Chain;

use LessPlate\Parser\Token\Expression\Chain\OrLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests OrLogicalExpressionToken
 */
final class OrChainExpressionTokenTest extends TestCase
{
    /**
     * Test name constant is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new OrLogicalExpressionToken(
            $left, $right
        );

        self::assertSame(
            OrLogicalExpressionToken::NAME,
            $token->getName()
        );
    }
}
