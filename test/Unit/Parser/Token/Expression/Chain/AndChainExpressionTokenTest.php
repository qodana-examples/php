<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Chain;

use LessPlate\Parser\Token\Expression\Chain\AndLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests AndLogicalExpressionToken
 */
final class AndChainExpressionTokenTest extends TestCase
{
    /**
     * Test constant is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new AndLogicalExpressionToken(
            $left, $right
        );

        self::assertSame(
            AndLogicalExpressionToken::NAME,
            $token->getName()
        );
    }
}
