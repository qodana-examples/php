<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Chain;

use LessPlate\Parser\Token\Expression\Chain\AbstractChainExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests AbstractChainExpressionToken
 */
final class AbstractChainExpressionTokenTest extends TestCase
{
    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = $this->getMockForAbstractClass(
            AbstractChainExpressionToken::class,
            [$left, $right]
        );

        self::assertSame($left, $token->getLeft());
        self::assertSame($right, $token->getRight());
    }
}
