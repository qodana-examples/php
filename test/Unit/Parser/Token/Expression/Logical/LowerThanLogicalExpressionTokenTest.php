<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Expression\Logical\LowerThanLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests LowerThanLogicalExpressionToken
 */
final class LowerThanLogicalExpressionTokenTest extends TestCase
{
    /**
     * Test constant name is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new LowerThanLogicalExpressionToken(
            $left, $right
        );

        self::assertSame(
            LowerThanLogicalExpressionToken::NAME,
            $token->getName()
        );
    }
}
