<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Expression\Logical\LowerThanOrEqualsLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests LowerThanOrEqualsLogicalExpressionToken
 */
final class LowerThanOrEqualsLogicalExpressionTokenTest extends TestCase
{
    /**
     * Tests constant name is same as method
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new LowerThanOrEqualsLogicalExpressionToken(
            $left, $right
        );

        self::assertSame(
            LowerThanOrEqualsLogicalExpressionToken::NAME,
            $token->getName()
        );
    }
}
