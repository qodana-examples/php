<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Expression\Logical\GreaterThanLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests GreaterThanLogicalExpressionToken
 */
final class GreaterThanLogicalExpressionTokenTest extends TestCase
{
    /**
     * Test name constant is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new GreaterThanLogicalExpressionToken(
            $left, $right
        );

        self::assertSame(
            GreaterThanLogicalExpressionToken::NAME,
            $token->getName()
        );
    }
}
