<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Expression\Logical\AbstractLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests AbstractLogicalExpressionToken
 */
final class AbstractLogicalExpressionTokenTest extends TestCase
{
    /**
     * Test get methods
     *
     * @return void
     */
    public function testGetters(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = $this->getMockForAbstractClass(
            AbstractLogicalExpressionToken::class,
            [$left, $right]
        );

        self::assertSame($left, $token->getLeft());
        self::assertSame($right, $token->getRight());
    }
}
