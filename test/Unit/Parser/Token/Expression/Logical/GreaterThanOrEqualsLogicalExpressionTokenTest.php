<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Expression\Logical\GreaterThanOrEqualsLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests GreaterThanOrEqualsLogicalExpressionToken
 */
final class GreaterThanOrEqualsLogicalExpressionTokenTest extends TestCase
{
    /**
     * Test constant is same as method
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new GreaterThanOrEqualsLogicalExpressionToken(
            $left, $right
        );

        self::assertSame(
            GreaterThanOrEqualsLogicalExpressionToken::NAME,
            $token->getName()
        );
    }
}
