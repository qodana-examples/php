<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Expression\Logical\NotEqualsLogicalExpressionToken;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests NotEqualsLogicalExpressionToken
 */
final class NotEqualsLogicalExpressionTokenTest extends TestCase
{
    /**
     * Test constant name is same as method result
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $left = $this->createMock(Token::class);
        $right = $this->createMock(Token::class);

        $token = new NotEqualsLogicalExpressionToken(
            $left, $right
        );

        self::assertSame(
            NotEqualsLogicalExpressionToken::NAME,
            $token->getName()
        );
    }
}
