<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token\Util;

use LessPlate\Parser\Token\Util\TransformerToken;
use LessPlate\Parser\Token\Value\ValueToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests TransformerToken
 */
final class TransformerTokenTest extends TestCase
{
    /**
     * Test name constant is same as getName
     *
     * @return void
     */
    public function testNameConstantIsSameAsGetName(): void
    {
        $token = new TransformerToken('foo', []);

        self::assertSame(
            TransformerToken::NAME,
            $token->getName()
        );
    }

    /**
     * Test get methods
     *
     * @return void
     */
    public function testGetMethods(): void
    {
        $value = $this->createMock(ValueToken::class);

        $token = new TransformerToken(
            'foo',
            [$value]
        );

        self::assertSame(
            'foo',
            $token->getReference()
        );

        self::assertSame(
            [$value],
            $token->getParameters()
        );
    }
}
