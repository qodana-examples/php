<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Token;

use LessPlate\Parser\Token\TextToken;
use PHPUnit\Framework\TestCase;

/**
 * Tests TextToken
 */
final class TextTokenTest extends TestCase
{
    /**
     * Test constant is
     *
     * @return void
     */
    public function testConstantIsSameAsGetName(): void
    {
        $token = new TextToken('');

        self::assertSame(
            TextToken::NAME,
            $token->getName()
        );
    }

    /**
     * Test text is same as provided
     *
     * @return void
     */
    public function testTextIsSameAsProvided(): void
    {
        $token = new TextToken('foo');

        self::assertSame('foo', $token->getText());
    }
}
