<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\SyntaxTree;

use LessPlate\Parser\SyntaxTree\IterableSyntaxTree;
use LessPlate\Parser\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * Tests IterableSyntaxTree
 */
final class IterableSyntaxTreeTest extends TestCase
{
    /**
     * Test iterate
     *
     * @return void
     */
    public function testIterate(): void
    {
        $first = $this->createMock(Token::class);
        $second = $this->createMock(Token::class);

        $fn = [$first, $second];

        $stream = new IterableSyntaxTree($fn);

        self::assertTrue($stream->valid());
        self::assertSame($first, $stream->getCurrent());
        $stream->consume();

        self::assertTrue($stream->valid());
        self::assertSame($second, $stream->getCurrent());
        $stream->consume();

        self::assertFalse($stream->valid());
    }
}
