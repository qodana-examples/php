<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Parser\Util;

use LessPlate\Lexer\Token\CharacterToken;
use LessPlate\Lexer\Token\Token;
use LessPlate\Lexer\Token\Value\DigitToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\Token\Value\StringToken;
use LessPlate\Lexer\TokenStream\ArrayTokenStream;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Token\Value\DynamicValueToken;
use LessPlate\Parser\Token\Value\Number\FloatValueToken;
use LessPlate\Parser\Token\Value\Number\IntegerValueToken;
use LessPlate\Parser\Token\Value\StringValueToken;
use LessPlate\Parser\Util\ValueParser;
use PHPUnit\Framework\TestCase;

/**
 * Tests ValueParser
 */
final class ValueParserTest extends TestCase
{
    /**
     * Test value parsing
     *
     * @param array|Token[] $tokens
     * @param string $expected
     *
     * @return void
     *
     * @dataProvider getValues
     *
     * @throws ParseError
     */
    public function testValueParsing(array $tokens, string $expected): void
    {
        $stream = new ArrayTokenStream($tokens);

        $parser = new ValueParser();

        $result = $parser->parse($stream);

        self::assertInstanceOf($expected, $result);
    }

    /**
     * Returns possible values
     *
     * @return array|mixed[]
     */
    public function getValues(): array
    {
        return [
            [
                [new DigitToken('1', 1, 1)],
                IntegerValueToken::class,
            ],
            [
                [
                    new DigitToken('1', 1, 1),
                    new CharacterToken('.', 1, 2),
                    new DigitToken('2', 1, 3),
                ],
                FloatValueToken::class,
            ],
            [
                [new StringToken('bar', 1, 1)],
                StringValueToken::class,
            ],
            [
                [new ReferenceToken('foo', 1, 1)],
                DynamicValueToken::class,
            ],
        ];
    }

    /**
     * Test unknown value throws exception
     *
     * @return void
     *
     * @throws ParseError
     * @throws UnexpectedToken
     */
    public function testUnknownValue(): void
    {
        $this->expectException(UnexpectedToken::class);

        $token = $this->createMock(Token::class);

        $stream = $this->createMock(TokenStream::class);
        $stream->method('getCurrent')->willReturn($token);

        $parser = new ValueParser();

        try {
            $parser->parse($stream);
        } catch (UnexpectedToken $e) {
            self::assertSame($token, $e->getGot());
            self::assertSame(
                [
                    DigitToken::NAME,
                    StringToken::NAME,
                    ReferenceToken::NAME,
                ],
                $e->getExpected()
            );

            throw $e;
        }
    }
}
