<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Input;

use LessPlate\Input\AbstractInput;
use PHPUnit\Framework\TestCase;

/**
 * Tests AbstractInput
 */
final class AbstractInputTest extends TestCase
{
    /**
     * Tests getSequence
     *
     * @return void
     */
    public function testGetSequence(): void
    {
        $mock = $this->getMockForAbstractClass(AbstractInput::class);

        $mock
            ->method('getCurrent')
            ->willReturn('b');

        $mock
            ->expects(self::at(1))
            ->method('lookahead')
            ->with(1)
            ->willReturn('a');

        $mock
            ->expects(self::at(2))
            ->method('lookahead')
            ->with(2)
            ->willReturn('r');

        $sequence = $mock->getSequence(3);

        self::assertSame('bar', $sequence);
    }
}
