<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Input;

use LessPlate\Input\ResourceInput;
use PHPUnit\Framework\TestCase;
use TypeError;

/**
 * Tests ResourceInput
 */
final class ResourceInputTest extends TestCase
{
    private const TEXT_ITERATE_STRING = <<<'TXT'
F
ô
TXT;

    /**
     * Test iterate
     *
     * @return void
     */
    public function testIterate(): void
    {
        $resource = fopen('php://memory', 'rb+');
        fwrite($resource, self::TEXT_ITERATE_STRING);
        rewind($resource);

        $input = new ResourceInput($resource, 'foo');

        self::assertTrue($input->valid());
        self::assertSame('F', $input->getCurrent());
        self::assertSame(1, $input->getCurrentLine());
        self::assertSame(1, $input->getCurrentColumn());
        $input->consume();

        self::assertTrue($input->valid());
        self::assertSame(PHP_EOL, $input->getCurrent());
        self::assertSame(1, $input->getCurrentLine());
        self::assertSame(2, $input->getCurrentColumn());
        $input->consume();

        self::assertTrue($input->valid());
        self::assertSame('ô', $input->getCurrent());
        self::assertSame(2, $input->getCurrentLine());
        self::assertSame(1, $input->getCurrentColumn());
        $input->consume();

        self::assertFalse($input->valid());
    }

    /**
     * Test only resource is allowed
     *
     * @return void
     */
    public function testOnlyResourceIsAllowed(): void
    {
        self::expectException(TypeError::class);

        new ResourceInput(1, 'foo');
    }

    /**
     * Test lookahead
     *
     * @return void
     */
    public function testLookahead(): void
    {
        $resource = fopen('php://memory', 'rb+');
        fwrite($resource, self::TEXT_ITERATE_STRING);
        rewind($resource);

        $input = new ResourceInput($resource, 'foo');

        self::assertSame(PHP_EOL, $input->lookahead());
        self::assertSame('ô', $input->lookahead(2));
        self::assertNull($input->lookahead(3));
    }

    /**
     * Test checksum
     *
     * @return void
     */
    public function testChecksum(): void
    {
        $resource = fopen('php://memory', 'rb+');

        $input = new ResourceInput($resource, 'foo');

        self::assertSame('foo', $input->getCheckSum());
    }
}
