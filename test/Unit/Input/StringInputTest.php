<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Input;

use LessPlate\Input\StringInput;
use PHPUnit\Framework\TestCase;

/**
 * Test for StringInput
 */
final class StringInputTest extends TestCase
{
    private const TEXT_ITERATE_STRING = <<<'TXT'
F
ô
TXT;

    /**
     * Test iterate
     *
     * @return void
     */
    public function testIterate(): void
    {
        $input = new StringInput(self::TEXT_ITERATE_STRING, 'foo');

        self::assertTrue($input->valid());
        self::assertSame('F', $input->getCurrent());
        self::assertSame(1, $input->getCurrentLine());
        self::assertSame(1, $input->getCurrentColumn());
        $input->consume();

        self::assertTrue($input->valid());
        self::assertSame(PHP_EOL, $input->getCurrent());
        self::assertSame(1, $input->getCurrentLine());
        self::assertSame(2, $input->getCurrentColumn());
        $input->consume();

        self::assertTrue($input->valid());
        self::assertSame('ô', $input->getCurrent());
        self::assertSame(2, $input->getCurrentLine());
        self::assertSame(1, $input->getCurrentColumn());
        $input->consume();

        self::assertFalse($input->valid());
    }

    /**
     * Test lookahead
     *
     * @return void
     */
    public function testLookahead(): void
    {
        $input = new StringInput(self::TEXT_ITERATE_STRING, 'foo');

        self::assertSame(PHP_EOL, $input->lookahead());
        self::assertSame('ô', $input->lookahead(2));
        self::assertNull($input->lookahead(3));
    }

    /**
     * Test md5 checksum
     *
     * @return void
     */
    public function testMd5Checksum(): void
    {
        $string = 'foo';
        $md5 = md5($string);

        $input = StringInput::withMd5Checksum($string);

        self::assertSame($md5, $input->getCheckSum());
    }

    /**
     * Test sha1 checksum
     *
     * @return void
     */
    public function testSha1Checksum(): void
    {
        $string = 'foo';
        $sha1 = sha1($string);

        $input = StringInput::withSha1Checksum($string);

        self::assertSame($sha1, $input->getCheckSum());
    }

    /**
     * Tests locateSequence
     *
     * @param string $sequence
     * @param string $text
     * @param int|null $position
     *
     * @return void
     *
     * @dataProvider getInputLocateSequence
     */
    public function testLocateSequence(string $sequence, string $text, ?int $position): void
    {
        $input = StringInput::withMd5Checksum($text);

        self::assertSame(
            $position,
            $input->locateSequence($sequence)
        );
    }

    /**
     * Returns input for testLocateSequence
     *
     * @return iterable|mixed[]
     */
    public function getInputLocateSequence(): iterable
    {
        return [
            ['foo', 'foobar', 0],
            ['bar', 'foobar', 3],
            ['a', 'foobar', 4],
            ['x', 'foobar', null],
        ];
    }
}
