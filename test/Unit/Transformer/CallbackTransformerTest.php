<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Transformer;

use LessPlate\Transformer\CallbackTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Tests CallbackTransformer
 */
final class CallbackTransformerTest extends TestCase
{
    /**
     * Test transform
     *
     * @return void
     */
    public function testTransform(): void
    {
        $callback = static function ($input, $parameter) {
            return $input === 'foo' && $parameter === 'bar';
        };

        $transformer = new CallbackTransformer($callback);

        self::assertTrue(
            $transformer->transform(
                'foo',
                'bar'
            )
        );
    }
}
