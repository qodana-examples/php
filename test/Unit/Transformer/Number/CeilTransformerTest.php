<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Transformer\Number;

use LessPlate\Transformer\Number\CeilTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Tests CeilTransformer
 */
final class CeilTransformerTest extends TestCase
{
    /**
     * Test transform
     *
     * @param mixed $input
     * @param mixed $expected
     * @param int $precision
     *
     * @return void
     *
     * @dataProvider getTestInput
     */
    public function testTransform($input, $expected, int $precision): void
    {
        $transformer = new CeilTransformer();

        self::assertSame(
            $expected,
            $transformer->transform($input, $precision)
        );
    }

    /**
     * Test input for testTransform
     *
     * @return array|mixed[]
     */
    public function getTestInput(): array
    {
        return [
            [1, 1, 0],
            [1.1, 2, 0],
            [1.9, 2, 0],
            [1.81, 1.9, 1],
            ['foo', null, 0],
        ];
    }
}
