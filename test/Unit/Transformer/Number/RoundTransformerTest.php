<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Transformer\Number;

use LessPlate\Transformer\Number\RoundTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Tests RoundTransformer
 */
final class RoundTransformerTest extends TestCase
{
    /**
     * Test transform
     *
     * @param float $input
     * @param float $expected
     * @param int $precision
     *
     * @return void
     *
     * @dataProvider getTestValues
     */
    public function testTransform(float $input, float $expected, int $precision): void
    {
        $transformer = new RoundTransformer();

        $result = $transformer->transform($input, $precision);

        self::assertSame($expected, $result);
    }

    /**
     * Returns test values
     *
     * @return iterable|mixed[]
     */
    public function getTestValues(): iterable
    {
        return [
            [1.1, 1, 0],
            [1.6, 2, 0],
            [1.11, 1.1, 1],
            [1.15, 1.2, 1],
        ];
    }
}
