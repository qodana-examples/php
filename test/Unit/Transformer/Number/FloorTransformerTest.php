<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Transformer\Number;

use LessPlate\Transformer\Number\FloorTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Tests FloorTransformer
 */
final class FloorTransformerTest extends TestCase
{
    /**
     * Test transform
     *
     * @param mixed $input
     * @param mixed $expected
     * @param int $precision
     *
     * @return void
     *
     * @dataProvider getTestInput
     */
    public function testTransform($input, $expected, int $precision): void
    {
        $transformer = new FloorTransformer();

        self::assertSame(
            $expected,
            $transformer->transform($input, $precision)
        );
    }

    /**
     * Returns input to test with
     *
     * @return array|mixed[]
     */
    public function getTestInput(): array
    {
        return [
            [1, 1, 0],
            [1.1, 1, 0],
            [1.9, 1, 0],
            [1.99, 1.9, 1],
            ['foo', null, 0],
        ];
    }
}
