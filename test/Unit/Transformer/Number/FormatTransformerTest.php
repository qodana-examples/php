<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Transformer\Number;

use LessPlate\Transformer\Number\FormatTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Tests FormatTransformer
 *
 * @covers \LessPlate\Transformer\Number\FormatTransformer
 */
final class FormatTransformerTest extends TestCase
{
    /**
     * Test transform
     *
     * @param mixed $input
     * @param mixed $expected
     * @param mixed ...$parameters
     *
     * @return void
     *
     * @dataProvider getTestValues
     */
    public function testTransform($input, $expected, ...$parameters): void
    {
        $transformer = new FormatTransformer();

        $result = $transformer->transform($input, ...$parameters);

        self::assertSame($expected, $result);
    }

    /**
     * Returns test values
     *
     * @return iterable|mixed[]
     */
    public function getTestValues(): iterable
    {
        return [
            [1.1, '1'],
            [123456.789, '123,456.789', 3],
            [123456.789, '123.456,79', 2, ',', '.'],
        ];
    }
}
