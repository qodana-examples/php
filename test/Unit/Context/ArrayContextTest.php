<?php
declare(strict_types=1);

namespace LessPlateTest\Unit\Context;

use LessPlate\Context\ArrayContext;
use LessPlate\Context\Exception\UnknownValue;
use PHPUnit\Framework\TestCase;

/**
 * Tests ArrayContext
 */
final class ArrayContextTest extends TestCase
{
    /**
     * Test has
     *
     * @param string $key
     * @param bool $expected
     *
     * @return void
     *
     * @dataProvider getHasValues
     */
    public function testHas(string $key, bool $expected): void
    {
        $context = new ArrayContext(
            ['foo' => 1]
        );

        self::assertSame(
            $expected,
            $context->hasValue($key)
        );
    }

    /**
     * Returns data to use for testHas
     *
     * @return array|mixed[]
     */
    public function getHasValues(): array
    {
        return [
            ['foo', true],
            ['bar', false],
        ];
    }

    /**
     * Test get value
     *
     * @return void
     *
     * @throws UnknownValue
     */
    public function testGetValue(): void
    {
        $context = new ArrayContext(
            ['foo' => 1]
        );

        self::assertSame(
            1,
            $context->getValue('foo')
        );
    }

    /**
     * Test unknown value throws exception
     *
     * @return void
     *
     * @throws UnknownValue
     */
    public function testUnknownValueThrowsException(): void
    {
        $this->expectException(UnknownValue::class);

        $context = new ArrayContext(
            ['foo' => 1]
        );

        try {
            $context->getValue('bar');
        } catch (UnknownValue $e) {
            self::assertSame(
                'bar',
                $e->getKey()
            );

            throw $e;
        }
    }
}
