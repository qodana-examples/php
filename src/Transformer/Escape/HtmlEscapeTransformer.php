<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Escape;

use LessPlate\Transformer\Raw\RawValue;
use LessPlate\Transformer\Transformer;

/**
 * Transformer to escape input
 */
final class HtmlEscapeTransformer implements Transformer
{
    /**
     * Escapes the input
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return string
     */
    public function transform($input, ...$parameters): string
    {
        if (is_scalar($input) && !is_string($input)) {
            return (string)$input;
        }

        if ($input instanceof RawValue) {
            return (string)$input;
        }

        return htmlspecialchars(
            (string)$input,
            $parameters[0] ?? ENT_COMPAT | ENT_HTML401,
            $parameters[1] ?? ini_get('default_charset'),
            $parameters[2] ?? true
        );
    }
}
