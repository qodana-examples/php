<?php
declare(strict_types=1);

namespace LessPlate\Transformer;

use function array_shift;

/**
 * Return parameter if empty
 */
final class OnEmptyTransformer implements Transformer
{
    /**
     * If input is empty use parameter
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed
     */
    public function transform($input, ...$parameters)
    {
        // phpcs:ignore SlevomatCodingStandard.ControlStructures.DisallowEmpty.DisallowedEmpty
        if (empty($input)) {
            $input = array_shift($parameters);
        }

        return $input;
    }
}