<?php
declare(strict_types=1);

namespace LessPlate\Transformer;

use function array_shift;

/**
 * Return parameter if null
 */
final class OnNullTransformer implements Transformer
{
    /**
     * If input is null use parameter
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed|null
     */
    public function transform($input, ...$parameters)
    {
        if ($input === null) {
            $input = array_shift($parameters);
        }

        return $input;
    }
}