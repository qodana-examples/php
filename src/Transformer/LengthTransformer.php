<?php
declare(strict_types=1);

namespace LessPlate\Transformer;

use Countable;

/**
 * Transformer to get length
 */
final class LengthTransformer implements Transformer
{
    /**
     * Returns input length
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return int|mixed|void
     */
    public function transform($input, ...$parameters)
    {
        if (is_array($input) || $input instanceof Countable) {
            return count($input);
        }

        if (is_string($input) || (is_object($input) && method_exists($input, '__toString'))) {
            return mb_strlen((string)$input);
        }

        return 0;
    }
}