<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Number;

use LessPlate\Transformer\Transformer;

/**
 * Number format transformer
 */
final class FormatTransformer implements Transformer
{
    /**
     * Transforms input using number_format
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed|string
     */
    public function transform($input, ...$parameters)
    {
        if (is_int($input) || is_float($input)) {
            $input = number_format($input, ...$parameters);
        }

        return $input;
    }
}
