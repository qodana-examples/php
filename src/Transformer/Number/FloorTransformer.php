<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Number;

use LessPlate\Transformer\Transformer;

/**
 * Transform a number to the floor value
 */
final class FloorTransformer implements Transformer
{
    /**
     * Transform a number with the floor
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return float|mixed|null
     */
    public function transform($input, ...$parameters)
    {
        if (is_int($input) || is_float($input)) {
            $precision = $parameters[0] ?? 0;

            if ($precision === 0) {
                return (int)floor($input);
            }

            $power = pow(10, $precision);

            return (int)floor($input * $power) / $power;
        }

        return null;
    }
}