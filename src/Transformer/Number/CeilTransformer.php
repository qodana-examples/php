<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Number;

use LessPlate\Transformer\Transformer;

/**
 * Transform the input number to an int
 */
final class CeilTransformer implements Transformer
{
    /**
     * Transform the input number with ceil
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return float|mixed|null
     */
    public function transform($input, ...$parameters)
    {
        if (is_int($input) || is_float($input)) {
            $precision = $parameters[0] ?? 0;

            if ($precision === 0) {
                return (int)ceil($input);
            }

            $power = pow(10, $precision);

            return (int)ceil($input * $power) / $power;
        }

        return null;
    }
}