<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Number;

use LessPlate\Transformer\Transformer;

/**
 * Round number
 */
final class RoundTransformer implements Transformer
{
    /**
     * Round number
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed|string
     */
    public function transform($input, ...$parameters)
    {
        if (is_int($input) || is_float($input)) {
            $precision = $parameters[0] ?? 0;

            $input = round($input, $precision);
        }

        return $input;
    }
}