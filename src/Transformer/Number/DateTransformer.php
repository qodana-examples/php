<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Number;

use LessPlate\Transformer\Transformer;

/**
 * Transformer for date format
 */
final class DateTransformer implements Transformer
{
    /**
     * Transform input using date
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return false|mixed|string
     */
    public function transform($input, ...$parameters)
    {
        if (is_int($input) || is_float($input)) {
            $input = date($parameters[0], (int)$input);
        }

        return $input;
    }
}