<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Raw;

/**
 * Raw value
 */
interface RawValue
{
    /**
     * Returns the war string value to use
     *
     * @return string
     */
    public function __toString(): string;
}