<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Raw;

use LessPlate\Transformer\Transformer;

/**
 * Transforms the value to a raw value
 */
final class RawTransformer implements Transformer
{
    /**
     * Returns the raw value
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return RawValueConstruct|mixed
     */
    public function transform($input, ...$parameters)
    {
        if (is_string($input)) {
            $input = new RawValueConstruct($input);
        }

        return $input;
    }
}