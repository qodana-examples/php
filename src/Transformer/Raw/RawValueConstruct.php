<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Raw;

/**
 * Raw based on construct
 */
final class RawValueConstruct implements RawValue
{
    /**
     * Value to be used
     *
     * @var mixed
     */
    private $value;

    /**
     * RawConstruct constructor
     *
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * Returns the string value to use
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->value;
    }
}