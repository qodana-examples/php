<?php
declare(strict_types=1);

namespace LessPlate\Transformer\Encode;

use LessPlate\Transformer\Transformer;

/**
 * Transform input to json
 */
final class JsonTransformer implements Transformer
{
    /**
     * Transform input to json
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return false|mixed|string
     */
    public function transform($input, ...$parameters)
    {
        $options = $parameters[0] ?? 0;

        return json_encode($input, $options);
    }
}