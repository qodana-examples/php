<?php
declare(strict_types=1);

namespace LessPlate\Transformer;

/**
 * Callback transformer
 */
final class CallbackTransformer implements Transformer
{
    /**
     * Callback to use for transform
     *
     * @var callable
     */
    protected $callback;

    /**
     * CallbackTransformer constructor
     *
     * @param callable $callback
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * Transform input using the callback
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed
     */
    public function transform($input, ...$parameters)
    {
        $callback = $this->callback;

        return $callback($input, ...$parameters);
    }
}