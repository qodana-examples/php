<?php
declare(strict_types=1);

namespace LessPlate\Transformer\String;

use LessPlate\Transformer\Transformer;

/**
 * Transformer around nl2br
 */
final class Nl2BrTransformer implements Transformer
{
    /**
     * Changes new lines to <br?
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed|string
     */
    public function transform($input, ...$parameters)
    {
        if (is_string($input)) {
            return nl2br($input);
        }

        return $input;
    }
}
