<?php
declare(strict_types=1);

namespace LessPlate\Transformer\String;

use LessPlate\Transformer\Transformer;

/**
 * Strip tags transformer
 */
final class StripTagsTransformer implements Transformer
{
    /**
     * Transform
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed|string
     */
    public function transform($input, ...$parameters)
    {
        if (is_string($input)) {
            $input = strip_tags(
                $input,
                ...$parameters
            );
        }

        return $input;
    }
}
