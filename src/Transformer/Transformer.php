<?php
declare(strict_types=1);

namespace LessPlate\Transformer;

/**
 * Transformer to transform an input
 */
interface Transformer
{
    /**
     * Transform the input
     *
     * @param mixed $input
     * @param mixed ...$parameters
     *
     * @return mixed
     */
    public function transform($input, ...$parameters);
}