<?php
declare(strict_types=1);

namespace LessPlate\Context;

use LessPlate\Context\Exception\ContextError;

/**
 * Context to render with
 */
interface Context
{
    /**
     * Returns the value from the context
     *
     * @param string $key
     *
     * @return mixed
     *
     * @throws ContextError
     */
    public function getValue(string $key);

    /**
     * Check if the value exists in the context
     *
     * @param string $key
     *
     * @return bool
     *
     * @throws ContextError
     */
    public function hasValue(string $key): bool;
}