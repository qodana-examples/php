<?php
declare(strict_types=1);

namespace LessPlate\Context;

use LessPlate\Context\Exception\ContextError;
use LessPlate\Context\Exception\UnknownValue;

/**
 * Combines multiple contexts
 */
final class CombinedContext implements Context
{
    /**
     * Holds the combined context
     *
     * @var array|Context[]
     */
    private $contexts = [];

    /**
     * CombinedContext constructor
     *
     * @param array|Context[] $contexts
     */
    public function __construct(iterable $contexts)
    {
        foreach ($contexts as $context) {
            $this->addContext($context);
        }
    }

    /**
     * Named construct
     *
     * @param Context ...$contexts
     *
     * @return CombinedContext
     */
    public static function join(Context ...$contexts): CombinedContext
    {
        return new static($contexts);
    }

    /**
     * Adds context
     *
     * @param Context $context
     *
     * @return void
     */
    private function addContext(Context $context): void
    {
        $this->contexts[] = $context;
    }

    /**
     * Get value from any of the contexts
     *
     * @param string $key
     *
     * @return mixed
     *
     * @throws ContextError
     * @throws UnknownValue
     */
    public function getValue(string $key)
    {
        foreach ($this->contexts as $context) {
            if ($context->hasValue($key)) {
                return $context->getValue($key);
            }
        }

        throw new UnknownValue($key);
    }

    /**
     * Check if the value exists in any of the contexts
     *
     * @param string $key
     *
     * @return bool
     *
     * @throws ContextError
     */
    public function hasValue(string $key): bool
    {
        foreach ($this->contexts as $context) {
            if ($context->hasValue($key)) {
                return true;
            }
        }

        return false;
    }
}