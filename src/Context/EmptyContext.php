<?php
declare(strict_types=1);

namespace LessPlate\Context;

use LessPlate\Context\Exception\UnknownValue;

/**
 * Empty context
 */
final class EmptyContext implements Context
{
    /**
     * Returns the value from the array
     *
     * @param string $key
     *
     * @return mixed
     *
     * @throws UnknownValue
     */
    public function getValue(string $key)
    {
        throw new UnknownValue($key);
    }

    /**
     * Checks the array for the value
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasValue(string $key): bool
    {
        return false;
    }
}
