<?php
declare(strict_types=1);

namespace LessPlate\Context;

use LessPlate\Context\Exception\UnknownValue;

/**
 * Array implementation of context
 */
final class ArrayContext implements Context
{
    /**
     * Holds the context
     *
     * @var array|mixed[]
     */
    protected $array;

    /**
     * ArrayContext constructor
     *
     * @param array|mixed[] $array
     */
    public function __construct(array $array)
    {
        $this->array = $array;
    }

    /**
     * Returns the value from the array
     *
     * @param string $key
     *
     * @return mixed
     *
     * @throws UnknownValue
     */
    public function getValue(string $key)
    {
        if (!$this->hasValue($key)) {
            throw new UnknownValue($key);
        }

        return $this->array[$key];
    }

    /**
     * Checks the array for the value
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasValue(string $key): bool
    {
        return array_key_exists($key, $this->array);
    }
}
