<?php
declare(strict_types=1);

namespace LessPlate\Context;

use InvalidArgumentException;
use LessPlate\Context\Exception\UnknownValue;

/**
 * Object implementation of context
 */
final class ObjectContext implements Context
{
    /**
     * Holds the context
     *
     * @var object
     */
    protected $object;

    /**
     * ObjectContext constructor
     *
     * @param object|mixed $object
     */
    public function __construct($object)
    {
        if (!is_object($object)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Expected object got %s',
                    gettype($object)
                )
            );
        }

        $this->object = $object;
    }

    /**
     * Returns the value from the object
     *
     * @param string $key
     *
     * @return mixed
     *
     * @throws UnknownValue
     */
    public function getValue(string $key)
    {
        if (!$this->hasValue($key)) {
            throw new UnknownValue($key);
        }

        return $this->object->{$key};
    }

    /**
     * Check the value exists
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasValue(string $key): bool
    {
        return property_exists($this->object, $key);
    }
}