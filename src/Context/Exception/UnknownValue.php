<?php
declare(strict_types=1);

namespace LessPlate\Context\Exception;

use Exception;

/**
 * Thrown when the value that is requested is not known
 */
final class UnknownValue extends Exception implements ContextError
{
    /**
     * Used key
     *
     * @var string
     */
    protected $key;

    /**
     * UnknownValue constructor
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        parent::__construct(
            sprintf(
                'There is no value for "%s"',
                $key
            )
        );

        $this->key = $key;
    }

    /**
     * THe key that is not known
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }
}