<?php
declare(strict_types=1);

namespace LessPlate\Context\Exception;

use Throwable;

/**
 * Thrown on an error in the context
 */
interface ContextError extends Throwable
{
}