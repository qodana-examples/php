<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag;

use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Parser;
use LessPlate\Parser\Token\Token;

/**
 * Parser for a tag
 */
interface TagParser
{
    /**
     * Parse a tag
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return Token
     */
    public function parse(TokenStream $stream, Parser $parser): Token;
}