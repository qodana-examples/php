<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Block;

use LessPlate\Parser\Tag\TagParser;

/**
 * Block parser
 */
interface TagBlockParser extends TagParser
{
}