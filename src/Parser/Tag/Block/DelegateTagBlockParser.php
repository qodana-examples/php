<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Block;

use LessPlate\Lexer\Token\Tag\TagStartToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Parser;
use LessPlate\Parser\Tag\Block\Exception\NoBlockParserForReference;
use LessPlate\Parser\Token\Token;

/**
 * Delegates parsing to other tag block parsers
 */
final class DelegateTagBlockParser implements TagBlockParser
{
    /**
     * Holds tag block parsers
     *
     * @var array|TagBlockParser[]
     */
    private $tagBlockParsers = [];

    /**
     * DelegateTagBlockParser constructor
     *
     * @param array|TagBlockParser[] $tagBlockParsers
     */
    public function __construct(iterable $tagBlockParsers)
    {
        foreach ($tagBlockParsers as $for => $tagBlockParser) {
            $this->addTagBlockParser($for, $tagBlockParser);
        }
    }

    /**
     * Adds the tag block parser
     *
     * @param string $for
     * @param TagBlockParser $tagBlockParser
     *
     * @return void
     */
    private function addTagBlockParser(string $for, TagBlockParser $tagBlockParser): void
    {
        $this->tagBlockParsers[strtolower($for)] = $tagBlockParser;
    }

    /**
     * Parse that delegates the task to other tag block parsers
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws NoBlockParserForReference
     */
    public function parse(TokenStream $stream, Parser $parser): Token
    {
        if (!$stream->getCurrent() instanceof TagStartToken) {
            throw new UnexpectedToken($stream->getCurrent(), TagStartToken::GROUP);
        }

        if (!$stream->lookahead() instanceof ReferenceToken) {
            throw new UnexpectedToken($stream->lookahead(), ReferenceToken::NAME);
        }

        $block = strtolower($stream->lookahead(1)->getInput());

        if (!array_key_exists($block, $this->tagBlockParsers)) {
            throw new NoBlockParserForReference($block);
        }

        return $this->tagBlockParsers[$block]->parse($stream, $parser);
    }
}