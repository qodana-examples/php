<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Block;

use LessPlate\Lexer\Token\Tag\Block\TagBlockCloseToken;
use LessPlate\Lexer\Token\Tag\Block\TagBlockStartToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Parser;
use LessPlate\Parser\SyntaxTree\IterableSyntaxTree;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\IfBlockToken;
use LessPlate\Parser\Token\Block\ValueObject\Condition;
use LessPlate\Parser\Token\Token;

/**
 * Parser for the if block
 */
final class IfTagBlockParser implements TagBlockParser
{
    /**
     * Sequence to mark else if
     *
     * @var string
     */
    private $elseIfSequence;
    /**
     * Sequence to mark else
     *
     * @var string
     */
    private $elseSequence;
    /**
     * Sequence to mark end
     *
     * @var string
     */
    private $endSequence;

    /**
     * IfTagBlockParser constructor
     *
     * @param string|null $elseIfSequence
     * @param string|null $elseSequence
     * @param string|null $endSequence
     */
    public function __construct(?string $elseIfSequence = null, ?string $elseSequence = null, ?string $endSequence = null)
    {
        $this->elseIfSequence = strtolower($elseIfSequence ?? 'elseif');
        $this->elseSequence = strtolower($elseSequence ?? 'else');
        $this->endSequence = strtolower($endSequence ?? 'endif');
    }

    /**
     * Parse if block
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return Token
     *
     * @throws ParseError
     * @throws UnexpectedToken
     */
    public function parse(TokenStream $stream, Parser $parser): Token
    {
        $conditions = [$this->parseCondition($stream, $parser)];

        while ($this->hasNextCondition($stream)) {
            $conditions[] = $this->parseCondition($stream, $parser);
        }

        if ($this->hasElse($stream)) {
            $stream->consume(3);
            $else = $this->getConditionTree($stream, $parser);
        }

        if (!$this->isEndTag($stream)) {
            throw new UnexpectedToken($stream->getCurrent(), TagBlockStartToken::NAME);
        }

        $stream->consume(3);

        return new IfBlockToken(
            $conditions,
            $else ?? null
        );
    }

    /**
     * Parse condition
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return Condition
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    private function parseCondition(TokenStream $stream, Parser $parser): Condition
    {
        $stream->consume(2);
        $expression = $parser->getConfig()->getExpressionParser()->parse($stream);

        if (!$stream->getCurrent() instanceof TagBlockCloseToken) {
            throw new UnexpectedToken(
                $stream->getCurrent(),
                TagBlockCloseToken::NAME
            );
        }

        $stream->consume();

        return new Condition(
            $expression,
            $this->getConditionTree($stream, $parser)
        );
    }

    /**
     * Returns the syntax tree for the condition
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return SyntaxTree<Token>
     *
     * @throws ParseError
     */
    private function getConditionTree(TokenStream $stream, Parser $parser): SyntaxTree
    {
        $content = [];

        while ($stream->valid() && !$this->isNextPart($stream)) {
            $content[] = $parser->partial($stream);
        }

        return new IterableSyntaxTree($content);
    }

    /**
     * Check if there is a next part
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function isNextPart(TokenStream $stream): bool
    {
        $sequences = [
            $this->elseIfSequence,
            $this->elseSequence,
            $this->endSequence,
        ];

        return
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            in_array(strtolower($stream->lookahead()->getInput()), $sequences);
    }

    /**
     * Does the stream has a next condition
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function hasNextCondition(TokenStream $stream): bool
    {
        return
            $stream->valid()
            &&
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            strtolower($stream->lookahead()->getInput()) === $this->elseIfSequence;
    }

    /**
     * Check if there is an else
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function hasElse(TokenStream $stream): bool
    {
        return
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            strtolower($stream->lookahead()->getInput()) === $this->elseSequence;
    }

    /**
     * Checks if the current is a close tag
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function isEndTag(TokenStream $stream): bool
    {
        return
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            strtolower($stream->lookahead()->getInput()) === $this->endSequence
            &&
            $stream->lookahead(2) instanceof TagBlockCloseToken;
    }
}
