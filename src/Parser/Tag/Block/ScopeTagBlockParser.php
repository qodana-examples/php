<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Block;

use LessPlate\Lexer\Token\Tag\Block\TagBlockCloseToken;
use LessPlate\Lexer\Token\Tag\Block\TagBlockStartToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Parser;
use LessPlate\Parser\SyntaxTree\IterableSyntaxTree;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\ScopeBlockToken;
use LessPlate\Parser\Token\Token;
use RuntimeException;

/**
 * Parser for scope tag
 */
final class ScopeTagBlockParser implements TagBlockParser
{
    /**
     * Sequence to mark end
     *
     * @var string
     */
    private $endSequence;

    /**
     * ScopeBlockParser constructor
     *
     * @param string|null $endSequence
     */
    public function __construct(?string $endSequence = null)
    {
        $this->endSequence = strtolower($endSequence ?? 'endScope');
    }

    /**
     * Parse scope tag
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return Token
     *
     * @throws ParseError
     * @throws UnexpectedToken
     */
    public function parse(TokenStream $stream, Parser $parser): Token
    {
        $stream->consume(2);

        if (!$stream->getCurrent() instanceof ReferenceToken) {
            throw new UnexpectedToken($stream->getCurrent(), ReferenceToken::NAME);
        }

        $as = $stream->getCurrent()->getInput();
        $stream->consume();

        if (!$stream->getCurrent() instanceof ReferenceToken) {
            throw new UnexpectedToken(
                $stream->getCurrent(),
                ReferenceToken::NAME
            );
        }

        if (strtolower($stream->getCurrent()->getInput()) !== 'from') {
            throw new RuntimeException(
                sprintf(
                    'Expected `from`, got "%s"',
                    $stream->getCurrent()->getInput()
                )
            );
        }

        $stream->consume();
        $expression = $parser->getConfig()->getExpressionParser()->parse($stream);

        if (!$stream->getCurrent() instanceof TagBlockCloseToken) {
            throw new UnexpectedToken(
                $stream->getCurrent(),
                TagBlockCloseToken::NAME
            );
        }

        $stream->consume();

        $content = $this->getContent($stream, $parser);

        if (!$this->isEndTag($stream)) {
            throw new UnexpectedToken($stream->getCurrent(), TagBlockCloseToken::NAME);
        }

        $stream->consume(3);

        return new ScopeBlockToken(
            $as,
            $expression,
            $content
        );
    }

    /**
     * Returns the syntax tree for the condition
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return SyntaxTree<Token>
     *
     * @throws ParseError
     */
    private function getContent(TokenStream $stream, Parser $parser): SyntaxTree
    {
        $content = [];

        while ($stream->valid() && !$this->isEndTag($stream)) {
            $content[] = $parser->partial($stream);
        }

        return new IterableSyntaxTree($content);
    }

    /**
     * Checks if the current is a close tag
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function isEndTag(TokenStream $stream): bool
    {
        return
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            strtolower($stream->lookahead()->getInput()) === $this->endSequence
            &&
            $stream->lookahead(2) instanceof TagBlockCloseToken;
    }
}
