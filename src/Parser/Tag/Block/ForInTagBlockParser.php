<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Block;

use LessPlate\Lexer\Token\Specialized\ConcatToken;
use LessPlate\Lexer\Token\Tag\Block\TagBlockCloseToken;
use LessPlate\Lexer\Token\Tag\Block\TagBlockStartToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Parser;
use LessPlate\Parser\SyntaxTree\IterableSyntaxTree;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\ForInBlockToken;
use LessPlate\Parser\Token\Token;
use RuntimeException;

/**
 * Parse for in tag block
 */
final class ForInTagBlockParser implements TagBlockParser
{
    /**
     * Sequence to mark on empty
     *
     * @var string
     */
    private $onEmptySequence;
    /**
     * Sequence to mark end
     *
     * @var string
     */
    private $endSequence;

    /**
     * ForEachTagBlockParser constructor
     *
     * @param string|null $onEmptySequence
     * @param string|null $endSequence
     */
    public function __construct(?string $onEmptySequence = null, ?string $endSequence = null)
    {
        $this->onEmptySequence = strtolower($onEmptySequence ?? 'onEmpty');
        $this->endSequence = strtolower($endSequence ?? 'endfor');
    }

    /**
     * Parse for in tag
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return Token
     *
     * @throws ParseError
     * @throws UnexpectedToken
     */
    public function parse(TokenStream $stream, Parser $parser): Token
    {
        $stream->consume(2);

        if (!$stream->getCurrent() instanceof ReferenceToken) {
            throw new UnexpectedToken($stream->getCurrent(), ReferenceToken::NAME);
        }

        $as = $stream->getCurrent()->getInput();
        $stream->consume();

        if ($stream->getCurrent() instanceof ConcatToken) {
            $stream->consume();

            if (!$stream->getCurrent() instanceof ReferenceToken) {
                throw new UnexpectedToken($stream->getCurrent(), ReferenceToken::NAME);
            }

            $key = $as;
            $as = $stream->getCurrent()->getInput();
            $stream->consume();
        }

        if (!$stream->getCurrent() instanceof ReferenceToken) {
            throw new UnexpectedToken(
                $stream->getCurrent(),
                ReferenceToken::NAME
            );
        }

        if (strtolower($stream->getCurrent()->getInput()) !== 'in') {
            throw new RuntimeException(
                sprintf(
                    'Expected `in`, got "%s"',
                    $stream->getCurrent()->getInput()
                )
            );
        }

        $stream->consume();
        $expression = $parser->getConfig()->getExpressionParser()->parse($stream);

        if (!$stream->getCurrent() instanceof TagBlockCloseToken) {
            throw new UnexpectedToken(
                $stream->getCurrent(),
                TagBlockCloseToken::NAME
            );
        }

        $stream->consume();

        $content = $this->getContent($stream, $parser);

        if ($this->hasOnEmpty($stream)) {
            $stream->consume(3);
            $onEmpty = $this->getContent($stream, $parser);
        }

        if (!$this->isEndTag($stream)) {
            throw new UnexpectedToken($stream->getCurrent(), TagBlockCloseToken::NAME);
        }

        $stream->consume(3);

        return new ForInBlockToken(
            $key ?? null,
            $as,
            $expression,
            $content,
            $onEmpty ?? null
        );
    }

    /**
     * Returns the syntax tree for the condition
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return SyntaxTree
     *
     * @throws ParseError
     */
    private function getContent(TokenStream $stream, Parser $parser): SyntaxTree
    {
        $content = [];

        while ($stream->valid() && !$this->isNextPart($stream)) {
            $content[] = $parser->partial($stream);
        }

        return new IterableSyntaxTree($content);
    }

    /**
     * Check if there is a next part
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function isNextPart(TokenStream $stream): bool
    {
        return
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            in_array(strtolower($stream->lookahead()->getInput()), [$this->onEmptySequence, $this->endSequence]);
    }

    /**
     * Check if there is an else
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function hasOnEmpty(TokenStream $stream): bool
    {
        return
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            strtolower($stream->lookahead()->getInput()) === $this->onEmptySequence;
    }

    /**
     * Checks if the current is a close tag
     *
     * @param TokenStream $stream
     *
     * @return bool
     */
    private function isEndTag(TokenStream $stream): bool
    {
        return
            $stream->getCurrent() instanceof TagBlockStartToken
            &&
            $stream->lookahead() instanceof ReferenceToken
            &&
            strtolower($stream->lookahead()->getInput()) === $this->endSequence
            &&
            $stream->lookahead(2) instanceof TagBlockCloseToken;
    }
}
