<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Block\Exception;

use Exception;

/**
 * Thrown when no block executor is found for the token
 */
final class NoBlockParserForReference extends Exception implements NoBlockParser
{
    /**
     * Reference where no executor is for found
     *
     * @var string
     */
    protected $reference;

    /**
     * NoBlockExecutorForReference constructor
     *
     * @param string $reference
     */
    public function __construct(string $reference)
    {
        parent::__construct(
            sprintf(
                'There is no block parser for the reference "%s"',
                $reference
            )
        );
        $this->reference = $reference;
    }

    /**
     * Returns the reference that was used
     *
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }
}
