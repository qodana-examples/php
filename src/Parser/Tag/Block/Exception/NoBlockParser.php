<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Block\Exception;

use Throwable;

/**
 * Thrown when no block executor is found
 */
interface NoBlockParser extends Throwable
{
}