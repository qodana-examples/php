<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Expression;

use LessPlate\Parser\Tag\TagParser;

/**
 * Parser for tag expression
 */
interface TagExpressionParser  extends TagParser
{
}