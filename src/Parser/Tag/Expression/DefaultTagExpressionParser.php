<?php
declare(strict_types=1);

namespace LessPlate\Parser\Tag\Expression;

use LessPlate\Lexer\Token\Tag\TagCloseToken;
use LessPlate\Lexer\Token\Tag\TagStartToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Parser;
use LessPlate\Parser\Token\Tag\Expression\TagExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Parser for tag expression
 */
final class DefaultTagExpressionParser implements TagExpressionParser
{
    /**
     * Parse tag expression
     *
     * @param TokenStream $stream
     * @param Parser $parser
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    public function parse(TokenStream $stream, Parser $parser): Token
    {
        if (!$stream->getCurrent() instanceof TagStartToken) {
            throw new UnexpectedToken(
                $stream->getCurrent(),
                TagStartToken::GROUP
            );
        }

        $stream->consume();
        $token = $parser->getConfig()->getExpressionParser()->parse($stream);

        if (!$stream->getCurrent() instanceof TagCloseToken) {
            throw new UnexpectedToken(
                $stream->getCurrent(),
                TagCloseToken::class
            );
        }

        $stream->consume();

        return new TagExpressionToken($token);
    }
}