<?php
declare(strict_types=1);

namespace LessPlate\Parser\Exception;

use Exception;
use LessPlate\Lexer\Token\Token;

/**
 * Thrown when an unexpected token was given
 */
final class UnexpectedToken extends Exception implements ParseError
{
    /**
     * Expected tokens
     *
     * @var string[]
     */
    protected $expected;
    /**
     * Actual given tokens
     *
     * @var Token
     */
    protected $got;

    /**
     * UnexpectedToken constructor
     *
     * @param Token $got
     * @param string  ...$expected
     */
    public function __construct(Token $got, string ...$expected)
    {
        parent::__construct(
            sprintf(
                'Expected %s, got %s on line %s:%s',
                implode(' or ', $expected),
                $got->getName(),
                $got->getLine(),
                $got->getColumn()
            )
        );

        $this->expected = $expected;
        $this->got = $got;
    }

    /**
     * Returns the expected tokens
     *
     * @return string[]|iterable
     */
    public function getExpected(): iterable
    {
        return $this->expected;
    }

    /**
     * Actual given token
     *
     * @return Token
     */
    public function getGot(): Token
    {
        return $this->got;
    }
}