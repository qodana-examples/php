<?php
declare(strict_types=1);

namespace LessPlate\Parser\Exception;

use Throwable;

/**
 * Thrown on parse error
 */
interface ParseError extends Throwable
{
}