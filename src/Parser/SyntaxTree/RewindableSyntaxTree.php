<?php
declare(strict_types=1);

namespace LessPlate\Parser\SyntaxTree;

/**
 * Syntax tree that can be rewinded
 *
 * @deprecated is standard
 */
interface RewindableSyntaxTree extends SyntaxTree
{
    /**
     * Rewind syntax tree
     *
     * @return void
     */
    public function rewind(): void;
}
