<?php
declare(strict_types=1);

namespace LessPlate\Parser\SyntaxTree;

use LessPlate\Parser\Token\Token;
use Traversable;

/**
 * Holds the parser tokens
 */
interface SyntaxTree extends Traversable
{
    /**
     * Returns the current token
     *
     * @return Token|null
     *
     * @deprecated use traverable setup
     */
    public function getCurrent(): ?Token;

    /**
     * Consumes the amount of token
     *
     * @param int $amount
     *
     * @return SyntaxTree
     *
     * @deprecated use traverable setup
     */
    public function consume(int $amount = 1): SyntaxTree;
}
