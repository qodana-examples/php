<?php
declare(strict_types=1);

namespace LessPlate\Parser\SyntaxTree;

use ArrayIterator;
use IteratorAggregate;
use LessPlate\Parser\Token\Token;
use Traversable;

/**
 * Cached syntax tree
 *
 * @deprecated use IterableSyntaxTree
 */
final class CachedSyntaxTree implements IteratorAggregate, RewindableSyntaxTree
{
    /**
     * Tokens of the tree
     *
     * @var array|Token[]
     */
    private $tokens = [];
    /**
     * Position of tree
     *
     * @var int
     */
    private $position = 0;

    /**
     * CachedSyntaxTree constructor
     *
     * @param iterable|Token[] $tokens
     */
    public function __construct(iterable $tokens)
    {
        foreach ($tokens as $token) {
            $this->addToken($token);
        }
    }

    /**
     * Returns iterator to use
     *
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->tokens);
    }

    /**
     * Adds token to collection
     *
     * @param Token $token
     *
     * @return void
     */
    private function addToken(Token $token): void
    {
        $this->tokens[] = $token;
    }

    /**
     * Rewind tree
     *
     * @return void
     */
    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * Returns current token
     *
     * @return Token|null
     */
    public function getCurrent(): ?Token
    {
        return $this->tokens[$this->position] ?? null;
    }

    /**
     * Check if there are currently tokens available
     *
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->tokens[$this->position]);
    }

    /**
     * Move position
     *
     * @param int $amount
     *
     * @return SyntaxTree
     */
    public function consume(int $amount = 1): SyntaxTree
    {
        $this->position += $amount;

        return $this;
    }
}
