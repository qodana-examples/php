<?php
declare(strict_types=1);

namespace LessPlate\Parser\SyntaxTree;

use Generator;
use Iterator;
use LessPlate\Parser\Token\Token;

/**
 * Iterable implementation of the SyntaxTree
 */
final class IterableSyntaxTree implements Iterator, SyntaxTree
{
    /**
     * Iterator to get the tokens of
     *
     * @var Generator
     */
    protected $iterable;
    /**
     * Tokens to for the tree
     *
     * @var Token[]
     */
    protected $tokens = [];
    /**
     * Position in tree
     *
     * @var int
     */
    protected $position = 0;

    /**
     * IterableSyntaxTree constructor
     *
     * @param iterable|Token[] $iterable
     */
    public function __construct(iterable $iterable)
    {
        $this->iterable = $this->getGenerator($iterable);
        $this->load();
    }

    /**
     * Returns token
     *
     * @return Token|null
     */
    public function current(): ?Token
    {
        return $this->tokens[$this->position] ?? null;
    }

    /**
     * Moves to next token
     *
     * @return void
     */
    public function next(): void
    {
        $this->load();
        $this->position += 1;
    }

    /**
     * Returns key
     *
     * @return int
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * Rewind tree
     *
     * @return void
     */
    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * Returns the generator from the iterable
     *
     * @param iterable|Token[] $iterable
     *
     * @return Generator
     */
    private function getGenerator(iterable $iterable): Generator
    {
        foreach ($iterable as $item) {
            yield $item;
        }
    }

    /**
     * Return the current token
     *
     * @return Token|null
     *
     * @deprecated will be droped
     */
    public function getCurrent(): ?Token
    {
        return $this->tokens[$this->position] ?? null;
    }

    /**
     * Checks if there is a token
     *
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->tokens[$this->position]);
    }

    /**
     * Consume tokens
     *
     * @param int $amount
     *
     * @return SyntaxTree
     *
     * @deprecated will be dropped
     */
    public function consume(int $amount = 1): SyntaxTree
    {
        $needed = $amount - count($this->tokens) + 1;

        if ($needed > 0) {
            $this->load($needed);
        }

        $this->tokens = array_slice($this->tokens, $amount);

        return $this;
    }

    /**
     * Load tokens
     *
     * @param int $amount
     *
     * @return void
     *
     * @todo check amount is positive
     */
    private function load(int $amount = 1): void
    {
        while ($this->iterable->valid() && $amount > 0) {
            $this->tokens[] = $this->iterable->current();
            $this->iterable->next();

            $amount -= 1;
        }
    }
}
