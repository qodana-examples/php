<?php
declare(strict_types=1);

namespace LessPlate\Parser;

use LessPlate\Config\Parser\ParserConfig;
use LessPlate\Lexer\Token\Tag\Block\TagBlockStartToken;
use LessPlate\Lexer\Token\Tag\Expression\TagExpressionStartToken;
use LessPlate\Lexer\Token\Tag\TagStartToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\SyntaxTree\IterableSyntaxTree;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\TextToken;
use LessPlate\Parser\Token\Token;
use RuntimeException;

/**
 * Lazy parse to the syntax tree
 */
final class LazyParser implements Parser
{
    /**
     * Config to use
     *
     * @var ParserConfig
     */
    private $config;

    /**
     * LazyParser constructor
     *
     * @param ParserConfig $config
     */
    public function __construct(ParserConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Returns config from LazyParser
     *
     * @return ParserConfig
     */
    public function getConfig(): ParserConfig
    {
        return $this->config;
    }

    /**
     * Parse the token stream to the syntax tree
     *
     * @param TokenStream $stream
     *
     * @return SyntaxTree
     */
    public function parse(TokenStream $stream): SyntaxTree
    {
        return new IterableSyntaxTree(
            (function () use ($stream) {
                while ($stream->valid()) {
                    yield $this->partial($stream);
                }
            })()
        );
    }

    /**
     * Only parse to the first token
     *
     * @param TokenStream $stream
     *
     * @return Token
     */
    public function partial(TokenStream $stream): Token
    {
        if ($stream->getCurrent() instanceof TagStartToken) {
            return $this->tag($stream);
        }

        return $this->text($stream);
    }

    /**
     * Parse token
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @todo use custom exception
     */
    private function tag(TokenStream $stream): Token
    {
        $parsers = $this->config->getTagParsers();
        $name = $stream->getCurrent()->getName();

        if (!array_key_exists($name, $parsers)) {
            throw new RuntimeException('Unknown tag');
        }

        return $parsers[$name]->parse(
            $stream, $this
        );
    }

    /**
     * Parse text
     *
     * @param TokenStream $stream
     *
     * @return Token
     */
    private function text(TokenStream $stream): Token
    {
        $text = '';

        while ($stream->valid()) {
            $current = $stream->getCurrent();

            if ($current instanceof TagBlockStartToken || $current instanceof TagExpressionStartToken) {
                break;
            }

            $text .= $current->getInput();
            $stream->consume();
        }

        return new TextToken($text);
    }
}