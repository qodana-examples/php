<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Value;

/**
 * String value token
 */
final class StringValueToken implements ValueToken
{
    public const NAME = 'STRING';

    /**
     * Actual string
     *
     * @var string
     */
    protected $string;

    /**
     * StringToken constructor
     *
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
    }

    /**
     * Returns string value
     *
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}