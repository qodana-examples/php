<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Value;

use LessPlate\Parser\Token\Token;

/**
 * Marks a token as the value type
 */
interface ValueToken extends Token
{
    public const TYPE = 'VALUE_TOKEN';
}
