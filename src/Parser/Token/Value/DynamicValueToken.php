<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Value;

use LessPlate\Parser\Token\Util\AccessorToken;

/**
 * Dynamic value token
 */
final class DynamicValueToken implements ValueToken
{
    public const NAME = 'DYNAMIC';

    /**
     * Reference for the dynamic value
     *
     * @var string
     */
    protected $reference;
    /**
     * If the value is optional
     *
     * @var bool
     */
    private $optional;
    /**
     * Holds the accessors to use on the reference value
     *
     * @var array|AccessorToken[]
     */
    private $accessors = [];

    /**
     * DynamicValueToken constructor
     *
     * @param string $reference
     * @param bool $optional
     * @param iterable|AccessorToken[] $accessors
     */
    public function __construct(string $reference, bool $optional, iterable $accessors)
    {
        $this->reference = $reference;
        $this->optional = $optional;

        foreach ($accessors as $accessor) {
            $this->addAccessor($accessor);
        }
    }

    /**
     * Adds the accessor
     *
     * @param AccessorToken $valueToken
     *
     * @return void
     */
    private function addAccessor(AccessorToken $valueToken): void
    {
        $this->accessors[] = $valueToken;
    }

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Returns the dynamic value
     *
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * Returns optional from DynamicValueToken
     *
     * @return bool
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * Returns accessors from DynamicValueToken
     *
     * @return iterable|AccessorToken[]
     */
    public function getAccessors(): iterable
    {
        return $this->accessors;
    }
}