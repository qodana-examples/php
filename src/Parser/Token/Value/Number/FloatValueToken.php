<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Value\Number;

/**
 * Float number value token
 */
final class FloatValueToken implements ValueNumberToken
{
    public const NAME = 'FLOAT';
    /**
     * Matched token
     *
     * @var string
     */
    protected $float;

    /**
     * FloatToken constructor
     *
     * @param string $float
     */
    public function __construct(string $float)
    {
        $this->float = $float;
    }

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Returns the matched float
     *
     * @return string
     */
    public function getFloat(): string
    {
        return $this->float;
    }
}