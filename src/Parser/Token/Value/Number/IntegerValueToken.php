<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Value\Number;

/**
 * Integer value number token
 */
final class IntegerValueToken implements ValueNumberToken
{
    public const NAME = 'INTEGER';
    /**
     * Matched integer
     *
     * @var string
     */
    protected $integer;

    /**
     * IntegerToken constructor
     *
     * @param string $integer
     */
    public function __construct(string $integer)
    {
        $this->integer = $integer;
    }

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Returns the matched integer
     *
     * @return string
     */
    public function getInteger(): string
    {
        return $this->integer;
    }
}