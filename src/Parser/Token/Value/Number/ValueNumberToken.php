<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Value\Number;

use LessPlate\Parser\Token\Value\ValueToken;

/**
 * Marks a token value as a number
 */
interface ValueNumberToken extends ValueToken
{
}