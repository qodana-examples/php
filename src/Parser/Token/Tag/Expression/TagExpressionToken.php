<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Tag\Expression;

use LessPlate\Parser\Token\Token;

/**
 * Tag token
 */
final class TagExpressionToken implements Token
{
    public const NAME = 'TAG';

    /**
     * Token expression
     *
     * @var Token
     */
    protected $expression;

    /**
     * TagToken constructor
     *
     * @param Token $expression
     */
    public function __construct(Token $expression)
    {
        $this->expression = $expression;
    }

    /**
     * Returns matched expression
     *
     * @return Token
     */
    public function getExpression(): Token
    {
        return $this->expression;
    }

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}