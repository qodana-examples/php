<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Block\ValueObject;

use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Token;

/**
 * Condition value object
 */
final class Condition
{
    /**
     * Holds the expression
     *
     * @var Token
     */
    private $expression;
    /**
     * Holds the syntax tree
     *
     * @var SyntaxTree<Token>
     */
    private $content;

    /**
     * Condition constructor
     *
     * @param Token $expression
     * @param SyntaxTree<Token> $content
     */
    public function __construct(Token $expression, SyntaxTree $content)
    {
        $this->expression = $expression;
        $this->content = $content;
    }

    /**
     * Returns the expression to use
     *
     * @return Token
     */
    public function getExpression(): Token
    {
        return $this->expression;
    }

    /**
     * Content to use if the expression is valid
     *
     * @return SyntaxTree<Token>
     */
    public function getContent(): SyntaxTree
    {
        return $this->content;
    }
}