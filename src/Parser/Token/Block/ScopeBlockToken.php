<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Block;

use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Token;

/**
 * Token for scope block
 */
final class ScopeBlockToken implements BlockToken
{
    public const NAME = 'SCOPE_BLOCK';

    /**
     * Context key for the expression result
     *
     * @var string
     */
    private $as;
    /**
     * Holds the expression
     *
     * @var Token
     */
    private $from;
    /**
     * Content to render
     *
     * @var SyntaxTree<Token>
     */
    private $content;

    /**
     * ScopeToken constructor
     *
     * @param string $as
     * @param Token $from
     * @param SyntaxTree<Token> $content
     */
    public function __construct(string $as, Token $from, SyntaxTree $content)
    {
        $this->as = $as;
        $this->from = $from;
        $this->content = $content;
    }

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Returns as from ScopeToken
     *
     * @return string
     */
    public function getAs(): string
    {
        return $this->as;
    }

    /**
     * Returns from from ScopeToken
     *
     * @return Token
     */
    public function getFrom(): Token
    {
        return $this->from;
    }

    /**
     * Returns content from ScopeToken
     *
     * @return SyntaxTree<Token>
     */
    public function getContent(): SyntaxTree
    {
        return $this->content;
    }
}