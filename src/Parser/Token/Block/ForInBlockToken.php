<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Block;

use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Token;

/**
 * Token for in block
 */
final class ForInBlockToken implements BlockToken
{
    public const NAME = 'FOR_IN_BLOCK';

    /**
     * Context key for key
     *
     * @var string|null
     */
    private $key;
    /**
     * Context key for value
     *
     * @var string
     */
    private $as;
    /**
     * Holds the expression
     *
     * @var Token
     */
    private $expression;
    /**
     * Content to render
     *
     * @var SyntaxTree
     */
    private $content;
    /**
     * Content to render on empty
     *
     * @var SyntaxTree|null
     */
    private $onEmpty;

    /**
     * ForInBlockToken constructor
     *
     * @param string|null $key
     * @param string $as
     * @param Token $expression
     * @param SyntaxTree<Token> $content
     * @param SyntaxTree<Token>|null $onEmpty
     */
    public function __construct(?string $key, string $as, Token $expression, SyntaxTree $content, ?SyntaxTree $onEmpty)
    {
        $this->key = $key;
        $this->as = $as;
        $this->expression = $expression;
        $this->content = $content;
        $this->onEmpty = $onEmpty;
    }

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Returns key from ForInBlockToken
     *
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->key;
    }

    /**
     * Returns as from ForInBlockToken
     *
     * @return string
     */
    public function getAs(): string
    {
        return $this->as;
    }

    /**
     * Returns expression from ForInBlockToken
     *
     * @return Token
     */
    public function getExpression(): Token
    {
        return $this->expression;
    }

    /**
     * Returns content from ForInBlockToken
     *
     * @return SyntaxTree<Token>
     */
    public function getContent(): SyntaxTree
    {
        return $this->content;
    }

    /**
     * Returns onEmpty from ForInBlockToken
     *
     * @return SyntaxTree<Token>|null
     */
    public function getOnEmpty(): ?SyntaxTree
    {
        return $this->onEmpty;
    }
}
