<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Block;

use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\ValueObject\Condition;
use LessPlate\Parser\Token\Token;

/**
 * Token to mark an if block
 */
final class IfBlockToken implements BlockToken
{
    public const NAME = 'IF_BLOCK';

    /**
     * Conditions to use
     *
     * @var iterable|Condition[]
     */
    protected $conditions = [];
    /**
     * To be used when all conditions fail
     *
     * @var SyntaxTree<Token>|null
     */
    protected $else;

    /**
     * IfBlockToken constructor
     *
     * @param iterable|Condition[] $conditions
     * @param SyntaxTree<Token>|null $else
     */
    public function __construct(iterable $conditions, ?SyntaxTree $else)
    {
        foreach ($conditions as $condition) {
            $this->addCondition($condition);
        }

        $this->else = $else;
    }

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Adds the condition to the stack
     *
     * @param Condition $condition
     *
     * @return void
     */
    protected function addCondition(Condition $condition): void
    {
        $this->conditions[] = $condition;
    }

    /**
     * Returns the conditions to be used
     *
     * @return iterable|Condition[]
     */
    public function getConditions(): iterable
    {
        return $this->conditions;
    }

    /**
     * Returns the else to be used
     *
     * @return SyntaxTree<Token>|null
     */
    public function getElse(): ?SyntaxTree
    {
        return $this->else;
    }
}