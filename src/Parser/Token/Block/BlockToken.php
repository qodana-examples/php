<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Block;

use LessPlate\Parser\Token\Token;

/**
 * Block token
 */
interface BlockToken extends Token
{
    public const GROUP = 'BLOCK_TOKEN';
}