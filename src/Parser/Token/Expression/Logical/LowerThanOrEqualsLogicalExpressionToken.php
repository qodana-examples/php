<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Lower than or equals logical expression token
 */
final class LowerThanOrEqualsLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'LOWER_THAN_OR_EQUALS_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}