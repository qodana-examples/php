<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Token to mark the greater than logical
 */
final class GreaterThanLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'GREATER_THAN_EXPRESSION';

    /**
     * Returns the name of the token
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}