<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Token to mark an equals logical expression
 */
final class EqualsLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'EQUALS_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}