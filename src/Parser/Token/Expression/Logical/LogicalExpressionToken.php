<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Expression\ExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Mark a token as an logical expression
 */
interface LogicalExpressionToken extends ExpressionToken
{
    public const TYPE = 'LOGICAL_EXPRESSION';

    /**
     * Returns the left token for the expression
     *
     * @return Token
     */
    public function getLeft(): Token;

    /**
     * Returns the right token for the expression
     *
     * @return Token
     */
    public function getRight(): Token;
}