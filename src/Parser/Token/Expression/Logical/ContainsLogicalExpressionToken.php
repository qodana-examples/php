<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Token to mark an in logical expression
 */
final class ContainsLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'CONTAINS_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
