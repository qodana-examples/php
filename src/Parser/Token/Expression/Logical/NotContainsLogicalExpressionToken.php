<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Token to mark a not contains logical expression
 */
final class NotContainsLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'NOT_CONTAINS_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
