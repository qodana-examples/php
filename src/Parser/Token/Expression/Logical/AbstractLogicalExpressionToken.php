<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

use LessPlate\Parser\Token\Token;

/**
 * Adds the left and right token
 */
abstract class AbstractLogicalExpressionToken implements LogicalExpressionToken
{
    /**
     * Left token
     *
     * @var Token
     */
    protected $left;
    /**
     * Right token
     *
     * @var Token
     */
    protected $right;

    /**
     * AbstractLogicalExpressionToken constructor
     *
     * @param Token $left
     * @param Token $right
     */
    public function __construct(Token $left, Token $right)
    {
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * Returns the left token
     *
     * @return Token
     */
    public function getLeft(): Token
    {
        return $this->left;
    }

    /**
     * Returns the right token
     *
     * @return Token
     */
    public function getRight(): Token
    {
        return $this->right;
    }
}