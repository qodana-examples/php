<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Token to mark the greater than or equals logical expression
 */
final class GreaterThanOrEqualsLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'GREATER_THAN_OR_EQUALS_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}