<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Token to mark lower than logical
 */
final class LowerThanLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'LOWER_THAN_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}