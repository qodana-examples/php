<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Logical;

/**
 * Token to mark not equals logical
 */
final class NotEqualsLogicalExpressionToken extends AbstractLogicalExpressionToken
{
    public const NAME = 'NOT_EQUALS_EXPRESSION';

    /**
     * Returns the toke name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}