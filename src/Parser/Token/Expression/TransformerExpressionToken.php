<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression;

use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Util\TransformerToken;

/**
 * Marks a transformer tokens
 */
final class TransformerExpressionToken implements ExpressionToken
{
    /**
     * Value to be transformed
     *
     * @var Token
     */
    protected $value;
    /**
     * Transformers to apply
     *
     * @var iterable|TransformerToken[]
     */
    protected $transformers = [];

    public const NAME = 'TRANSFORMER_EXPRESSION';

    /**
     * TransformerExpressionToken constructor
     *
     * @param Token $value
     * @param iterable|TransformerToken[] $transformers
     */
    public function __construct(Token $value, iterable $transformers)
    {
        $this->value = $value;

        foreach ($transformers as $parameter) {
            $this->addTransformer($parameter);
        }
    }

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Adds the transformer to the stack
     *
     * @param TransformerToken $transformer
     *
     * @return void
     */
    protected function addTransformer(TransformerToken $transformer): void
    {
        $this->transformers[] = $transformer;
    }

    /**
     * Returns the value to use
     *
     * @return Token
     */
    public function getValue(): Token
    {
        return $this->value;
    }

    /**
     * Returns the transformers to use
     *
     * @return iterable|TransformerToken[]
     */
    public function getTransformers(): iterable
    {
        return $this->transformers;
    }
}