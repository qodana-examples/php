<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

/**
 * Token to mark a power arithmetic expression
 */
final class PowerArithmeticExpressionToken extends AbstractArithmeticExpressionToken
{
    public const NAME = 'POWER_ARITHMETIC_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}