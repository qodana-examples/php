<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

/**
 * Token for addition
 */
final class AdditionArithmeticExpressionToken extends AbstractArithmeticExpressionToken
{
    public const NAME = 'ADDITION_ARITHMETIC_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}