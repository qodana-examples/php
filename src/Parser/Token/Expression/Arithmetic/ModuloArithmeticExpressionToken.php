<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

/**
 * Token for modulo
 */
final class ModuloArithmeticExpressionToken extends AbstractArithmeticExpressionToken
{
    public const NAME = 'MODULO_ARITHMETIC_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}