<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

/**
 * Token for divide
 */
final class DivideArithmeticExpressionToken extends AbstractArithmeticExpressionToken
{
    public const NAME = 'DIVIDE_ARITHMETIC_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}