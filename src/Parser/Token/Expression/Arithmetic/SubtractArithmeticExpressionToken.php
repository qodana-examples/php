<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

/**
 * Token for subtract
 */
final class SubtractArithmeticExpressionToken extends AbstractArithmeticExpressionToken
{
    public const NAME = 'SUBTRACT_ARITHMETIC_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}