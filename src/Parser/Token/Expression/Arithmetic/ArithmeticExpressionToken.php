<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Expression\ExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Marks a token as an arithmetic expression
 */
interface ArithmeticExpressionToken extends ExpressionToken
{
    public const TYPE = 'ARITHMETIC_EXPRESSION';

    /**
     * Returns the left part of the expression
     *
     * @return Token
     */
    public function getLeft(): Token;

    /**
     * Returns the right part of the expression
     *
     * @return Token
     */
    public function getRight(): Token;
}