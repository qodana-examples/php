<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

/**
 * Token for multiply
 */
final class MultiplyArithmeticExpressionToken extends AbstractArithmeticExpressionToken
{
    public const NAME = 'MULTIPLY_ARITHMETIC_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}