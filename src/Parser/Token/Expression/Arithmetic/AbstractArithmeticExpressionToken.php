<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Arithmetic;

use LessPlate\Parser\Token\Token;

/**
 * Adds the base methods for the arithmetic expressions
 */
abstract class AbstractArithmeticExpressionToken implements ArithmeticExpressionToken
{
    /**
     * Holds the left token
     *
     * @var Token
     */
    protected $left;
    /**
     * Holds the right token
     *
     * @var Token
     */
    protected $right;

    /**
     * AbstractArithmeticExpressionToken constructor
     *
     * @param Token $left
     * @param Token $right
     */
    public function __construct(Token $left, Token $right)
    {
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * Returns the left token
     *
     * @return Token
     */
    public function getLeft(): Token
    {
        return $this->left;
    }

    /**
     * Returns the right token
     *
     * @return Token
     */
    public function getRight(): Token
    {
        return $this->right;
    }
}