<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Chain;

use LessPlate\Parser\Token\Expression\ExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Chain expression token
 */
interface ChainExpressionToken extends ExpressionToken
{
    public const TYPE = 'CHAIN_EXPRESSION';

    /**
     * Returns left part of expression
     *
     * @return Token
     */
    public function getLeft(): Token;

    /**
     * Returns right part of expression
     *
     * @return Token
     */
    public function getRight(): Token;
}