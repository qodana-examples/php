<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Chain;

use LessPlate\Parser\Token\Token;

/**
 * Adds the left and right
 */
abstract class AbstractChainExpressionToken implements ChainExpressionToken
{
    /**
     * Left part of chain expression
     *
     * @var Token
     */
    protected $left;
    /**
     * Right part of chain expression
     *
     * @var Token
     */
    protected $right;

    /**
     * AbstractLogicalExpressionToken constructor
     *
     * @param Token $left
     * @param Token $right
     */
    public function __construct(Token $left, Token $right)
    {
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * Returns left token
     *
     * @return Token
     */
    public function getLeft(): Token
    {
        return $this->left;
    }

    /**
     * Returns right token
     *
     * @return Token
     */
    public function getRight(): Token
    {
        return $this->right;
    }
}