<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Chain;

/**
 * Token for and logical expression
 */
final class AndLogicalExpressionToken extends AbstractChainExpressionToken
{
    public const NAME = 'AND_EXPRESSION';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}