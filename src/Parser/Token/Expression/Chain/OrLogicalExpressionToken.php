<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression\Chain;

/**
 * Token for or logical expressions
 */
final class OrLogicalExpressionToken extends AbstractChainExpressionToken
{
    public const NAME = 'OR_EXPRESSION';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}