<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Expression;

use LessPlate\Parser\Token\Token;

/**
 * Marks a token as an expression
 */
interface ExpressionToken extends Token
{
}