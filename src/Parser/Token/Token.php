<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token;

/**
 * Parser token
 */
interface Token
{
    /**
     * Name of the token
     *
     * @return string
     */
    public function getName(): string;
}