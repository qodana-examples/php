<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Util;

use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Value\ValueToken;

/**
 * Transformer token
 */
final class TransformerToken implements Token
{
    public const NAME = 'TRANSFORMER';

    /**
     * Transformer reference
     *
     * @var string
     */
    protected $reference;
    /**
     * Transformer parameters
     *
     * @var iterable|ValueToken[]
     */
    protected $parameters = [];

    /**
     * TransformerExpressionToken constructor
     *
     * @param string $reference
     * @param iterable|ValueToken[] $parameters
     */
    public function __construct(string $reference, iterable $parameters)
    {
        $this->reference = $reference;

        foreach ($parameters as $parameter) {
            $this->addParameter($parameter);
        }
    }

    /**
     * Returns the transformer name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Adds the transformer parameter
     *
     * @param Token $parameter
     *
     * @return void
     */
    protected function addParameter(Token $parameter): void
    {
        $this->parameters[] = $parameter;
    }

    /**
     * Returns the transformer reference
     *
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * Returns the parameters for the transformer
     *
     * @return iterable|ValueToken[]
     */
    public function getParameters(): iterable
    {
        return $this->parameters;
    }
}