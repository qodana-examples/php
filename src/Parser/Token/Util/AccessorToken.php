<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token\Util;

use LessPlate\Parser\Token\Token;

/**
 * Token to mark accessor
 */
final class AccessorToken implements Token
{
    public const NAME = 'ACCESSOR';

    /**
     * Accessor to use
     *
     * @var Token
     */
    private $expression;
    /**
     * If the accessor is optional
     *
     * @var bool
     */
    private $optional;

    /**
     * AccessorToken constructor
     *
     * @param Token $expression
     * @param bool $optional
     */
    public function __construct(Token $expression, bool $optional)
    {
        $this->expression = $expression;
        $this->optional = $optional;
    }

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Returns expression from AccessorToken
     *
     * @return Token
     */
    public function getExpression(): Token
    {
        return $this->expression;
    }

    /**
     * Returns optional from AccessorToken
     *
     * @return bool
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }
}