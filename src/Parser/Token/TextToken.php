<?php
declare(strict_types=1);

namespace LessPlate\Parser\Token;

/**
 * Token for text
 */
final class TextToken implements Token
{
    public const NAME = 'TEXT';

    /**
     * Text for the token
     *
     * @var string
     */
    protected $text;

    /**
     * TextToken constructor
     *
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * Returns the matched text
     *
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}