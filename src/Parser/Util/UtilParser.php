<?php
declare(strict_types=1);

namespace LessPlate\Parser\Util;

use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\Token\Token;

/**
 * Utility parser
 */
interface UtilParser
{
    /**
     * Parse the token stream to the token
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws ParseError
     */
    public function parse(TokenStream $stream): Token;
}