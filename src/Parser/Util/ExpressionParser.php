<?php
declare(strict_types=1);

namespace LessPlate\Parser\Util;

use LessPlate\Config\Parser\Arithmetic\ParserArithmeticConfig;
use LessPlate\Config\Parser\Chain\ParserChainConfig;
use LessPlate\Config\Parser\Logical\ParserLogicalConfig;
use LessPlate\Lexer\Token\EofToken;
use LessPlate\Lexer\Token\Operator\ArithmeticOperatorToken;
use LessPlate\Lexer\Token\Operator\ChainOperatorToken;
use LessPlate\Lexer\Token\Operator\LogicalOperatorToken;
use LessPlate\Lexer\Token\Operator\OperatorToken;
use LessPlate\Lexer\Token\Specialized\ConcatToken;
use LessPlate\Lexer\Token\Specialized\Group\GroupCloseToken;
use LessPlate\Lexer\Token\Specialized\Group\GroupStartToken;
use LessPlate\Lexer\Token\Specialized\TransformerToken as LexerTransformerToken;
use LessPlate\Lexer\Token\Token as LexerToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Token\Expression\TransformerExpressionToken;
use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Util\TransformerToken;

/**
 * Expression parser
 */
final class ExpressionParser implements UtilParser
{
    /**
     * Holds the value parser
     *
     * @var UtilParser
     */
    protected $valueParser;
    /**
     * Holds the parser chain configs
     *
     * @var array|ParserChainConfig[]
     */
    private $parserChainConfigs = [];
    /**
     * Holds the logical operators
     *
     * @var array|ParserLogicalConfig[]
     */
    protected $parserLogicalConfigs = [];
    /**
     * Holds the logical operators
     *
     * @var array|ParserArithmeticConfig[]
     */
    protected $parserArithmeticConfigs = [];

    /**
     * ExpressionParser constructor
     *
     * @param UtilParser $valueParser
     * @param iterable|ParserChainConfig[] $parserChainConfigs
     * @param iterable|ParserLogicalConfig[] $parserLogicalConfigs
     * @param iterable|ParserArithmeticConfig[] $parserArithmeticConfigs
     */
    public function __construct(UtilParser $valueParser, iterable $parserChainConfigs, iterable $parserLogicalConfigs, iterable $parserArithmeticConfigs)
    {
        $this->valueParser = $valueParser;

        foreach ($parserChainConfigs as $parserChainConfig) {
            $this->addParserChainConfig($parserChainConfig);
        }

        foreach ($parserLogicalConfigs as $parserLogicalConfig) {
            $this->addParserLogicalConfig($parserLogicalConfig);
        }

        foreach ($parserArithmeticConfigs as $parserArithmeticConfig) {
            $this->addParserArithmeticConfig($parserArithmeticConfig);
        }
    }

    /**
     * Adds the parser chain config
     *
     * @param ParserChainConfig $config
     *
     * @return void
     */
    private function addParserChainConfig(ParserChainConfig $config): void
    {
        $this->parserChainConfigs[$config->getSequence()] = $config;
    }

    /**
     * Adds the logical operator config
     *
     * @param ParserLogicalConfig $config
     *
     * @return void
     */
    private function addParserLogicalConfig(ParserLogicalConfig $config): void
    {
        $this->parserLogicalConfigs[$config->getSequence()] = $config;
    }

    /**
     * Adds the logical operator config
     *
     * @param ParserArithmeticConfig $config
     *
     * @return void
     */
    private function addParserArithmeticConfig(ParserArithmeticConfig $config): void
    {
        $this->parserArithmeticConfigs[$config->getSequence()] = $config;
    }

    /**
     * Parse for an expression
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    public function parse(TokenStream $stream): Token
    {
        return $this->getTransformerPart(
            $stream,
            $this->chainExpression($stream)
        );
    }

    /**
     * Return a chain part
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    public function getChainPart(TokenStream $stream): Token
    {
        $isChainParentheses = false;

        if ($stream->getCurrent() instanceof GroupStartToken) {
            $ahead = 2;

            $isChainParentheses = false;

            while (!$stream->lookahead($ahead) instanceof EofToken) {
                $lookahead = $stream->lookahead($ahead);

                if ($lookahead instanceof OperatorToken && !$lookahead instanceof ChainOperatorToken) {
                    break;
                }

                if ($stream->lookahead($ahead) instanceof GroupCloseToken) {
                    $isChainParentheses = true;

                    break;
                }

                $ahead += 1;
            }
        }

        if ($isChainParentheses) {
            $stream->consume();
            $part = $this->chainExpression($stream);

            $stream->consume();
        } else {
            $part = $this->logicalOperator($stream);
        }

        return $part;
    }

    /**
     * Parse and expression
     *
     * @param TokenStream $stream
     * @param Token|null $left
     * @param int|null $minPriority
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    private function chainExpression(TokenStream $stream, ?Token $left = null, ?int $minPriority = null): Token
    {
        $left = $left ?? $this->getChainPart($stream);
        $current = $stream->getCurrent();

        while ($this->isNextChain($current, $minPriority)) {
            $config = $this->parserChainConfigs[$current->getInput()];
            $stream->consume();

            $right = $this->getChainPart($stream);

            $next = $stream->getCurrent();

            if ($this->isNextChain($next, $config->getPriority())) {
                $right = $this->chainExpression(
                    $stream,
                    $right,
                    $config->getPriority() + 1
                );
            }

            $left = $config->makeToken($left, $right);

            $current = $stream->getCurrent();
        }

        return $left;
    }

    /**
     * Check if the next is an allowed chain
     *
     * @param LexerToken $token
     * @param int|null $minPriority
     *
     * @return bool
     */
    private function isNextChain(LexerToken $token, ?int $minPriority): bool
    {
        return $token instanceof ChainOperatorToken && $this->parserChainConfigs[$token->getInput()]->getPriority() > $minPriority;
    }

    /**
     * Returns the logical part
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws ParseError
     * @throws UnexpectedToken
     */
    private function getLogicalPart(TokenStream $stream): Token
    {
        $isChainParentheses = false;

        if ($stream->getCurrent() instanceof GroupStartToken) {
            $ahead = 2;

            $isChainParentheses = false;

            while (!$stream->lookahead($ahead) instanceof EofToken) {
                $lookahead = $stream->lookahead($ahead);

                if ($lookahead instanceof OperatorToken && !$lookahead instanceof LogicalOperatorToken) {
                    break;
                }

                if ($stream->lookahead($ahead) instanceof GroupCloseToken) {
                    $isChainParentheses = true;

                    break;
                }

                $ahead += 1;
            }
        }

        if ($isChainParentheses) {
            $stream->consume();
            $part = $this->logicalOperator($stream);

            $stream->consume();
        } else {
            $part = $this->arithmeticExpression($stream);
        }

        return $part;
    }

    /**
     * Parse a logical operator
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    private function logicalOperator(TokenStream $stream): Token
    {
        $part = $this->getLogicalPart($stream);

        if ($stream->getCurrent() instanceof LogicalOperatorToken) {
            $config = $this->parserLogicalConfigs[$stream->getCurrent()->getInput()];
            $stream->consume();

            $part = $config->makeToken(
                $part,
                $this->getLogicalPart($stream)
            );
        }

        return $part;
    }

    /**
     * Return an arithmetic part
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    private function getArithmeticPart(TokenStream $stream): Token
    {
        if ($stream->getCurrent() instanceof GroupStartToken) {
            $stream->consume();
            $part = $this->arithmeticExpression($stream);

            if (!$stream->getCurrent() instanceof GroupCloseToken) {
                throw new UnexpectedToken(
                    $stream->getCurrent(),
                    GroupCloseToken::NAME
                );
            }

            $stream->consume();
        } else {
            $part = $this->getTransformerPart($stream);
        }

        return $part;
    }

    /**
     * Parse a power expression
     *
     * @param TokenStream $stream
     * @param Token|null $left
     * @param int|null $minPriority
     *
     * @return Token
     *
     * @throws UnexpectedToken
     * @throws ParseError
     */
    private function arithmeticExpression(TokenStream $stream, ?Token $left = null, ?int $minPriority = null): Token
    {
        $left = $left ?? $this->getArithmeticPart($stream);
        $current = $stream->getCurrent();

        while ($this->isNextArithmetic($current, $minPriority)) {
            $config = $this->parserArithmeticConfigs[$current->getInput()];
            $stream->consume();

            $right = $this->getArithmeticPart($stream);

            $next = $stream->getCurrent();

            if ($this->isNextArithmetic($next, $config->getPriority())) {
                $right = $this->arithmeticExpression(
                    $stream,
                    $right,
                    $config->getPriority() + 1
                );
            }

            $left = $config->makeToken($left, $right);

            $current = $stream->getCurrent();
        }

        return $left;
    }

    /**
     * Check if the next is an allowed arithmetic token
     *
     * @param LexerToken $token
     * @param int|null $minPriority
     *
     * @return bool
     */
    private function isNextArithmetic(LexerToken $token, ?int $minPriority): bool
    {
        return $token instanceof ArithmeticOperatorToken && $this->parserArithmeticConfigs[$token->getInput()]->getPriority() > $minPriority;
    }

    /**
     * Parse transformer part
     *
     * @param TokenStream $stream
     * @param Token $part
     *
     * @return Token
     *
     * @throws ParseError
     * @throws UnexpectedToken
     */
    private function getTransformerPart(TokenStream $stream, ?Token $part = null): Token
    {
        $part = $part ?? $this->value($stream);
        $transformers = [];

        while ($stream->getCurrent() instanceof LexerTransformerToken) {
            $stream->consume();

            if (!$stream->getCurrent() instanceof ReferenceToken) {
                throw new UnexpectedToken(
                    $stream->getCurrent(),
                    ReferenceToken::NAME
                );
            }

            $reference = $stream->getCurrent()->getInput();
            $stream->consume();
            $parameters = [];

            if ($stream->getCurrent() instanceof GroupStartToken) {
                $stream->consume();

                if (!$stream->getCurrent() instanceof GroupCloseToken) {
                    $parameters[] = $this->chainExpression($stream);

                    while ($stream->getCurrent() instanceof ConcatToken) {
                        $stream->consume();
                        $parameters[] = $this->chainExpression($stream);
                    }

                    if (!$stream->getCurrent() instanceof GroupCloseToken) {
                        throw new UnexpectedToken(
                            $stream->getCurrent(),
                            GroupCloseToken::NAME
                        );
                    }
                }

                $stream->consume();
            }

            $transformers[] = new TransformerToken(
                $reference,
                $parameters
            );
        }

        if (count($transformers)) {
            $part = new TransformerExpressionToken(
                $part,
                $transformers
            );
        }

        return $part;
    }

    /**
     * Parse a value
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws ParseError
     */
    private function value(TokenStream $stream): Token
    {
        return $this->getValueParser()->parse($stream);
    }

    /**
     * Returns utility parser to parse a value with
     *
     * @return UtilParser
     */
    private function getValueParser(): UtilParser
    {
        return $this->valueParser;
    }
}
