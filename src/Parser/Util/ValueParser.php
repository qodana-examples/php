<?php
declare(strict_types=1);

namespace LessPlate\Parser\Util;

use LessPlate\Lexer\Token\Operator\ArithmeticOperatorToken;
use LessPlate\Lexer\Token\Specialized\Accessor\AccessorCloseToken;
use LessPlate\Lexer\Token\Specialized\Accessor\AccessorStartToken;
use LessPlate\Lexer\Token\Specialized\Accessor\AccessorToken as LexerAccessorToken;
use LessPlate\Lexer\Token\Specialized\OptionalToken;
use LessPlate\Lexer\Token\Token as LexerToken;
use LessPlate\Lexer\Token\Value\DigitToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\Token\Value\StringToken;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\UnexpectedToken;
use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Util\AccessorToken;
use LessPlate\Parser\Token\Value\DynamicValueToken;
use LessPlate\Parser\Token\Value\Number\FloatValueToken;
use LessPlate\Parser\Token\Value\Number\IntegerValueToken;
use LessPlate\Parser\Token\Value\StringValueToken;

/**
 * Utility parser for value's
 */
final class ValueParser implements UtilParser
{
    /**
     * Parse a value
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws UnexpectedToken
     */
    public function parse(TokenStream $stream): Token
    {
        $current = $stream->getCurrent();

        if ($current instanceof ArithmeticOperatorToken && $current->getInput() === '-' && $stream->lookahead() instanceof DigitToken) {
            $stream->consume();

            return $this->number($stream, true);
        }

        if ($current instanceof DigitToken) {
            return $this->number($stream, false);
        }

        if ($current instanceof ReferenceToken) {
            return $this->dynamic($stream);
        }

        if ($current instanceof StringToken) {
            $stream->consume();

            return new StringValueToken($current->getInput());
        }

        throw new UnexpectedToken(
            $current,
            DigitToken::NAME,
            StringToken::NAME,
            ReferenceToken::NAME
        );
    }

    /**
     * Parse number value
     *
     * @param TokenStream $stream
     * @param bool $negative
     *
     * @return Token
     *
     * @throws UnexpectedToken
     */
    private function number(TokenStream $stream, bool $negative): Token
    {
        $number = ($negative ? '-' : '') . $stream->getCurrent()->getInput();
        $stream->consume();

        if ($stream->getCurrent()->getInput() === '.') {
            $number .= $stream->getCurrent()->getInput();
            $stream->consume();

            if (!$stream->getCurrent() instanceof DigitToken) {
                throw new UnexpectedToken(
                    $stream->getCurrent(),
                    DigitToken::NAME
                );
            }

            $number .= $stream->getCurrent()->getInput();
            $stream->consume();
            $token = new FloatValueToken($number);
        } else {
            $token = new IntegerValueToken($number);
        }

        return $token;
    }

    /**
     * Parse dynamic token
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws UnexpectedToken
     */
    private function dynamic(TokenStream $stream): Token
    {
        $token = $stream->getCurrent();
        $stream->consume();
        $accessors = [];

        $optional = $stream->getCurrent() instanceof OptionalToken;

        if ($optional) {
            $stream->consume();
        }

        while ($this->isAccessorStart($stream->getCurrent())) {
            $starter = $stream->getCurrent();
            $stream->consume();

            $optional = $stream->getCurrent() instanceof OptionalToken;

            if ($optional) {
                $stream->consume();
            }

            if ($starter instanceof LexerAccessorToken) {
                if (!$stream->getCurrent() instanceof ReferenceToken) {
                    throw new UnexpectedToken(
                        $stream->getCurrent(),
                        ReferenceToken::class
                    );
                }

                $accessors[] = new AccessorToken(
                    new StringValueToken($stream->getCurrent()->getInput()),
                    $optional
                );
                $stream->consume();
            } elseif ($starter instanceof AccessorStartToken) {
                $accessors[] = new AccessorToken(
                    $this->parse($stream),
                    $optional
                );

                if (!$stream->getCurrent() instanceof AccessorCloseToken) {
                    throw new UnexpectedToken(
                        $stream->getCurrent(),
                        AccessorCloseToken::class
                    );
                }

                $stream->consume();
            }
        }

        return new DynamicValueToken($token->getInput(), $optional, $accessors);
    }

    /**
     * Check of the lexer token marks an accessor start
     *
     * @param LexerToken $token
     *
     * @return bool
     */
    private function isAccessorStart(LexerToken $token): bool
    {
        return $token instanceof LexerAccessorToken || $token instanceof AccessorStartToken;
    }
}