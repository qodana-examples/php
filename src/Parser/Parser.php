<?php
declare(strict_types=1);

namespace LessPlate\Parser;

use LessPlate\Config\Parser\ParserConfig;
use LessPlate\Lexer\TokenStream\TokenStream;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Token;

/**
 * Parser to lexer tokens
 */
interface Parser
{
    /**
     * Parse the token stream to a syntax tree
     *
     * @param TokenStream $stream
     *
     * @return SyntaxTree
     *
     * @throws ParseError
     */
    public function parse(TokenStream $stream): SyntaxTree;

    /**
     * Only parse to a single token
     *
     * @param TokenStream $stream
     *
     * @return Token
     *
     * @throws ParseError
     */
    public function partial(TokenStream $stream): Token;

    /**
     * Returns the parser config
     *
     * @return ParserConfig
     */
    public function getConfig(): ParserConfig;
}