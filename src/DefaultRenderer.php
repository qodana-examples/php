<?php
declare(strict_types=1);

namespace LessPlate;

use LessPlate\Config\Config;
use LessPlate\Config\DefaultConfig;
use LessPlate\Context\Context;
use LessPlate\Context\EmptyContext;
use LessPlate\Executor\BaseExecutor;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Executor\Executor;
use LessPlate\Input\Input;
use LessPlate\Lexer\Exception\TokenizeError;
use LessPlate\Lexer\LazyLexer;
use LessPlate\Lexer\Lexer;
use LessPlate\Parser\Exception\ParseError;
use LessPlate\Parser\LazyParser;
use LessPlate\Parser\Parser;

/**
 * Default implementation of the Renderer
 */
final class DefaultRenderer implements Renderer
{
    /**
     * Executor to use
     *
     * @var Executor|null
     */
    protected $executor;
    /**
     * Parser to use
     *
     * @var Parser|null
     */
    protected $parser;
    /**
     * Lexer to use
     *
     * @var Lexer|null
     */
    protected $lexer;

    /**
     * BaseRenderer constructor
     *
     * @param Config|null $config
     */
    public function __construct(?Config $config = null)
    {
        $config = $config ?? new DefaultConfig();

        $this->executor = new BaseExecutor($config->getExecutorConfig());
        $this->parser = new LazyParser($config->getParserConfig());
        $this->lexer = new LazyLexer($config->getLexerConfig());
    }

    /**
     * Renders the input with the given context
     *
     * @param Input $input
     * @param Context|null $context
     *
     * @return string
     *
     * @throws ExecuteError
     * @throws ParseError
     * @throws TokenizeError
     *
     * @todo use empty context
     */
    public function render(Input $input, ?Context $context = null): string
    {
        $context = $context ?? new EmptyContext();

        $tokens = $this->lexer->tokenize($input);
        $tree = $this->parser->parse($tokens);

        return trim($this->executor->execute($tree, $context));
    }
}