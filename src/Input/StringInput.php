<?php
declare(strict_types=1);

namespace LessPlate\Input;

/**
 * String implementation of the input
 */
final class StringInput extends AbstractInput
{
    /**
     * Actual string
     *
     * @var string
     */
    protected $string;
    /**
     * Input checksum
     *
     * @var string|null
     */
    protected $checksum;
    /**
     * Current line
     *
     * @var int
     */
    protected $line = 1;
    /**
     * Current column
     *
     * @var int
     */
    protected $column = 1;

    /**
     * StringInput constructor
     *
     * @param string $string
     * @param string|null $checksum
     */
    public function __construct(string $string, ?string $checksum = null)
    {
        $this->string = $string;
        $this->checksum = $checksum;
    }

    /**
     * Creates input with the string as md5 checksum
     *
     * @param string $string
     *
     * @return StringInput
     */
    public static function withMd5Checksum(string $string): StringInput
    {
        return new static($string, md5($string));
    }

    /**
     * Creates input with the string as sha1 checksum
     *
     * @param string $string
     *
     * @return StringInput
     */
    public static function withSha1Checksum(string $string): StringInput
    {
        return new static($string, sha1($string));
    }

    /**
     * Returns current character
     *
     * @return string
     */
    public function getCurrent(): string
    {
        return mb_substr($this->string, 0, 1);
    }

    /**
     * Returns current line
     *
     * @return int
     */
    public function getCurrentLine(): int
    {
        return $this->line;
    }

    /**
     * Returns current column
     *
     * @return int
     */
    public function getCurrentColumn(): int
    {
        return $this->column;
    }

    /**
     * Lookahead n characters
     *
     * @param int $ahead
     *
     * @return string|null
     */
    public function lookahead(int $ahead = 1): ?string
    {
        $character = mb_substr($this->string, $ahead, 1);

        if (is_string($character) && $character !== '') {
            return $character;
        }

        return null;
    }

    /**
     * Simple version of get sequence
     *
     * @param int $length
     *
     * @return string
     */
    public function getSequence(int $length): string
    {
        return mb_substr($this->string, 0, $length);
    }

    /**
     * Returns location of sequence
     *
     * @param string $sequence
     *
     * @return int|null
     */
    public function locateSequence(string $sequence): ?int
    {
        $position = mb_strpos($this->string, $sequence);

        return is_int($position)
            ? $position
            : null;
    }

    /**
     * Consume n characters
     *
     * @param int $amount
     *
     * @return Input
     *
     * @todo check amount is positive
     */
    public function consume(int $amount = 1): Input
    {
        $part = mb_substr($this->string, 0, $amount);
        $lines = mb_substr_count($part, PHP_EOL);

        if ($lines > 0) {
            $this->line += $lines;
            $this->column = 0;

            $this->column += $amount - mb_strrpos($part, PHP_EOL);
        } else {
            $this->column += $amount;
        }

        $this->string = mb_substr($this->string, $amount);

        return $this;
    }

    /**
     * Returns valid if there is a character available
     *
     * @return bool
     */
    public function valid(): bool
    {
        return mb_strlen($this->string) > 0;
    }

    /**
     * Returns string checksum
     *
     * @return string
     */
    public function getCheckSum(): string
    {
        return $this->checksum;
    }
}
