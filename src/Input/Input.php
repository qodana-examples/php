<?php
declare(strict_types=1);

namespace LessPlate\Input;

/**
 * Input
 */
interface Input
{
    /**
     * Returns current character
     *
     * @return string
     */
    public function getCurrent(): string;

    /**
     * Lookahead n characters
     *
     * @param int $ahead
     *
     * @return string|null
     */
    public function lookahead(int $ahead = 1): ?string;

    /**
     * Checks if the past sequence is the current on
     *
     * @param string $sequence
     *
     * @return bool
     */
    public function isSequence(string $sequence): bool;

    /**
     * Returns input sequence
     *
     * @param int $length
     *
     * @return string
     */
    public function getSequence(int $length): string;

    /**
     * Returns the first position of the given sequence, null on never
     *
     * @param string $sequence
     *
     * @return int|null
     */
    public function locateSequence(string $sequence): ?int;

    /**
     * Returns current line
     *
     * @return int
     */
    public function getCurrentLine(): int;

    /**
     * Returns current column
     *
     * @return int
     */
    public function getCurrentColumn(): int;

    /**
     * Consume n characters
     *
     * @param int $amount
     *
     * @return Input
     */
    public function consume(int $amount = 1): Input;

    /**
     * Checks if there is a current character
     *
     * @return bool
     */
    public function valid(): bool;

    /**
     * Returns the input checksum
     *
     * @return string|null
     */
    public function getCheckSum(): ?string;
}