<?php
declare(strict_types=1);

namespace LessPlate\Input;

use TypeError;

/**
 * Input based on resource
 */
final class ResourceInput extends AbstractInput
{
    /**
     * Resource to use
     *
     * @var resource|null
     */
    protected $resource;
    /**
     * Checksum of the resource
     *
     * @var string|null
     */
    protected $checksum;

    /**
     * Current memory lines
     *
     * @var string
     */
    protected $lines = '';
    /**
     * Current line
     *
     * @var int
     */
    protected $line = 1;
    /**
     * Current column
     *
     * @var int
     */
    protected $column = 1;

    /**
     * ResourceInput constructor
     *
     * @param resource $resource
     * @param string $checksum
     */
    public function __construct($resource, ?string $checksum = null)
    {
        if (!is_resource($resource)) {
            throw new TypeError('resource expected, got ' . gettype($resource));
        }

        $this->resource = $resource;
        $this->checksum = $checksum;

        $this->addLine();
    }

    /**
     * Returns the current character
     *
     * @return string
     */
    public function getCurrent(): string
    {
        return mb_substr($this->lines, 0, 1);
    }

    /**
     * Returns current line
     *
     * @return int
     */
    public function getCurrentLine(): int
    {
        return $this->line;
    }

    /**
     * Returns current column
     *
     * @return int
     */
    public function getCurrentColumn(): int
    {
        return $this->column;
    }

    /**
     * Consume n characters
     *
     * @param int $amount
     *
     * @return Input
     *
     * @todo check amount is positive
     */
    public function consume(int $amount = 1): Input
    {
        do {
            $length = mb_strlen($this->lines);

            if ($length <= $amount) {
                $amount -= $length;
                $this->lines = '';
                $this->addLine();
                $this->line += 1;
                $this->column = 1;
            } else {
                $this->lines = mb_substr($this->lines, $amount);
                $this->column += $amount;
                $amount = 0;
            }
        } while ($amount > 0 && $this->valid());

        return $this;
    }

    /**
     * Lookahead n characters
     *
     * @param int $ahead
     *
     * @return string|null
     *
     * @todo check ahead is positive
     */
    public function lookahead(int $ahead = 1): ?string
    {
        while ($this->hasMoreLines() && mb_strlen($this->lines) - 1 < $ahead) {
            $this->addLine();
        }

        $character = mb_substr($this->lines, $ahead, 1);

        if (is_string($character) && $character !== '') {
            return $character;
        }

        return null;
    }

    /**
     * Locates sequence
     *
     * @param string $sequence
     *
     * @return int|null
     */
    public function locateSequence(string $sequence): ?int
    {
        $position = mb_strpos($this->lines, $sequence);

        while ($this->hasMoreLines() && !is_int($position)) {
            $this->addLine();

            $position = mb_strpos($this->lines, $sequence);
        }

        return is_int($position)
            ? $position
            : null;
    }

    /**
     * Adds the next line to the current lines
     *
     * @return void
     */
    private function addLine(): void
    {
        if ($this->hasMoreLines()) {
            $line = fgets($this->resource);

            if (is_string($line)) {
                $this->lines .= $line;
            } else {
                $this->resource = null;
            }
        }
    }

    /**
     * Checks if there are more lines to read
     *
     * @return bool
     */
    private function hasMoreLines(): bool
    {
        return is_resource($this->resource);
    }

    /**
     * Check if the input is not finished
     *
     * @return bool
     */
    public function valid(): bool
    {
        return mb_strlen($this->lines) > 0;
    }

    /**
     * Returns current checksum
     *
     * @return string
     */
    public function getCheckSum(): ?string
    {
        return $this->checksum;
    }
}
