<?php
declare(strict_types=1);

namespace LessPlate\Input;

/**
 * Adds the sequence method
 */
abstract class AbstractInput implements Input
{
    /**
     * Returns input sequence
     *
     * @param int $length
     *
     * @return string
     */
    public function getSequence(int $length): string
    {
        $sequence = $this->getCurrent();

        for ($i = 1; $i < $length; $i += 1) {
            $sequence .= $this->lookahead($i);
        }

        return $sequence;
    }

    /**
     * Checks if the sequence is the same
     *
     * @param string $sequence
     *
     * @return bool
     */
    public function isSequence(string $sequence): bool
    {
        return strtolower($this->getSequence(strlen($sequence))) === strtolower($sequence);
    }
}
