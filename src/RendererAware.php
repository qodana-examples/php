<?php
declare(strict_types=1);

namespace LessPlate;

/**
 * Adds the renderer as a property
 */
trait RendererAware
{
    /**
     * Holds the renderer to use
     *
     * @var Renderer
     */
    protected $renderer;

    /**
     * RendererAware constructor
     *
     * @param Renderer $renderer
     */
    public function __construct(Renderer $renderer)
    {
        $this->setRenderer($renderer);
    }

    /**
     * Returns the renderer to use
     *
     * @return Renderer
     */
    protected function getRenderer(): Renderer
    {
        return $this->renderer;
    }

    /**
     * Sets the renderer to use
     *
     * @param Renderer $renderer
     *
     * @return $this
     */
    protected function setRenderer(Renderer $renderer)
    {
        $this->renderer = $renderer;

        return $this;
    }
}