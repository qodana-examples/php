<?php
declare(strict_types=1);

namespace LessPlate\Executor\Exception;

use Exception;
use LessPlate\Parser\Token\Token;

/**
 * Thrown when an unexpected token was given
 */
final class UnexpectedToken extends Exception implements ExecuteError
{
    /**
     * Expected token
     *
     * @var string[]
     */
    protected $expected;
    /**
     * Actual token that was got
     *
     * @var Token
     */
    protected $got;

    /**
     * UnexpectedToken constructor
     *
     * @param Token $got
     * @param string ...$expected
     */
    public function __construct(Token $got, string ...$expected)
    {
        parent::__construct(
            sprintf(
                'Expected %s, got %s',
                implode(', ', $expected),
                $got->getName()
            )
        );

        $this->expected = $expected;
        $this->got = $got;
    }

    /**
     * Returns the expected tokens
     *
     * @return string[]|iterable
     */
    public function getExpected(): iterable
    {
        return $this->expected;
    }

    /**
     * Returns the actual given token
     *
     * @return Token
     */
    public function getGot(): Token
    {
        return $this->got;
    }
}