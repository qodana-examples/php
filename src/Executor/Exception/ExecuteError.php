<?php
declare(strict_types=1);

namespace LessPlate\Executor\Exception;

use Throwable;

/**
 * Thrown on an error during execution
 */
interface ExecuteError extends Throwable
{
}