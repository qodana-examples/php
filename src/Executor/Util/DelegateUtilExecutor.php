<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Parser\Token\Token;

/**
 * Executor to delegate to specified executors
 */
final class DelegateUtilExecutor implements UtilExecutor
{
    /**
     * Holds the executors
     *
     * @var UtilExecutor[]
     *
     * @todo split value token
     */
    protected $executors = [];

    /**
     * DelegateUtilExecutor constructor
     *
     * @param UtilExecutor[] $utilExecutors
     */
    public function __construct(iterable $utilExecutors)
    {
        foreach ($utilExecutors as $for => $utilExecutor) {
            $this->addUtilExecutor($for, $utilExecutor);
        }
    }

    /**
     * Adds the util executor
     *
     * @param string $for
     * @param UtilExecutor $utilExecutor
     *
     * @return void
     */
    private function addUtilExecutor(string $for, UtilExecutor $utilExecutor): void
    {
        $this->executors[$for] = $utilExecutor;
    }

    /**
     * Passes the token to the designated executor
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return mixed
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!array_key_exists($token->getName(), $this->executors)) {
            throw new UnexpectedToken(
                $token,
                ...array_keys($this->executors)
            );
        }

        return $this->executors[$token->getName()]->execute($token, $context, $executor);
    }
}