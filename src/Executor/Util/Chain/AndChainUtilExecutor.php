<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Chain;

/**
 * Executor for and chain
 */
final class AndChainUtilExecutor extends AbstractChainUtilExecutor
{
    /**
     * And chain
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function chain($left, $right)
    {
        return $left && $right;
    }
}