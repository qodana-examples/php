<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Chain;

/**
 * Or chain executor
 */
final class OrChainUtilExecutor extends AbstractChainUtilExecutor
{
    /**
     * Or chain
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    protected function chain($left, $right)
    {
        return $left || $right;
    }
}