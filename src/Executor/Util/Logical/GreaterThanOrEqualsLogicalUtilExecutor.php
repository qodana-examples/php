<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

/**
 * Greater than or equals executor
 */
final class GreaterThanOrEqualsLogicalUtilExecutor extends AbstractLogicalUtilExecutor
{
    /**
     * Compare left is greater than or equals to right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function compare($left, $right)
    {
        return $left >= $right;
    }
}