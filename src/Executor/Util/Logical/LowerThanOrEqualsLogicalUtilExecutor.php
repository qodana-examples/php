<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

/**
 * Lower than or equals executor
 */
final class LowerThanOrEqualsLogicalUtilExecutor  extends AbstractLogicalUtilExecutor
{
    /**
     * Compare that left is lower than or equals to right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function compare($left, $right)
    {
        return $left <= $right;
    }
}