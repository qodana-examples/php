<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

/**
 * Greater than executor
 */
final class GreaterThanLogicalUtilExecutor extends AbstractLogicalUtilExecutor
{
    /**
     * Compare that left is greater than right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function compare($left, $right)
    {
        return $left > $right;
    }
}