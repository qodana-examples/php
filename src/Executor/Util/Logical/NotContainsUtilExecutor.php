<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

/**
 * Not contains executor
 */
final class NotContainsUtilExecutor extends AbstractLogicalUtilExecutor
{
    /**
     * Compare that left is not equals to right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function compare($left, $right)
    {
        if (is_string($left) && is_string($right)) {
            return strpos($left, $right) === false;
        }

        if (is_array($left)) {
            return in_array($right, $left, true) === false;
        }

        return false;
    }
}
