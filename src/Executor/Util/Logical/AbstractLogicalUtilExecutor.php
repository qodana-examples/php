<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Parser\Token\Expression\Chain\ChainExpressionToken;
use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Adds the base setup
 */
abstract class AbstractLogicalUtilExecutor implements UtilExecutor
{
    /**
     * Execute chain tokens
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return mixed
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!$token instanceof LogicalExpressionToken) {
            throw new UnexpectedToken(
                $token,
                ChainExpressionToken::TYPE
            );
        }

        $expressionExecutor = $executor->getConfig()->getExpressionExecutor();

        return $this->compare(
            $expressionExecutor->execute($token->getLeft(), $context, $executor),
            $expressionExecutor->execute($token->getRight(), $context, $executor)
        );
    }

    /**
     * Compare left with right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    abstract protected function compare($left, $right);
}