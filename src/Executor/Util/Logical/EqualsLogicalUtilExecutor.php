<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

/**
 * Equals executor
 */
final class EqualsLogicalUtilExecutor extends AbstractLogicalUtilExecutor
{
    /**
     * Check that left equals right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function compare($left, $right)
    {
        return $left === $right;
    }
}