<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

/**
 * Lower than executor
 */
final class LowerThanLogicalUtilExecutor extends AbstractLogicalUtilExecutor
{
    /**
     * Check left is lower than right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function compare($left, $right)
    {
        return $left < $right;
    }
}