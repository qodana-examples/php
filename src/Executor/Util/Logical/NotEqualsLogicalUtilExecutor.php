<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Logical;

/**
 * Not equals executor
 */
final class NotEqualsLogicalUtilExecutor extends AbstractLogicalUtilExecutor
{
    /**
     * Compare that left is not equals to right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return bool|mixed
     */
    protected function compare($left, $right)
    {
        return $left !== $right;
    }
}