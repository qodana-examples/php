<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util;

use LessPlate\Context\Context;
use LessPlate\Executor\Executor;
use LessPlate\Parser\Token\Token;

/**
 * Utility executor
 */
interface UtilExecutor
{
    /**
     * Execute utility tokens
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return mixed
     */
    public function execute(Token $token, Context $context, Executor $executor);
}