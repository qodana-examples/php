<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Value;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Value\StringValueToken;

/**
 * Executor for string
 */
final class StringValueUtilExecutor implements UtilExecutor
{
    /**
     * Returns string
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return int|mixed
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!$token instanceof StringValueToken) {
            throw new UnexpectedToken($token, StringValueToken::NAME);
        }

        return (string) $token->getString();
    }
}