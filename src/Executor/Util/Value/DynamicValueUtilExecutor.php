<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Value;

use LessPlate\Context\Context;
use LessPlate\Context\Exception\ContextError;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Value\DynamicValueToken;
use LessPlate\Parser\Token\Value\Number\IntegerValueToken;
use LessPlate\Parser\Token\Value\StringValueToken;
use RuntimeException;

/**
 * Executor for dynamic value
 */
final class DynamicValueUtilExecutor implements UtilExecutor
{
    /**
     * Constant values
     *
     * @var array|mixed[]
     */
    private $constants = [];

    /**
     * DynamicValueUtilExecutor constructor
     *
     * @param array|mixed[] $constants
     */
    public function __construct(iterable $constants)
    {
        foreach ($constants as $key => $value) {
            $this->constants[strtolower($key)] = $value;
        }
    }

    /**
     * Execute dynamic value token
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return mixed
     *
     * @throws UnexpectedToken
     * @throws ContextError
     *
     * @todo use custom exception
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!$token instanceof DynamicValueToken) {
            throw new UnexpectedToken($token, DynamicValueToken::NAME);
        }

        if ($token->isOptional()) {
            trigger_error('Optional will be dropped', E_USER_NOTICE);
        }

        if (array_key_exists(strtolower($token->getReference()), $this->constants)) {
            $value = $this->constants[strtolower($token->getReference())];
        } elseif ($context->hasValue($token->getReference())) {
            $value = $context->getValue($token->getReference());
        } else {
            $value = null;
        }

        foreach ($token->getAccessors() as $accessor) {
            if ($value === null) {
                break;
            }

            if ($accessor->isOptional()) {
                trigger_error('Optional will be dropped', E_USER_NOTICE);
            }

            $expression = $accessor->getExpression();

            if ($expression instanceof StringValueToken) {
                $key = $expression->getString();
            } elseif ($expression instanceof IntegerValueToken) {
                $key = $expression->getInteger();
            } elseif ($expression instanceof DynamicValueToken) {
                $key = $this->execute($expression, $context, $executor);

                if ($key === null) {
                    return null;
                }
            } else {
                throw new UnexpectedToken(
                    $expression,
                    StringValueToken::NAME,
                    DynamicValueToken::NAME,
                    IntegerValueToken::NAME
                );
            }

            if (is_object($value)) {
                $value = property_exists($value, $key)
                    ? $value->{$key}
                    : null;
            } elseif (is_array($value)) {
                $value = array_key_exists($key, $value)
                    ? $value[$key]
                    : null;
            } else {
                throw new RuntimeException(
                    sprintf(
                        'Input must be an array or object, got %s',
                        gettype($value)
                    )
                );
            }
        }

        return $value;
    }
}
