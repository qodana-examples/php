<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Value\Number;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Value\Number\FloatValueToken;

/**
 * Executor for float
 */
final class FloatValueUtilExecutor implements UtilExecutor
{
    /**
     * Returns float
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return int|mixed
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!$token instanceof FloatValueToken) {
            throw new UnexpectedToken($token, FloatValueToken::NAME);
        }

        return (float) $token->getFloat();
    }
}