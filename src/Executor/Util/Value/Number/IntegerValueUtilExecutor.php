<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Value\Number;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Parser\Token\Token;
use LessPlate\Parser\Token\Value\Number\IntegerValueToken;

/**
 * Executor for integer
 */
final class IntegerValueUtilExecutor implements UtilExecutor
{
    /**
     * Returns int
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return int|mixed
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!$token instanceof IntegerValueToken) {
            throw new UnexpectedToken($token, IntegerValueToken::NAME);
        }

        return (int) $token->getInteger();
    }
}