<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Arithmetic;

use OutOfRangeException;

/**
 * Divide executor
 */
final class DivideUtilExecutor extends AbstractArithmeticUtilExecutor
{
    /**
     * Divide left from right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     *
     * @todo use custom exception
     */
    protected function calculate($left, $right)
    {
        if ((int)$right === 0) {
            throw new OutOfRangeException(
                sprintf(
                    'Cannot divide by zero'
                )
            );
        }

        return $left / $right;
    }
}
