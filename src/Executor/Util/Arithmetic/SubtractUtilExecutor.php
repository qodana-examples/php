<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Arithmetic;

/**
 * Subtract executor
 */
final class SubtractUtilExecutor extends AbstractArithmeticUtilExecutor
{
    /**
     * Subtracts left from right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    protected function calculate($left, $right)
    {
        return $left - $right;
    }
}