<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Arithmetic;

/**
 * Addition executor
 */
final class AdditionUtilExecutor extends AbstractArithmeticUtilExecutor
{
    /**
     * Add left from right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    protected function calculate($left, $right)
    {
        return $left + $right;
    }
}