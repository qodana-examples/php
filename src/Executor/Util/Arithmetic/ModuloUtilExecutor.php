<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Arithmetic;

/**
 * Modulo executor
 */
final class ModuloUtilExecutor extends AbstractArithmeticUtilExecutor
{
    /**
     * Modulo left from right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    protected function calculate($left, $right)
    {
        return $left % $right;
    }
}