<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Arithmetic;

/**
 * Executor for power
 */
final class PowerUtilExecutor extends AbstractArithmeticUtilExecutor
{
    /**
     * Subtracts left from right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    protected function calculate($left, $right)
    {
        return pow($left, $right);
    }
}