<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Arithmetic;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Parser\Token\Expression\Arithmetic\ArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Setups up the base
 */
abstract class AbstractArithmeticUtilExecutor implements UtilExecutor
{
    /**
     * Execute arithmetic token
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return mixed
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!$token instanceof ArithmeticExpressionToken) {
            throw new UnexpectedToken(
                $token,
                ArithmeticExpressionToken::TYPE
            );
        }

        $expressionExecutor = $executor->getConfig()->getExpressionExecutor();

        return $this->calculate(
            $expressionExecutor->execute($token->getLeft(), $context, $executor),
            $expressionExecutor->execute($token->getRight(), $context, $executor)
        );
    }

    /**
     * Calculate result
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    abstract protected function calculate($left, $right);
}