<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util\Arithmetic;

/**
 * Multiply executor
 */
final class MultiplyUtilExecutor extends AbstractArithmeticUtilExecutor
{
    /**
     * Multiply left from right
     *
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     */
    protected function calculate($left, $right)
    {
        return $left * $right;
    }
}