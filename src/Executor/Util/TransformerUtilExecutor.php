<?php
declare(strict_types=1);

namespace LessPlate\Executor\Util;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Parser\Token\Expression\TransformerExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Transform expression util
 */
final class TransformerUtilExecutor implements UtilExecutor
{
    /**
     * Execute token
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return mixed
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor)
    {
        if (!$token instanceof TransformerExpressionToken) {
            throw new UnexpectedToken(
                $token,
                'TRANSFORMER_TOKEN'
            );
        }

        $config = $executor->getConfig();

        $result = $config->getExpressionExecutor()->execute($token->getValue(), $context, $executor);

        foreach ($token->getTransformers() as $transformer) {
            $parameters = [$result];

            foreach ($transformer->getParameters() as $parameter) {
                $parameters[] = $executor->getConfig()->getExpressionExecutor()->execute($parameter, $context, $executor);
            }

            $result = $config
                ->getTransformerConfig()
                ->getByReference($transformer->getReference())
                ->transform(...$parameters);
        }

        return $result;
    }
}