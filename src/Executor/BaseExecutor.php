<?php
declare(strict_types=1);

namespace LessPlate\Executor;

use LessPlate\Config\Executor\ExecutorConfig;
use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\BlockToken;
use LessPlate\Parser\Token\Tag\Expression\TagExpressionToken;
use LessPlate\Parser\Token\TextToken;
use LessPlate\Parser\Token\Token;

/**
 * Base executor
 */
final class BaseExecutor implements Executor
{
    /**
     * Config to use
     *
     * @var ExecutorConfig
     */
    private $config;

    /**
     * BaseExecutor constructor
     *
     * @param ExecutorConfig $config
     */
    public function __construct(ExecutorConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Execute the syntax tree with the given context
     *
     * @param SyntaxTree<Token> $tree
     * @param Context $context
     *
     * @return string
     *
     * @throws UnexpectedToken
     */
    public function execute(SyntaxTree $tree, Context $context): string
    {
        $text = '';

        foreach ($tree as $token) {
            $text .= $this->partial($token, $context);
        }

        return $text;
    }

    /**
     * Execute token with context
     *
     * @param Token $token
     * @param Context $context
     *
     * @return string
     *
     * @throws UnexpectedToken
     */
    public function partial(Token $token, Context $context): string
    {
        if ($token instanceof TextToken) {
            return $this->text($token);
        }

        if ($token instanceof TagExpressionToken) {
            return $this->tag($token, $context);
        }

        if ($token instanceof BlockToken) {
            return $this->block($token, $context);
        }

        throw new UnexpectedToken(
            $token,
            TextToken::NAME,
            TagExpressionToken::NAME
        );
    }

    /**
     * Returns config from BaseExecutor
     *
     * @return ExecutorConfig
     */
    public function getConfig(): ExecutorConfig
    {
        return $this->config;
    }

    /**
     * Execute block token
     *
     * @param BlockToken $token
     * @param Context $context
     *
     * @return string
     */
    private function block(BlockToken $token, Context $context): string
    {
        return $this->config
            ->getTagBlockExecutor()
            ->execute($token, $context, $this);
    }

    /**
     * Execute tag token
     *
     * @param TagExpressionToken $token
     * @param Context $context
     *
     * @return string
     */
    private function tag(TagExpressionToken $token, Context $context): string
    {
        $config = $this->config;
        $result = $config->getExpressionExecutor()->execute($token->getExpression(), $context, $this);
        $escaper = $this->config->getTagEscaper();

        if (is_string($result) && $escaper) {
            $result = $escaper->transform($result);
        }

        return (string) $result;
    }

    /**
     * Execute the text token
     *
     * @param TextToken $token
     *
     * @return string
     */
    private function text(TextToken $token): string
    {
        return $token->getText();
    }
}
