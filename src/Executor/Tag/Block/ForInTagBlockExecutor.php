<?php
declare(strict_types=1);

namespace LessPlate\Executor\Tag\Block;

use LessPlate\Context\ArrayContext;
use LessPlate\Context\CombinedContext;
use LessPlate\Context\Context;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\ForInBlockToken;
use LessPlate\Parser\Token\Token;

/**
 * Executor for in block
 */
final class ForInTagBlockExecutor implements TagBlockExecutor
{
    /**
     * Executor token
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return string
     *
     * @throws UnexpectedToken
     * @throws ExecuteError
     */
    public function execute(Token $token, Context $context, Executor $executor): string
    {
        if (!$token instanceof ForInBlockToken) {
            throw new UnexpectedToken(
                $token,
                ForInBlockToken::NAME
            );
        }

        $utilExecutor = $executor->getConfig()->getExpressionExecutor();
        $collection = $utilExecutor
            ->execute($token->getExpression(), $context, $executor);

        $result = '';

        $iterated = false;

        if (is_iterable($collection)) {
            foreach ($collection as $key => $value) {
                $iterated = true;
                $subContext = [$token->getAs() => $value];

                if ($token->getKey()) {
                    $subContext[$token->getKey()] = $key;
                }

                $result .= ltrim(
                    $executor->execute(
                        $token->getContent(),
                        CombinedContext::join(
                            new ArrayContext($subContext),
                            $context
                        )
                    )
                );
            }
        }

        if ($iterated === false && (is_iterable($collection) || $collection === null) && $token->getOnEmpty() instanceof SyntaxTree) {
            $result = ltrim(
                $executor->execute(
                    $token->getOnEmpty(),
                    $context
                )
            );
        }

        return trim($result);
    }
}
