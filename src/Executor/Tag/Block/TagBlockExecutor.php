<?php
declare(strict_types=1);

namespace LessPlate\Executor\Tag\Block;

use LessPlate\Context\Context;
use LessPlate\Executor\Executor;
use LessPlate\Parser\Token\Token;

/**
 * Executor for blocks
 */
interface TagBlockExecutor
{
    /**
     * Test execute
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return string
     */
    public function execute(Token $token, Context $context, Executor $executor): string;
}