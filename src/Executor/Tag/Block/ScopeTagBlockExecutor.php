<?php
declare(strict_types=1);

namespace LessPlate\Executor\Tag\Block;

use LessPlate\Context\ArrayContext;
use LessPlate\Context\CombinedContext;
use LessPlate\Context\Context;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Parser\Token\Block\ScopeBlockToken;
use LessPlate\Parser\Token\Token;

/**
 * Executor for scope
 */
final class ScopeTagBlockExecutor implements TagBlockExecutor
{
    /**
     * Executor scope token
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return string
     *
     * @throws UnexpectedToken
     * @throws ExecuteError
     */
    public function execute(Token $token, Context $context, Executor $executor): string
    {
        if (!$token instanceof ScopeBlockToken) {
            throw new UnexpectedToken(
                $token,
                ScopeBlockToken::NAME
            );
        }

        $utilExecutor = $executor->getConfig()->getExpressionExecutor();

        $subContext = CombinedContext::join(
            new ArrayContext(
                [
                    $token->getAs() => $utilExecutor->execute(
                        $token->getFrom(),
                        $context,
                        $executor
                    ),
                ]
            ),
            $context
        );

        return trim(
            $executor->execute(
                $token->getContent(),
                $subContext
            )
        );
    }
}