<?php
declare(strict_types=1);

namespace LessPlate\Executor\Tag\Block;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Block\IfBlockToken;
use LessPlate\Parser\Token\Token;

/**
 * Executor for an if block
 */
final class IfTagBlockExecutor implements TagBlockExecutor
{
    /**
     * Execute the if block
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return string
     *
     * @throws UnexpectedToken
     * @throws ExecuteError
     */
    public function execute(Token $token, Context $context, Executor $executor): string
    {
        if (!$token instanceof IfBlockToken) {
            throw new UnexpectedToken(
                $token,
                IfBlockToken::NAME
            );
        }

        $utilExecutor = $executor->getConfig()->getExpressionExecutor();

        foreach ($token->getConditions() as $condition) {
            if ($utilExecutor->execute($condition->getExpression(), $context, $executor)) {
                $result = $executor->execute(
                    $condition->getContent(),
                    $context
                );

                break;
            }
        }

        if (!isset($result) && $token->getElse() instanceof SyntaxTree) {
            $result = $executor->execute(
                $token->getElse(),
                $context
            );
        }

        return trim($result ?? '');
    }
}