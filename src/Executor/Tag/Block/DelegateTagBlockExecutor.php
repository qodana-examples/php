<?php
declare(strict_types=1);

namespace LessPlate\Executor\Tag\Block;

use LessPlate\Context\Context;
use LessPlate\Executor\Exception\UnexpectedToken;
use LessPlate\Executor\Executor;
use LessPlate\Parser\Token\Block\BlockToken;
use LessPlate\Parser\Token\Token;

/**
 * Delegate the execution
 */
final class DelegateTagBlockExecutor implements TagBlockExecutor
{
    /**
     * Holds tag block executors
     *
     * @var array|TagBlockExecutor[]
     */
    private $tagBlockExecutors = [];

    /**
     * DelegateTagBlockExecutor constructor
     *
     * @param array|TagBlockExecutor[] $tagBlockExecutors
     */
    public function __construct(iterable $tagBlockExecutors)
    {
        foreach ($tagBlockExecutors as $for => $tagBlockExecutor) {
            $this->addTagBlockExecutor($for, $tagBlockExecutor);
        }
    }

    /**
     * Adds the tag block executor
     *
     * @param string $for
     * @param TagBlockExecutor $tagBlockExecutor
     *
     * @return void
     */
    private function addTagBlockExecutor(string $for, TagBlockExecutor $tagBlockExecutor): void
    {
        $this->tagBlockExecutors[$for] = $tagBlockExecutor;
    }

    /**
     * Delegates the execution
     *
     * @param Token $token
     * @param Context $context
     * @param Executor $executor
     *
     * @return string
     *
     * @throws UnexpectedToken
     */
    public function execute(Token $token, Context $context, Executor $executor): string
    {
        if (!$token instanceof BlockToken) {
            throw new UnexpectedToken($token, BlockToken::GROUP);
        }

        return  $this
            ->tagBlockExecutors[$token->getName()]
            ->execute($token, $context, $executor);
    }
}