<?php
declare(strict_types=1);

namespace LessPlate\Executor;

use LessPlate\Config\Executor\ExecutorConfig;
use LessPlate\Context\Context;
use LessPlate\Executor\Exception\ExecuteError;
use LessPlate\Parser\SyntaxTree\SyntaxTree;
use LessPlate\Parser\Token\Token;

/**
 * Execute parse result to gain the result
 */
interface Executor
{
    /**
     * Execute the syntax tree
     *
     * @param SyntaxTree<Token> $tree
     * @param Context $context
     *
     * @return string
     *
     * @throws ExecuteError
     */
    public function execute(SyntaxTree $tree, Context $context): string;

    /**
     * Executes only with a single token
     *
     * @param Token $token
     * @param Context $context
     *
     * @return string
     *
     * @throws ExecuteError
     */
    public function partial(Token $token, Context $context): string;

    /**
     * Returns the config to use
     *
     * @return ExecutorConfig
     */
    public function getConfig(): ExecutorConfig;
}