<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Exception;

use Throwable;

/**
 * Error thrown on tokenize
 */
interface TokenizeError extends Throwable
{
}