<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Exception;

use Exception;

/**
 * Thrown when a closing quote is missing
 */
final class MissingCloseQuote extends Exception implements TokenizeError
{
    /**
     * Actual expected quote
     *
     * @var string
     */
    protected $expected;
    /**
     * Input that was given
     *
     * @var string
     */
    protected $got;

    /**
     * MissingCloseQuote constructor
     *
     * @param string $expected
     * @param string $got
     */
    public function __construct(string $expected, string $got)
    {
        parent::__construct(
            sprintf(
                'Expected a `%s`, got `%s`',
                $expected,
                $got
            )
        );
        $this->expected = $expected;
        $this->got = $got;
    }

    /**
     * Returns the expected quote
     *
     * @return string
     */
    public function getExpected(): string
    {
        return $this->expected;
    }

    /**
     * Actual given character
     *
     * @return string
     */
    public function getGot(): string
    {
        return $this->got;
    }
}