<?php
declare(strict_types=1);

namespace LessPlate\Lexer;

use LessPlate\Config\Lexer\LexerConfig;
use LessPlate\Input\Input;
use LessPlate\Lexer\Exception\TokenizeError;
use LessPlate\Lexer\TokenStream\TokenStream;

/**
 * Lexer to tokenize input
 */
interface Lexer
{
    /**
     * Tokenize the input to the token stream
     *
     * @param Input $input
     *
     * @return TokenStream
     *
     * @throws TokenizeError
     */
    public function tokenize(Input $input): TokenStream;

    /**
     * Returns config to be used
     *
     * @return LexerConfig
     */
    public function getConfig(): LexerConfig;
}