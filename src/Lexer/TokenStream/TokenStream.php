<?php
declare(strict_types=1);

namespace LessPlate\Lexer\TokenStream;

use LessPlate\Lexer\Token\Token;

/**
 * Stream to hold tokens
 */
interface TokenStream
{
    /**
     * Returns a token ahead of the current pointer
     *
     * @param int $ahead
     *
     * @return Token
     */
    public function lookahead(int $ahead = 1): Token;

    /**
     * Returns the current token
     *
     * @return Token
     */
    public function getCurrent(): Token;

    /**
     * Returns if there is a token available
     *
     * @return bool
     */
    public function valid(): bool;

    /**
     * Uses the given amount of tokens
     *
     * @param int $amount
     *
     * @return TokenStream
     */
    public function consume(int $amount = 1): TokenStream;
}
