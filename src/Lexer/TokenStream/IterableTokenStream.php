<?php
declare(strict_types=1);

namespace LessPlate\Lexer\TokenStream;

use Generator;
use LessPlate\Lexer\Token\EofToken;
use LessPlate\Lexer\Token\Token;

/**
 * Iterable token stream
 */
final class IterableTokenStream implements TokenStream
{
    /**
     * Generator to obtain tokens
     *
     * @var Generator
     */
    protected $iterable;
    /**
     * Loaded tokens
     *
     * @var Token[]
     */
    protected $tokens = [];

    /**
     * IterableTokenStream constructor
     *
     * @param iterable|Token[] $iterable
     */
    public function __construct(iterable $iterable)
    {
        $this->iterable = $this->getGenerator($iterable);

        $this->load(1);
    }

    /**
     * Returns the generator from the iterable
     *
     * @param iterable|Token[] $iterable
     *
     * @return iterable|Token[]
     */
    protected function getGenerator(iterable $iterable): iterable
    {
        foreach ($iterable as $item) {
            yield $item;
        }
    }

    /**
     * Lookahead n tokens
     *
     * @param int $ahead
     *
     * @return Token
     *
     * @todo check ahead is positive
     */
    public function lookahead(int $ahead = 1): Token
    {
        $extra = count($this->tokens) - 1;

        if ($ahead > $extra && $this->valid()) {
            $this->load($ahead - $extra);
        }

        return $this->tokens[$ahead] ?? new EofToken();
    }

    /**
     * Returns current character
     *
     * @return Token
     */
    public function getCurrent(): Token
    {
        return $this->tokens[0] ?? new EofToken();
    }

    /**
     * Checks if there is a token available
     *
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->tokens[0]);
    }

    /**
     * Consume n tokens
     *
     * @param int $amount
     *
     * @return TokenStream
     *
     * @todo check amount is positive
     */
    public function consume(int $amount = 1): TokenStream
    {
        $needed = $amount - count($this->tokens) + 1;

        if ($needed > 0) {
            $this->load($needed);
        }

        $this->tokens = array_slice($this->tokens, $amount);

        return $this;
    }

    /**
     * Load the tokens
     *
     * @param int $amount
     *
     * @return void
     *
     * @todo throw exception when amount is not positive
     */
    private function load(int $amount = 1): void
    {
        while ($this->iterable->valid() && $amount > 0) {
            $this->tokens[] = $this->iterable->current();
            $this->iterable->next();

            $amount -= 1;
        }
    }
}