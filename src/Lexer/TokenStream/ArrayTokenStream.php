<?php
declare(strict_types=1);

namespace LessPlate\Lexer\TokenStream;

use LessPlate\Lexer\Token\EofToken;
use LessPlate\Lexer\Token\Token;

/**
 * Array implementation of the token stream
 */
final class ArrayTokenStream implements TokenStream
{
    /**
     * Loaded tokens
     *
     * @var Token[]
     */
    protected $tokens;

    /**
     * ArrayTokenStream constructor
     *
     * @param Token[] $tokens
     */
    public function __construct(array $tokens)
    {
        reset($tokens);

        $this->tokens = $tokens;
    }

    /**
     * Lookahead n tokens
     *
     * @param int $ahead
     *
     * @return Token
     *
     * @todo check ahead is positive
     */
    public function lookahead(int $ahead = 1): Token
    {
        return $this->tokens[$ahead] ?? new EofToken();
    }

    /**
     * Returns the current token
     *
     * @return Token
     */
    public function getCurrent(): Token
    {
        return $this->tokens[0] ?? new EofToken();
    }

    /**
     * Returns if there is a token available
     *
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->tokens[0]);
    }

    /**
     * Consume n tokens
     *
     * @param int $amount
     *
     * @return TokenStream
     */
    public function consume(int $amount = 1): TokenStream
    {
        for ($run = 0; $run < $amount; $run += 1) {
            array_shift($this->tokens);
        }

        return $this;
    }
}