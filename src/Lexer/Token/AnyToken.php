<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token;

/**
 * Token to mark anything
 */
final class AnyToken extends AbstractToken
{
    public const NAME = 'ANY';

    /**
     * Returns the name of the token
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}