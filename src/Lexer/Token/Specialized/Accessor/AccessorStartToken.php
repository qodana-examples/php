<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized\Accessor;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Lexer token for opening accessor
 */
final class AccessorStartToken extends AbstractToken implements SpecializedToken
{
    public const NAME = 'ACCESSOR_START';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}