<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized\Accessor;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Token to mark object access
 */
final class AccessorToken extends AbstractToken implements SpecializedToken
{
    public const NAME = 'ACCESSOR';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}