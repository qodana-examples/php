<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized\Accessor;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Lexer token for closing accessor
 */
final class AccessorCloseToken extends AbstractToken implements SpecializedToken
{
    public const NAME = 'ACCESSOR_CLOSE';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}