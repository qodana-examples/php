<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized;

use LessPlate\Lexer\Token\Token;

/**
 * Mark a token as specialized
 */
interface SpecializedToken extends Token
{
}