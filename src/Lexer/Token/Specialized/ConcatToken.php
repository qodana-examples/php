<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Token to mark a parameter concat
 */
final class ConcatToken extends AbstractToken implements SpecializedToken
{
    public const NAME = 'CONCAT';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}