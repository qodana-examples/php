<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized\Group;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Parentheses open token
 */
final class GroupStartToken extends AbstractToken implements SpecializedToken
{
    public const NAME = 'GROUP_START';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}