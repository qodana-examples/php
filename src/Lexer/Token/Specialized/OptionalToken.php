<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Mark value as optional
 */
final class OptionalToken extends AbstractToken implements SpecializedToken
{
    public const NAME = 'OPTIONAL';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}