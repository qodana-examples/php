<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Specialized;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Token to mark a start transform
 */
final class TransformerToken extends AbstractToken implements SpecializedToken
{
    public const NAME = 'TRANSFORMER';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}