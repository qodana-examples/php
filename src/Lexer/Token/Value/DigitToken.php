<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Value;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Mark a digit
 */
final class DigitToken extends AbstractToken
{
    public const NAME = 'DIGIT';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}