<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Value;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Marks the token as a string
 */
final class StringToken extends AbstractToken
{
    public const NAME = 'STRING';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}