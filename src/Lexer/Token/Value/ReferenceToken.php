<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Value;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Marks a reference
 */
final class ReferenceToken extends AbstractToken
{
    public const NAME = 'REFERENCE';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}