<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token;

/**
 * Literal token
 */
final class CharacterToken extends AbstractToken
{
    public const NAME = 'CHARACTER';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}