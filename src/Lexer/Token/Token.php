<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token;

/**
 * Lexer token
 */
interface Token
{
    /**
     * Name of the token
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Returns the line
     *
     * @return int
     */
    public function getLine(): int;

    /**
     * Returns the column
     *
     * @return int
     */
    public function getColumn(): int;

    /**
     * Actual input
     *
     * @return string
     */
    public function getInput(): string;
}