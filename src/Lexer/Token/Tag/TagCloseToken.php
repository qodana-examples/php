<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Tag;

/**
 * Mark a token as a tag close
 */
interface TagCloseToken extends TagToken
{
}