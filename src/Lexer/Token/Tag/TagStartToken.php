<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Tag;

/**
 * Mark a tag start
 */
interface TagStartToken extends TagToken
{
    public const GROUP = 'TAG_START';
}