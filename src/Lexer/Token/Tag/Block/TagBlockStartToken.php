<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Tag\Block;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Tag\TagStartToken;

/**
 * Token to mark a block open
 */
final class TagBlockStartToken extends AbstractToken implements TagStartToken
{
    public const NAME = 'TAG_BLOCK_START';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}