<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Tag\Block;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Tag\TagCloseToken;

/**
 * Token to mark a close block
 */
final class TagBlockCloseToken extends AbstractToken implements TagCloseToken
{
    public const NAME = 'TAG_BLOCK_CLOSE';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}