<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Tag\Expression;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Tag\TagStartToken;

/**
 * Token for tag open
 */
final class TagExpressionStartToken extends AbstractToken implements TagStartToken
{
    public const NAME = 'TAG_EXPRESSION_START';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}