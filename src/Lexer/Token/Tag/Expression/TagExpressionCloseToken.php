<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Tag\Expression;

use LessPlate\Lexer\Token\AbstractToken;
use LessPlate\Lexer\Token\Tag\TagCloseToken;

/**
 * Tag close token
 */
final class TagExpressionCloseToken extends AbstractToken implements TagCloseToken
{
    public const NAME = 'TAG_EXPRESSION_CLOSE';

    /**
     * Returns the token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}