<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Tag;

use LessPlate\Lexer\Token\Token;

/**
 * Mark a token for a tag
 */
interface TagToken extends Token
{
}