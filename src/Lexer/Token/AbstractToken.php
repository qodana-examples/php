<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token;

use InvalidArgumentException;

/**
 * Adds the input and line
 */
abstract class AbstractToken implements Token
{
    /**
     * Input that matched
     *
     * @var string
     */
    private $input;
    /**
     * Line for the token
     *
     * @var int
     */
    private $line;
    /**
     * Column for the token
     *
     * @var int
     */
    private $column;

    /**
     * AbstractToken constructor
     *
     * @param string $input
     * @param int $line
     * @param int $column
     *
     * @todo use custom exception
     */
    public function __construct(string $input, int $line, int $column)
    {
        if ($input === '') {
            throw new InvalidArgumentException('No empty string');
        }

        $this->input = $input;
        $this->line = $line;
        $this->column = $column;
    }

    /**
     * Returns matched input
     *
     * @return string
     */
    public function getInput(): string
    {
        return $this->input;
    }

    /**
     * Returns token line
     *
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * Returns token column
     *
     * @return int
     */
    public function getColumn(): int
    {
        return $this->column;
    }
}
