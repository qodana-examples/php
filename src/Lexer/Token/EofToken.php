<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token;

/**
 * End of file token
 */
final class EofToken implements Token
{
    public const NAME = 'EOF';

    /**
     * Name of the token
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * Input of the token
     *
     * @return string
     */
    public function getInput(): string
    {
        return 'EOF';
    }

    /**
     * Line of the token
     *
     * @return int
     */
    public function getLine(): int
    {
        return 0;
    }

    /**
     * Column of the token
     *
     * @return int
     */
    public function getColumn(): int
    {
        return 0;
    }
}