<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Operator;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Token for arithmetic
 */
final class ArithmeticOperatorToken  extends AbstractToken implements OperatorToken
{
    public const NAME = 'ARITHMETIC_OPERATOR';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}