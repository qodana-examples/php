<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Operator;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Token for logical operator
 */
final class LogicalOperatorToken extends AbstractToken implements OperatorToken
{
    public const NAME = 'LOGICAL_OPERATOR';

    /**
     * Returns token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}