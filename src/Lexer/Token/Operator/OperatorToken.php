<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Operator;

use LessPlate\Lexer\Token\Token;

/**
 * Marks a token as an operator
 */
interface OperatorToken extends Token
{
}