<?php
declare(strict_types=1);

namespace LessPlate\Lexer\Token\Operator;

use LessPlate\Lexer\Token\AbstractToken;

/**
 * Token for chain
 */
final class ChainOperatorToken extends AbstractToken implements OperatorToken
{
    public const NAME = 'CHAIN_OPERATOR';

    /**
     * Return token name
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}