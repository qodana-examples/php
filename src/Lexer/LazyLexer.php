<?php
declare(strict_types=1);

namespace LessPlate\Lexer;

use LessPlate\Config\Lexer\LexerConfig;
use LessPlate\Input\Input;
use LessPlate\Lexer\Exception\MissingCloseQuote;
use LessPlate\Lexer\Token\AnyToken;
use LessPlate\Lexer\Token\CharacterToken;
use LessPlate\Lexer\Token\Operator\OperatorToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;
use LessPlate\Lexer\Token\Token;
use LessPlate\Lexer\Token\Value\DigitToken;
use LessPlate\Lexer\Token\Value\ReferenceToken;
use LessPlate\Lexer\Token\Value\StringToken;
use LessPlate\Lexer\TokenStream\IterableTokenStream;
use LessPlate\Lexer\TokenStream\TokenStream;
use RuntimeException;

/**
 * Lazy tokenize the input
 */
final class LazyLexer implements Lexer
{
    /**
     * Config to use
     *
     * @var LexerConfig
     */
    private $config;

    /**
     * LazyLexer constructor
     *
     * @param LexerConfig $config
     */
    public function __construct(LexerConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Returns config from LazyLexer
     *
     * @return LexerConfig
     */
    public function getConfig(): LexerConfig
    {
        return $this->config;
    }

    /**
     * Tokenize input
     *
     * @param Input $input
     *
     * @return TokenStream
     *
     * @throws MissingCloseQuote
     */
    public function tokenize(Input $input): TokenStream
    {
        return new IterableTokenStream(
            $this->getIterable($input)
        );
    }

    /**
     * Lazy lex
     *
     * @param Input $input
     *
     * @return iterable|Token[]
     *
     * @throws MissingCloseQuote
     */
    private function getIterable(Input $input): iterable
    {
        while ($input->valid()) {
            if ($this->isTagStart($input)) {
                yield from $this->tag($input);

                continue;
            }

            yield from $this->text($input);
        }
    }

    /**
     * Is tag start
     *
     * @param Input $input
     *
     * @return bool
     */
    private function isTagStart(Input $input): bool
    {
        foreach ($this->config->getTagConfigs() as $tagConfig) {
            if ($input->isSequence($tagConfig->getStartSequence())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Lex tag token
     *
     * @param Input $input
     *
     * @return iterable|Token[]
     *
     * @throws MissingCloseQuote
     */
    private function tag(Input $input): iterable
    {
        foreach ($this->config->getTagConfigs() as $tagConfig) {
            if ($input->isSequence($tagConfig->getStartSequence())) {
                yield $tagConfig->makeStartToken($input->getCurrentLine(), $input->getCurrentColumn());

                $input->consume(strlen($tagConfig->getStartSequence()));

                yield from $this->expression(
                    $input,
                    $tagConfig->getCloseSequence()
                );

                if ($input->isSequence($tagConfig->getCloseSequence())) {
                    yield $tagConfig->makeCloseToken($input->getCurrentLine(), $input->getCurrentColumn());

                    $input->consume(strlen($tagConfig->getCloseSequence()));
                }

                break;
            }
        }
    }

    /**
     * Parse expression
     *
     * @param Input $input
     * @param string $closeSequence
     *
     * @return iterable|Token[]
     *
     * @throws MissingCloseQuote
     */
    private function expression(Input $input, string $closeSequence): iterable
    {
        while ($input->valid() && !$input->isSequence($closeSequence)) {
            if (in_array($input->getCurrent(), [' ', "\t", PHP_EOL], true)) {
                $input->consume();
            } elseif ($this->isOperator($input)) {
                yield $this->operator($input);
            } elseif ($this->isSpecialized($input)) {
                yield $this->specialized($input);
            } elseif (ctype_alpha($input->getCurrent())) {
                yield $this->reference($input);
            } elseif (in_array($input->getCurrent(), ['"', "'"], true)) {
                yield $this->string($input);
            } elseif ($this->isDigit($input)) {
                yield $this->digit($input);
            } else {
                yield new CharacterToken($input->getCurrent(), $input->getCurrentLine(), $input->getCurrentColumn());

                $input->consume();
            }
        }
    }

    /**
     * Parse text
     *
     * @param Input $input
     *
     * @return Token[]|iterable
     */
    private function text(Input $input): iterable
    {
        $column = $input->getCurrentColumn();
        $line = $input->getCurrentLine();
        $text = '';

        do {
            $escape = $input->locateSequence('\\');
            $sequencePosition = null;

            foreach ($this->config->getTagConfigs() as $tagConfig) {
                $position = $input->locateSequence($tagConfig->getStartSequence());

                if (is_int($position)) {
                    $sequencePosition = is_int($sequencePosition)
                        ? min($sequencePosition, $position)
                        : $position;
                }
            }

            if ($sequencePosition !== null && ($escape === null || $sequencePosition < $escape)) {
                $text .= $input->getSequence($sequencePosition);
                $input->consume($sequencePosition);
            } elseif ($escape !== null && ($sequencePosition === null || $sequencePosition > $escape)) {
                $text = '';

                if ($escape > 0) {
                    $text .= $input->getSequence($escape);
                    $input->consume($escape);
                }

                $input->consume(1);
                $text .= $input->getCurrent();
                $input->consume();
            } else {
                while ($input->valid()) {
                    $text .= $input->getCurrent();
                    $input->consume();
                }
            }
        } while ($input->valid() && !$this->isTagStart($input));

        yield new AnyToken($text, $line, $column);
    }

    /**
     * Parse reference
     *
     * @param Input $input
     *
     * @return Token
     */
    private function reference(Input $input): Token
    {
        $column = $input->getCurrentColumn();
        $line = $input->getCurrentLine();

        $reference = $input->getCurrent();
        $input->consume();

        while ($input->valid() && (ctype_alnum($input->getCurrent()) || in_array($input->getCurrent(), ['_']))) {
            $reference .= $input->getCurrent();
            $input->consume();
        }

        return new ReferenceToken($reference, $line, $column);
    }

    /**
     * Parse string
     *
     * @param Input $input
     *
     * @return StringToken
     *
     * @throws MissingCloseQuote
     */
    private function string(Input $input): StringToken
    {
        $start = $input->getCurrent();
        $input->consume();

        $column = $input->getCurrentColumn();
        $line = $input->getCurrentLine();

        $string = '';

        while ($input->valid() && $input->getCurrent() !== $start) {
            $char = $input->getCurrent();

            if ($char === '\\' && $input->lookahead() === $start) {
                $input->consume();
                $char = $input->getCurrent();
            }

            $string .= $char;
            $input->consume();
        }

        if ($input->getCurrent() !== $start) {
            $got = $input->valid()
                ? $input->getCurrent()
                : 'EOF';

            throw new MissingCloseQuote($start, $got);
        }

        $input->consume();

        return new StringToken($string, $line, $column);
    }

    /**
     * Checks if the input is a digit
     *
     * @param Input $input
     *
     * @return bool
     */
    private function isDigit(Input $input): bool
    {
        return ctype_digit($input->getCurrent());
    }

    /**
     * Parse digit
     *
     * @param Input $input
     *
     * @return DigitToken
     */
    private function digit(Input $input): DigitToken
    {
        $column = $input->getCurrentColumn();
        $line = $input->getCurrentLine();
        $number = '';

        do {
            $number .= $input->getCurrent();
            $input->consume();
        } while (ctype_digit($input->getCurrent()) && $input->valid());

        return new DigitToken($number, $line, $column);
    }

    /**
     * Check if the current input matches an operator
     *
     * @param Input $input
     *
     * @return bool
     */
    private function isOperator(Input $input): bool
    {
        foreach ($this->config->getOperatorConfigs() as $config) {
            if ($input->isSequence($config->getSequence())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Operator lexer
     *
     * @param Input $input
     *
     * @return OperatorToken
     *
     * @todo use custom exception
     */
    private function operator(Input $input): OperatorToken
    {
        foreach ($this->config->getOperatorConfigs() as $config) {
            if ($input->isSequence($config->getSequence())) {
                $token = $config->makeToken(
                    $config->getSequence(),
                    $input->getCurrentLine(),
                    $input->getCurrentColumn()
                );

                $input->consume(strlen($config->getSequence()));

                return $token;
            }
        }

        throw new RuntimeException('Unknown operator');
    }

    /**
     * Check if current input is a specialized
     *
     * @param Input $input
     *
     * @return bool
     */
    private function isSpecialized(Input $input): bool
    {
        foreach ($this->config->getSpecializedConfigs() as $config) {
            if ($input->isSequence($config->getSequence())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a SpecializedToken
     *
     * @param Input $input
     *
     * @return SpecializedToken
     *
     * @todo use custom exception
     */
    private function specialized(Input $input): SpecializedToken
    {
        foreach ($this->config->getSpecializedConfigs() as $config) {
            if ($input->isSequence($config->getSequence())) {
                $token = $config->makeToken($input->getCurrentLine(), $input->getCurrentColumn());
                $input->consume(strlen($config->getSequence()));

                return $token;
            }
        }

        throw new RuntimeException();
    }
}
