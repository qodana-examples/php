<?php
declare(strict_types=1);

namespace LessPlate\Config;

use LessPlate\Config\Executor\DefaultExecutorConfig;
use LessPlate\Config\Lexer\DefaultLexerConfig;
use LessPlate\Config\Parser\DefaultParserConfig;

/**
 * Default config
 */
final class DefaultConfig extends AbstractConfig
{
    /**
     * DefaultConfig constructor
     */
    public function __construct()
    {
        parent::__construct(
            new DefaultExecutorConfig(),
            new DefaultLexerConfig(),
            new DefaultParserConfig()
        );
    }
}