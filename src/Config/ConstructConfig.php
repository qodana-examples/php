<?php
declare(strict_types=1);

namespace LessPlate\Config;

/**
 * Require config to be set via construct
 */
final class ConstructConfig extends AbstractConfig
{
}