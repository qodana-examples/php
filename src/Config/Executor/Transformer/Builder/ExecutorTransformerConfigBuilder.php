<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Transformer\Builder;

use LessPlate\Config\Executor\Transformer\ExecutorTransformerConfig;

/**
 * Builder for ExecutorTransformerConfig
 */
interface ExecutorTransformerConfigBuilder
{
    /**
     * Builds ExecutorTransformerConfig
     *
     * @return ExecutorTransformerConfig
     */
    public function build(): ExecutorTransformerConfig;
}