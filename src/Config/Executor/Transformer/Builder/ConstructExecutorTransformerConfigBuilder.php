<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Transformer\Builder;

use LessPlate\Config\Executor\Transformer\ConstructExecutorTransformerConfig;
use LessPlate\Config\Executor\Transformer\DefaultExecutorTransformerConfig;
use LessPlate\Config\Executor\Transformer\ExecutorTransformerConfig;
use LessPlate\Transformer\Transformer;

/**
 * Builder for ConstructExecutorTransformerConfig
 */
final class ConstructExecutorTransformerConfigBuilder implements ExecutorTransformerConfigBuilder
{
    /**
     * If the default transformers should be used
     *
     * @var bool
     */
    private $defaultTransformers = true;
    /**
     * Extra transformers to use
     *
     * @var array|Transformer[]
     */
    private $extraTransformers = [];

    /**
     * Builds transformer config
     *
     * @return ExecutorTransformerConfig
     */
    public function build(): ExecutorTransformerConfig
    {
        return new ConstructExecutorTransformerConfig(
            $this->getTransformers()
        );
    }

    /**
     * If the default transformers should be added
     *
     * @param bool $use
     *
     * @return $this
     */
    public function withDefaultTransformers(bool $use)
    {
        $this->defaultTransformers = $use;

        return $this;
    }

    /**
     * Adds the transformer
     *
     * @param string $reference
     * @param Transformer $transformer
     *
     * @return $this
     */
    public function addExtraTransformer(string $reference, Transformer $transformer)
    {
        $this->extraTransformers[strtolower($reference)] = $transformer;

        return $this;
    }

    /**
     * Returns transformers to use
     *
     * @return iterable|Transformer[]
     */
    private function getTransformers(): iterable
    {
        if ($this->defaultTransformers) {
            yield from DefaultExecutorTransformerConfig::getDefaultTransformers();
        }

        yield from $this->extraTransformers;
    }
}