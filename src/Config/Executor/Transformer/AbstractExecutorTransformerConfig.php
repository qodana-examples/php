<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Transformer;

use LessPlate\Transformer\Transformer;
use RuntimeException;

/**
 * Adds base setup
 */
abstract class AbstractExecutorTransformerConfig implements ExecutorTransformerConfig
{
    /**
     * Holds transformers to use
     *
     * @var array|Transformer[]
     */
    private $transformers = [];

    /**
     * DefaultTransformerConfig constructor
     *
     * @param iterable|string[]|Transformer[] $transformers
     */
    public function __construct(iterable $transformers)
    {
        foreach ($transformers as $reference => $transformer) {
            $this->addTransformer($reference, $transformer);
        }
    }

    /**
     * Adds the transformer
     *
     * @param string $reference
     * @param Transformer $transformer
     *
     * @return void
     */
    private function addTransformer(string $reference, Transformer $transformer): void
    {
        $this->transformers[strtolower($reference)] = $transformer;
    }

    /**
     * Returns transformer from the given reference
     *
     * @param string $reference
     *
     * @return Transformer
     *
     * @todo use custom exception
     */
    public function getByReference(string $reference): Transformer
    {
        $reference = strtolower($reference);

        if (!array_key_exists($reference, $this->transformers)) {
            throw new RuntimeException(
                sprintf(
                    'No transformer with %s',
                    $reference
                )
            );
        }

        return $this->transformers[$reference];
    }
}