<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Transformer;

/**
 * Construct based config
 */
final class ConstructExecutorTransformerConfig extends AbstractExecutorTransformerConfig
{
}