<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Transformer;

use LessPlate\Transformer\Transformer;

/**
 * Config for transformer setup
 */
interface ExecutorTransformerConfig
{
    /**
     * Returns a transformer by the given reference
     *
     * @param string $reference
     *
     * @return Transformer
     */
    public function getByReference(string $reference): Transformer;
}