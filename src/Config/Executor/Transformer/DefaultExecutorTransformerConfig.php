<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Transformer;

use LessPlate\Transformer\Encode;
use LessPlate\Transformer\Escape\HtmlEscapeTransformer;
use LessPlate\Transformer\LengthTransformer;
use LessPlate\Transformer\Number;
use LessPlate\Transformer\OnEmptyTransformer;
use LessPlate\Transformer\OnNullTransformer;
use LessPlate\Transformer\Raw;
use LessPlate\Transformer\String\Nl2BrTransformer;
use LessPlate\Transformer\String\StripTagsTransformer;
use LessPlate\Transformer\Transformer;

/**
 * Default transformer config
 */
final class DefaultExecutorTransformerConfig extends AbstractExecutorTransformerConfig
{
    /**
     * DefaultTransformerConfig constructor
     */
    public function __construct()
    {
        parent::__construct(self::getDefaultTransformers());
    }

    /**
     * Returns default transformers
     *
     * @return iterable|Transformer[]
     */
    public static function getDefaultTransformers(): iterable
    {
        return [
            'json' => new Encode\JsonTransformer(),

            'htmlEscape' => new HtmlEscapeTransformer(),

            'ceil' => new Number\CeilTransformer(),
            'date' => new Number\DateTransformer(),
            'floor' => new Number\FloorTransformer(),
            'round' => new Number\RoundTransformer(),
            'numberFormat' => new Number\FormatTransformer(),

            'nl2br' => new Nl2BrTransformer(),
            'stripTags' => new StripTagsTransformer(),

            'raw' => new Raw\RawTransformer(),

            'length' => new LengthTransformer(),
            'onEmpty' => new OnEmptyTransformer(),
            'onNull' => new OnNullTransformer(),
        ];
    }
}