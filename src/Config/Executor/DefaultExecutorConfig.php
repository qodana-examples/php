<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor;

use LessPlate\Config\Executor\Transformer\DefaultExecutorTransformerConfig;
use LessPlate\Config\Executor\Transformer\ExecutorTransformerConfig;
use LessPlate\Executor\Tag\Block\DelegateTagBlockExecutor;
use LessPlate\Executor\Tag\Block\ForInTagBlockExecutor;
use LessPlate\Executor\Tag\Block\IfTagBlockExecutor;
use LessPlate\Executor\Tag\Block\ScopeTagBlockExecutor;
use LessPlate\Executor\Tag\Block\TagBlockExecutor;
use LessPlate\Executor\Util\Arithmetic as ArithmeticExecutor;
use LessPlate\Executor\Util\Chain as ChainExecutor;
use LessPlate\Executor\Util\DelegateUtilExecutor;
use LessPlate\Executor\Util\Logical as LogicalExecutor;
use LessPlate\Executor\Util\TransformerUtilExecutor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Executor\Util\Value as ValueExector;
use LessPlate\Parser\Token\Block\ForInBlockToken;
use LessPlate\Parser\Token\Block\IfBlockToken;
use LessPlate\Parser\Token\Block\ScopeBlockToken;
use LessPlate\Parser\Token\Expression\Arithmetic as ArithmeticToken;
use LessPlate\Parser\Token\Expression\Chain as ChainToken;
use LessPlate\Parser\Token\Expression\Logical as LogicalToken;
use LessPlate\Parser\Token\Expression\TransformerExpressionToken;
use LessPlate\Parser\Token\Value as ValueToken;
use LessPlate\Transformer\Escape\HtmlEscapeTransformer;
use LessPlate\Transformer\Transformer;

/**
 * Default executor config
 */
final class DefaultExecutorConfig extends AbstractExecutorConfig
{
    /**
     * DefaultExecutorConfig constructor
     */
    public function __construct()
    {
        parent::__construct(
            self::getDefaultTransformerConfig(),
            self::getDefaultExpressionExecutor(),
            self::getDefaultTagBlockExecutor(),
            self::getDefaultTagEscaper()
        );
    }

    /**
     * Returns default transformer config
     *
     * @return ExecutorTransformerConfig
     */
    public static function getDefaultTransformerConfig(): ExecutorTransformerConfig
    {
        return new DefaultExecutorTransformerConfig();
    }

    /**
     * Returns the expression executor to use
     *
     * @return UtilExecutor
     */
    public static function getDefaultExpressionExecutor(): UtilExecutor
    {
        return new DelegateUtilExecutor(
            [
                ArithmeticToken\AdditionArithmeticExpressionToken::NAME => new ArithmeticExecutor\AdditionUtilExecutor(),
                ArithmeticToken\DivideArithmeticExpressionToken::NAME => new ArithmeticExecutor\DivideUtilExecutor(),
                ArithmeticToken\ModuloArithmeticExpressionToken::NAME => new ArithmeticExecutor\ModuloUtilExecutor(),
                ArithmeticToken\MultiplyArithmeticExpressionToken::NAME => new ArithmeticExecutor\MultiplyUtilExecutor(),
                ArithmeticToken\PowerArithmeticExpressionToken::NAME => new ArithmeticExecutor\PowerUtilExecutor(),
                ArithmeticToken\SubtractArithmeticExpressionToken::NAME => new ArithmeticExecutor\SubtractUtilExecutor(),

                ChainToken\AndLogicalExpressionToken::NAME => new ChainExecutor\AndChainUtilExecutor(),
                ChainToken\OrLogicalExpressionToken::NAME => new ChainExecutor\OrChainUtilExecutor(),

                LogicalToken\ContainsLogicalExpressionToken::NAME => new LogicalExecutor\ContainsUtilExecutor(),
                LogicalToken\EqualsLogicalExpressionToken::NAME => new LogicalExecutor\EqualsLogicalUtilExecutor(),
                LogicalToken\GreaterThanLogicalExpressionToken::NAME => new LogicalExecutor\GreaterThanLogicalUtilExecutor(),
                LogicalToken\GreaterThanOrEqualsLogicalExpressionToken::NAME => new LogicalExecutor\GreaterThanOrEqualsLogicalUtilExecutor(),
                LogicalToken\LowerThanLogicalExpressionToken::NAME => new LogicalExecutor\LowerThanLogicalUtilExecutor(),
                LogicalToken\LowerThanOrEqualsLogicalExpressionToken::NAME => new LogicalExecutor\LowerThanOrEqualsLogicalUtilExecutor(),
                LogicalToken\NotContainsLogicalExpressionToken::NAME => new LogicalExecutor\NotContainsUtilExecutor(),
                LogicalToken\NotEqualsLogicalExpressionToken::NAME => new LogicalExecutor\NotEqualsLogicalUtilExecutor(),

                TransformerExpressionToken::NAME => new TransformerUtilExecutor(),

                ValueToken\Number\FloatValueToken::NAME => new ValueExector\Number\FloatValueUtilExecutor(),
                ValueToken\Number\IntegerValueToken::NAME => new ValueExector\Number\IntegerValueUtilExecutor(),
                ValueToken\DynamicValueToken::NAME => new ValueExector\DynamicValueUtilExecutor(
                    [
                        'null' => null,
                        'true' => true,
                        'false' => false,
                    ]
                ),
                ValueToken\StringValueToken::NAME => new ValueExector\StringValueUtilExecutor(),
            ]
        );
    }

    /**
     * Returns default tag block executor
     *
     * @return TagBlockExecutor
     */
    public static function getDefaultTagBlockExecutor(): TagBlockExecutor
    {
        return new DelegateTagBlockExecutor(
            [
                IfBlockToken::NAME => new IfTagBlockExecutor(),
                ForInBlockToken::NAME => new ForInTagBlockExecutor(),
                ScopeBlockToken::NAME => new ScopeTagBlockExecutor(),
            ]
        );
    }

    /**
     * Returns default tag escaper
     *
     * @return Transformer|null
     */
    public static function getDefaultTagEscaper(): ?Transformer
    {
        return new HtmlEscapeTransformer();
    }
}
