<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor;

use LessPlate\Config\Executor\Transformer\ExecutorTransformerConfig;
use LessPlate\Executor\Tag\Block\TagBlockExecutor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Transformer\Transformer;

/**
 * Config for executor
 */
interface ExecutorConfig
{
    /**
     * Returns transformer config
     *
     * @return ExecutorTransformerConfig
     */
    public function getTransformerConfig(): ExecutorTransformerConfig;

    /**
     * Returns the expression executor
     *
     * @return UtilExecutor
     */
    public function getExpressionExecutor(): UtilExecutor;

    /**
     * Returns tag block executors
     *
     * @return TagBlockExecutor
     */
    public function getTagBlockExecutor(): TagBlockExecutor;

    /**
     * Used for auto escaping, when non no auto escaping is done
     *
     * @return Transformer|null
     */
    public function getTagEscaper(): ?Transformer;
}