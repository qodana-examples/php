<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Builder;

use LessPlate\Config\Executor\ConstructExecutorConfig;
use LessPlate\Config\Executor\DefaultExecutorConfig;
use LessPlate\Config\Executor\ExecutorConfig;
use LessPlate\Config\Executor\Transformer\ExecutorTransformerConfig;
use LessPlate\Transformer\Transformer;

/**
 * Builder for ConstructExecutorConfig
 */
final class ConstructExecutorConfigBuilder implements ExecutorConfigBuilder
{
    /**
     * Transformer config to use
     *
     * @var ExecutorTransformerConfig
     */
    private $transformerConfig;
    /**
     * Which transformer to use for tag escaping
     *
     * @var Transformer|null
     */
    private $tagEscaper;

    /**
     * Builds ConstructExecutorConfig
     *
     * @return ExecutorConfig
     */
    public function build(): ExecutorConfig
    {
        return new ConstructExecutorConfig(
            $this->transformerConfig ?? DefaultExecutorConfig::getDefaultTransformerConfig(),
            DefaultExecutorConfig::getDefaultExpressionExecutor(),
            DefaultExecutorConfig::getDefaultTagBlockExecutor(),
            $this->tagEscaper ?? DefaultExecutorConfig::getDefaultTagEscaper()
        );
    }

    /**
     * Transformer config to be used
     *
     * @param ExecutorTransformerConfig $transformerConfig
     *
     * @return $this
     */
    public function withTransformerConfig(ExecutorTransformerConfig $transformerConfig)
    {
        $this->transformerConfig = $transformerConfig;

        return $this;
    }

    /**
     * Tag escaper to be used
     *
     * @param Transformer $transformer
     *
     * @return $this
     */
    public function withTagEscaper(Transformer $transformer)
    {
        $this->tagEscaper = $transformer;

        return $this;
    }
}