<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor\Builder;

use LessPlate\Config\Executor\ExecutorConfig;

/**
 * Builder for ExecutorConfig
 */
interface ExecutorConfigBuilder
{
    /**
     * Builds ExecutorConfig
     *
     * @return ExecutorConfig
     */
    public function build(): ExecutorConfig;
}