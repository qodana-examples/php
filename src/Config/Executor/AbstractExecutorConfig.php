<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor;

use LessPlate\Config\Executor\Transformer\ExecutorTransformerConfig;
use LessPlate\Executor\Tag\Block\TagBlockExecutor;
use LessPlate\Executor\Util\UtilExecutor;
use LessPlate\Transformer\Transformer;

/**
 * Adds the base setup
 */
abstract class AbstractExecutorConfig implements ExecutorConfig
{
    /**
     * Transformer config
     *
     * @var ExecutorTransformerConfig
     */
    private $transformerConfig;
    /**
     * Executor to use for expression
     *
     * @var UtilExecutor
     */
    private $expressionExecutor;
    /**
     * Executor to use for tag blocks
     *
     * @var TagBlockExecutor
     */
    private $tagBlockExecutor;
    /**
     * Tag escaper to use
     *
     * @var Transformer|null
     */
    private $tagEscaper;

    /**
     * AbstractExecutorConfig constructor
     *
     * @param ExecutorTransformerConfig $transformerConfig
     * @param UtilExecutor $expressionExecutor
     * @param TagBlockExecutor $tagBlockExecutor
     * @param Transformer|null $tagEscaper
     */
    public function __construct(ExecutorTransformerConfig $transformerConfig, UtilExecutor $expressionExecutor, TagBlockExecutor $tagBlockExecutor, ?Transformer $tagEscaper)
    {
        $this->transformerConfig = $transformerConfig;
        $this->expressionExecutor = $expressionExecutor;
        $this->tagBlockExecutor = $tagBlockExecutor;
        $this->tagEscaper = $tagEscaper;
    }

    /**
     * Returns transformerConfig from AbstractExecutorConfig
     *
     * @return ExecutorTransformerConfig
     */
    public function getTransformerConfig(): ExecutorTransformerConfig
    {
        return $this->transformerConfig;
    }

    /**
     * Returns expressionExecutor from AbstractExecutorConfig
     *
     * @return UtilExecutor
     */
    public function getExpressionExecutor(): UtilExecutor
    {
        return $this->expressionExecutor;
    }

    /**
     * Returns tagBlockExecutor from AbstractExecutorConfig
     *
     * @return TagBlockExecutor
     */
    public function getTagBlockExecutor(): TagBlockExecutor
    {
        return $this->tagBlockExecutor;
    }

    /**
     * Returns tagEscaper from AbstractExecutorConfig
     *
     * @return Transformer|null
     */
    public function getTagEscaper(): ?Transformer
    {
        return $this->tagEscaper;
    }
}