<?php
declare(strict_types=1);

namespace LessPlate\Config\Executor;

/**
 * Construct based config
 */
final class ConstructExecutorConfig extends AbstractExecutorConfig
{
}