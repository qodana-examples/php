<?php
declare(strict_types=1);

namespace LessPlate\Config\Builder;

use LessPlate\Config\Config;

/**
 * Builder for config
 */
interface ConfigBuilder
{
    /**
     * Builds config
     *
     * @return Config
     */
    public function build(): Config;
}