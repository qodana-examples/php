<?php
declare(strict_types=1);

namespace LessPlate\Config\Builder;

use LessPlate\Config\Config;
use LessPlate\Config\ConstructConfig;
use LessPlate\Config\Executor\DefaultExecutorConfig;
use LessPlate\Config\Executor\ExecutorConfig;
use LessPlate\Config\Lexer\DefaultLexerConfig;
use LessPlate\Config\Lexer\LexerConfig;
use LessPlate\Config\Parser\DefaultParserConfig;
use LessPlate\Config\Parser\ParserConfig;

/**
 * Builder for construct config
 */
final class ConstructConfigBuilder implements ConfigBuilder
{
    /**
     * Executor config to use
     *
     * @var ExecutorConfig
     */
    private $executorConfig;
    /**
     * Lexer config to use
     *
     * @var LexerConfig
     */
    private $lexerConfig;
    /**
     * Parser config to use
     *
     * @var ParserConfig
     */
    private $parserConfig;

    /**
     * Builds config to use
     *
     * @return Config
     */
    public function build(): Config
    {
        return new ConstructConfig(
            $this->executorConfig ?? new DefaultExecutorConfig(),
            $this->lexerConfig ?? new DefaultLexerConfig(),
            $this->parserConfig ?? new DefaultParserConfig()
        );
    }

    /**
     * With the given executor config to use, on null default
     *
     * @param ExecutorConfig $executorConfig
     *
     * @return $this
     */
    public function withExecutorConfig(ExecutorConfig $executorConfig)
    {
        $this->executorConfig = $executorConfig;

        return $this;
    }

    /**
     * With the given lexer config to use, on null default
     *
     * @param LexerConfig $lexerConfig
     *
     * @return $this
     */
    public function withLexerConfig(LexerConfig $lexerConfig)
    {
        $this->lexerConfig = $lexerConfig;

        return $this;
    }

    /**
     * With the given parser config to use, on null default
     *
     * @param ParserConfig $parserConfig
     *
     * @return $this
     */
    public function withParserConfig(ParserConfig $parserConfig)
    {
        $this->parserConfig = $parserConfig;

        return $this;
    }

    /**
     * Returns executorConfig from ConstructConfigBuilder
     *
     * @return ExecutorConfig
     */
    public function getExecutorConfig(): ExecutorConfig
    {
        return $this->executorConfig;
    }

    /**
     * Returns lexerConfig from ConstructConfigBuilder
     *
     * @return LexerConfig
     */
    public function getLexerConfig(): LexerConfig
    {
        return $this->lexerConfig;
    }

    /**
     * Returns parserConfig from ConstructConfigBuilder
     *
     * @return ParserConfig
     */
    public function getParserConfig(): ParserConfig
    {
        return $this->parserConfig;
    }
}
