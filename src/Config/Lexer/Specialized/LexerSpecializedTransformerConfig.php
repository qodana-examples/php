<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized;

use LessPlate\Lexer\Token\Specialized\SpecializedToken;
use LessPlate\Lexer\Token\Specialized\TransformerToken;

/**
 * Config for transformer
 */
final class LexerSpecializedTransformerConfig implements LexerSpecializedConfig
{
    /**
     * Returns sequence to use
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '|';
    }

    /**
     * Marks the transformer token
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new TransformerToken($this->getSequence(), $line, $column);
    }
}