<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized\Group;

use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Lexer\Token\Specialized\Group\GroupCloseToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Config for group close
 */
final class LexerSpecializedGroupCloseConfig implements LexerSpecializedConfig
{
    /**
     * Sequence to match token
     *
     * @return string
     */
    public function getSequence(): string
    {
        return ')';
    }

    /**
     * Makes token to use
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new GroupCloseToken($this->getSequence(), $line, $column);
    }
}