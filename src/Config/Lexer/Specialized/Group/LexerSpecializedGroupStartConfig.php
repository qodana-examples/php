<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized\Group;

use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Lexer\Token\Specialized\Group\GroupStartToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Group start config token
 */
final class LexerSpecializedGroupStartConfig implements LexerSpecializedConfig
{
    /**
     * Sequence for the token
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '(';
    }

    /**
     * Makes the token to use
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new GroupStartToken($this->getSequence(), $line, $column);
    }
}