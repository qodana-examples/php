<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized;

use LessPlate\Lexer\Token\Specialized\OptionalToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Config for optional token
 */
final class LexerSpecializedOptionalConfig implements LexerSpecializedConfig
{
    /**
     * Returns the sequence to use
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '?';
    }

    /**
     * Makes the optional token
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new OptionalToken($this->getSequence(), $line, $column);
    }
}