<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized;

use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Config for specialized tokens
 */
interface LexerSpecializedConfig
{
    /**
     * Sequence to match
     *
     * @return string
     */
    public function getSequence(): string;

    /**
     * Makes the token
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken;
}