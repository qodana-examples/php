<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized;

use LessPlate\Lexer\Token\Specialized\ConcatToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Config for concat token
 */
final class LexerSpecializedConcatConfig implements LexerSpecializedConfig
{
    /**
     * Returns the sequence to use
     *
     * @return string
     */
    public function getSequence(): string
    {
        return ',';
    }

    /**
     * Makes the concat token
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new ConcatToken($this->getSequence(), $line, $column);
    }
}