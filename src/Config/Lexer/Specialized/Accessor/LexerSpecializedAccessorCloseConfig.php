<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized\Accessor;

use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Lexer\Token\Specialized\Accessor\AccessorCloseToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Config for accessor close
 */
final class LexerSpecializedAccessorCloseConfig implements LexerSpecializedConfig
{
    /**
     * Sequence to match
     *
     * @return string
     */
    public function getSequence(): string
    {
        return ']';
    }

    /**
     * Makes the token to use
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new AccessorCloseToken($this->getSequence(), $line, $column);
    }
}