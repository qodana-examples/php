<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized\Accessor;

use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Lexer\Token\Specialized\Accessor\AccessorToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Config for accessor
 */
final class LexerSpecializedAccessorConfig implements LexerSpecializedConfig
{
    /**
     * Sequence to match the token to
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '.';
    }

    /**
     * Makes the accessor token
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new AccessorToken($this->getSequence(), $line, $column);
    }
}