<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Specialized\Accessor;

use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Lexer\Token\Specialized\Accessor\AccessorStartToken;
use LessPlate\Lexer\Token\Specialized\SpecializedToken;

/**
 * Config for accessor start
 */
final class LexerSpecializedAccessorStartConfig implements LexerSpecializedConfig
{
    /**
     * Sequence for token to match
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '[';
    }

    /**
     * Makes token to use
     *
     * @param int $line
     * @param int $column
     *
     * @return SpecializedToken
     */
    public function makeToken(int $line, int $column): SpecializedToken
    {
        return new AccessorStartToken($this->getSequence(), $line, $column);
    }
}