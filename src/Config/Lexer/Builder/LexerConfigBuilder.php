<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Builder;

use LessPlate\Config\Lexer\LexerConfig;

/**
 * Builder for LexerConfig
 */
interface LexerConfigBuilder
{
    /**
     * Builds lexer config
     *
     * @return LexerConfig
     */
    public function build(): LexerConfig;
}