<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Builder;

use LessPlate\Config\Lexer\ConstructLexerConfig;
use LessPlate\Config\Lexer\DefaultLexerConfig;
use LessPlate\Config\Lexer\LexerConfig;
use LessPlate\Config\Lexer\Operator\LexerOperatorConfig;
use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Config\Lexer\Tag\LexerTagConfig;

/**
 * Builder for ConstructLexerConfig
 */
final class ConstructLexerConfigBuilder implements LexerConfigBuilder
{
    /**
     * If the default tag configs should be added
     *
     * @var bool
     */
    private $defaultTagConfigs = true;
    /**
     * If the default operator configs should be added
     *
     * @var bool
     */
    private $defaultOperatorConfigs = true;
    /**
     * If the default specialized configs should be added
     *
     * @var bool
     */
    private $defaultSpecializedConfigs = true;
    /**
     * Extra tag configs to use
     *
     * @var array|LexerTagConfig[]
     */
    private $extraTagConfigs = [];
    /**
     * Extra operator configs to use
     *
     * @var array|LexerOperatorConfig[]
     */
    private $extraOperatorConfigs = [];
    /**
     * Extra specialized configs to use
     *
     * @var array|LexerSpecializedConfig[]
     */
    private $extraSpecializedConfigs = [];

    /**
     * Builds config to use
     *
     * @return LexerConfig
     */
    public function build(): LexerConfig
    {
        return new ConstructLexerConfig(
            $this->getTagConfigs(),
            $this->getOperatorConfigs(),
            $this->getSpecializedConfigs()
        );
    }

    /**
     * If default tag configs should be added
     *
     * @param bool $use
     *
     * @return $this
     */
    public function useDefaultTagConfigs(bool $use)
    {
        $this->defaultTagConfigs = $use;

        return $this;
    }

    /**
     * If default operator configs should be added
     *
     * @param bool $use
     *
     * @return $this
     */
    public function useDefaultOperatorConfigs(bool $use)
    {
        $this->defaultOperatorConfigs = $use;

        return $this;
    }

    /**
     * If default specialized configs should be added
     *
     * @param bool $use
     *
     * @return $this
     */
    public function useDefaultSpecializedConfigs(bool $use)
    {
        $this->defaultSpecializedConfigs = $use;

        return $this;
    }

    /**
     * Adds extra tag config
     *
     * @param LexerTagConfig $config
     *
     * @return $this
     */
    public function addExtraTagConfig(LexerTagConfig $config)
    {
        $this->extraTagConfigs[] =  $config;

        return $this;
    }

    /**
     * Adds extra operator config
     *
     * @param LexerOperatorConfig $config
     *
     * @return $this
     */
    public function addExtraOperatorConfig(LexerOperatorConfig $config)
    {
        $this->extraOperatorConfigs[] =  $config;

        return $this;
    }

    /**
     * Adds extra specialized config
     *
     * @param LexerSpecializedConfig $config
     *
     * @return $this
     */
    public function addExtraSpecializedConfig(LexerSpecializedConfig $config)
    {
        $this->extraSpecializedConfigs[] =  $config;

        return $this;
    }

    /**
     * Returns extraTagConfigs from ConstructLexerConfigBuilder
     *
     * @return iterable|LexerTagConfig[]
     */
    public function getTagConfigs(): iterable
    {
        if ($this->defaultTagConfigs) {
            yield from DefaultLexerConfig::getDefaultTagConfigs();
        }

        yield from $this->extraTagConfigs;
    }

    /**
     * Returns extraOperatorConfigs from ConstructLexerConfigBuilder
     *
     * @return iterable|LexerOperatorConfig[]
     */
    public function getOperatorConfigs(): iterable
    {
        if ($this->defaultOperatorConfigs) {
            yield from DefaultLexerConfig::getDefaultOperatorConfigs();
        }

        yield from $this->extraOperatorConfigs;
    }

    /**
     * Returns extraSpecializedConfigs from ConstructLexerConfigBuilder
     *
     * @return iterable|LexerSpecializedConfig[]
     */
    public function getSpecializedConfigs(): iterable
    {
        if ($this->defaultSpecializedConfigs) {
            yield from DefaultLexerConfig::getDefaultSpecializedConfigs();
        }

        yield from $this->extraSpecializedConfigs;
    }
}