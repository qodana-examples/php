<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Chain;

/**
 * And chain config
 */
final class AndChainLexerOperatorConfig extends AbstractChainLexerOperatorConfig
{
    /**
     * AndChainLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('&&');
    }
}