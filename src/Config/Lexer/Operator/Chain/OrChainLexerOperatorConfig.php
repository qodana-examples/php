<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Chain;

/**
 * Or chain config
 */
final class OrChainLexerOperatorConfig extends AbstractChainLexerOperatorConfig
{
    /**
     * OrChainLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('||');
    }
}