<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Chain;

use LessPlate\Config\Lexer\Operator\AbstractLexerOperatorConfig;
use LessPlate\Lexer\Token\Operator\ChainOperatorToken;
use LessPlate\Lexer\Token\Operator\OperatorToken;

/**
 * Base chain operator config
 */
abstract class AbstractChainLexerOperatorConfig extends AbstractLexerOperatorConfig
{
    /**
     * Makes the chain token
     *
     * @param string $sequence
     * @param int $line
     * @param int $column
     *
     * @return OperatorToken
     */
    public function makeToken(string $sequence, int $line, int $column): OperatorToken
    {
        return new ChainOperatorToken(strtolower($sequence), $line, $column);
    }
}
