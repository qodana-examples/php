<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator;

/**
 * Adds the sequence
 */
abstract class AbstractLexerOperatorConfig implements LexerOperatorConfig
{
    /**
     * Sequence to match on
     *
     * @var string
     */
    private $sequence;

    /**
     * AbstractLexerOperatorConfig constructor
     *
     * @param string $sequence
     */
    public function __construct(string $sequence)
    {
        $this->sequence = $sequence;
    }

    /**
     * Returns sequence to match on
     *
     * @return string
     */
    public function getSequence(): string
    {
        return $this->sequence;
    }
}