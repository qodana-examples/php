<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator;

use LessPlate\Lexer\Token\Operator\OperatorToken;

/**
 * Lexer operator config
 */
interface LexerOperatorConfig
{
    /**
     * Sequences to match
     *
     * @return string
     */
    public function getSequence(): string;

    /**
     * Makes the token
     *
     * @param string $sequence
     * @param int $line
     * @param int $column
     *
     * @return OperatorToken
     */
    public function makeToken(string $sequence, int $line, int $column): OperatorToken;
}
