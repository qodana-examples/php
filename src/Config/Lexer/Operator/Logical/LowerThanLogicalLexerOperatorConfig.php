<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Lower than logical config
 */
final class LowerThanLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * LowerThanLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('<');
    }
}