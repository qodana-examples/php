<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Equals logical config
 */
final class EqualsLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * EqualsLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('==');
    }
}