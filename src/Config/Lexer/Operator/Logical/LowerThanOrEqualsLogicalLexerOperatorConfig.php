<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Lower than or equals config
 */
final class LowerThanOrEqualsLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * LowerThanOrEqualsLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('<=');
    }
}