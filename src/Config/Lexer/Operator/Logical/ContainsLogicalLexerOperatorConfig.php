<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Contains logical config
 */
final class ContainsLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * EqualsLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('~~');
    }
}
