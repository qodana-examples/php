<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Greater than or equals logical config
 */
final class GreaterThanOrEqualsLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * GreaterThanOrEqualsLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('>=');
    }
}