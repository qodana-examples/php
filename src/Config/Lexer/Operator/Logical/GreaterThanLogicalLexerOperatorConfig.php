<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Greater than logical config
 */
final class GreaterThanLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * GreaterThanLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('>');
    }
}