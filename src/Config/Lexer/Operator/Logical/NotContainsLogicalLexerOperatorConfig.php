<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Not in logical config
 */
final class NotContainsLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * EqualsLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('!~');
    }
}
