<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

use LessPlate\Config\Lexer\Operator\AbstractLexerOperatorConfig;
use LessPlate\Lexer\Token\Operator\LogicalOperatorToken;
use LessPlate\Lexer\Token\Operator\OperatorToken;

/**
 * Base logical operator config
 */
abstract class AbstractLogicalLexerOperatorConfig extends AbstractLexerOperatorConfig
{
    /**
     * Makes the token
     *
     * @param string $sequence
     * @param int $line
     * @param int $column
     *
     * @return OperatorToken
     */
    public function makeToken(string $sequence, int $line, int $column): OperatorToken
    {
        return new LogicalOperatorToken(strtolower($sequence), $line, $column);
    }
}
