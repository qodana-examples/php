<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Logical;

/**
 * Not equals logical config
 */
final class NotEqualsLogicalLexerOperatorConfig extends AbstractLogicalLexerOperatorConfig
{
    /**
     * NotEqualsLogicalLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('!=');
    }
}