<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Arithmetic;

/**
 * Modulo config
 */
final class ModuloArithmeticLexerOperatorConfig extends AbstractArithmeticLexerOperatorConfig
{
    /**
     * ModuloArithmeticLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('%');
    }
}