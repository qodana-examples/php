<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Arithmetic;

/**
 * Multiply config
 */
final class MultiplyArithmeticLexerOperatorConfig extends AbstractArithmeticLexerOperatorConfig
{
    /**
     * MultiplyArithmeticLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('*');
    }
}