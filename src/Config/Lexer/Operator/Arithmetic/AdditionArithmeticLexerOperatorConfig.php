<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Arithmetic;

/**
 * Addition config
 */
final class AdditionArithmeticLexerOperatorConfig extends AbstractArithmeticLexerOperatorConfig
{
    /**
     * AdditionArithmeticLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('+');
    }
}