<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Arithmetic;

/**
 * Divide config
 */
final class DivideArithmeticLexerOperatorConfig extends AbstractArithmeticLexerOperatorConfig
{
    /**
     * DivideArithmeticLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('/');
    }
}