<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Arithmetic;

/**
 * Subtract config
 */
final class SubtractArithmeticLexerOperatorConfig extends AbstractArithmeticLexerOperatorConfig
{
    /**
     * SubtractArithmeticLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('-');
    }
}