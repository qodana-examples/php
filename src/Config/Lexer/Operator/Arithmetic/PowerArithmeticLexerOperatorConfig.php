<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Arithmetic;

/**
 * Power config
 */
final class PowerArithmeticLexerOperatorConfig extends AbstractArithmeticLexerOperatorConfig
{
    /**
     * PowerArithmeticLexerOperatorConfig constructor
     */
    public function __construct()
    {
        parent::__construct('^');
    }
}