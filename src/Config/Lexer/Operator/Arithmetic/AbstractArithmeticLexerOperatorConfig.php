<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Operator\Arithmetic;

use LessPlate\Config\Lexer\Operator\AbstractLexerOperatorConfig;
use LessPlate\Lexer\Token\Operator\ArithmeticOperatorToken;
use LessPlate\Lexer\Token\Operator\OperatorToken;

/**
 * Base arithmetic config
 */
abstract class AbstractArithmeticLexerOperatorConfig extends AbstractLexerOperatorConfig
{
    /**
     * Make token for arithmetic
     *
     * @param string $sequence
     * @param int $line
     * @param int $column
     *
     * @return OperatorToken
     */
    public function makeToken(string $sequence, int $line, int $column): OperatorToken
    {
        return new ArithmeticOperatorToken(strtolower($sequence), $line, $column);
    }
}
