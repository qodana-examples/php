<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer;

use LessPlate\Config\Lexer\Operator\LexerOperatorConfig;
use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Config\Lexer\Tag\LexerTagConfig;

/**
 * Config for lexer
 */
interface LexerConfig
{
    /**
     * Returns the config for tags
     *
     * @return iterable|LexerTagConfig[]
     */
    public function getTagConfigs(): iterable;

    /**
     * Returns operators
     *
     * @return iterable|LexerOperatorConfig[]
     */
    public function getOperatorConfigs(): iterable;

    /**
     * Returns literals
     *
     * @return iterable|LexerSpecializedConfig[]
     */
    public function getSpecializedConfigs(): iterable;
}