<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer;

use LessPlate\Config\Lexer\Operator\LexerOperatorConfig;
use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Config\Lexer\Tag\LexerTagConfig;

/**
 * Adds the getters
 */
abstract class AbstractLexerConfig implements LexerConfig
{
    /**
     * Holds the logical operators
     *
     * @var array|LexerTagConfig[]
     */
    private $tagConfigs = [];
    /**
     * Holds the operators
     *
     * @var array|LexerOperatorConfig[]
     */
    private $operatorConfigs = [];
    /**
     * Holds the chain operators
     *
     * @var array|LexerSpecializedConfig[]
     */
    private $specializedConfigs = [];

    /**
     * AbstractLexerConfig constructor
     *
     * @param iterable|LexerTagConfig[] $tagConfigs
     * @param iterable|LexerOperatorConfig[] $operators
     * @param iterable|LexerSpecializedConfig[] $specialized
     */
    public function __construct(iterable $tagConfigs, iterable $operators, iterable $specialized)
    {
        foreach ($tagConfigs as $config) {
            $this->addTagConfig($config);
        }

        usort($this->tagConfigs, $this->getTagSorter());

        foreach ($operators as $config) {
            $this->addOperator($config);
        }

        usort($this->operatorConfigs, $this->getOperatorSorter());

        foreach ($specialized as $config) {
            $this->addSpecialized($config);
        }

        usort($this->specializedConfigs, $this->getSpecializedSorter());
    }

    /**
     * Returns sorter for tag config
     *
     * @return callable
     */
    protected function getTagSorter(): callable
    {
        return static function (LexerTagConfig $left, LexerTagConfig $right) {
            return strlen($right->getStartSequence()) <=> strlen($left->getStartSequence());
        };
    }

    /**
     * Returns sorter for operator configs
     *
     * @return callable
     */
    protected function getOperatorSorter(): callable
    {
        return static function (LexerOperatorConfig $left, LexerOperatorConfig $right) {
            return strlen($right->getSequence()) <=> strlen($left->getSequence());
        };
    }

    /**
     * Returns sorter for specialized config
     *
     * @return callable
     */
    protected function getSpecializedSorter(): callable
    {
        return static function (LexerSpecializedConfig $left, LexerSpecializedConfig $right) {
            return strlen($right->getSequence()) <=> strlen($left->getSequence());
        };
    }

    /**
     * Adds the tag config
     *
     * @param LexerTagConfig $config
     *
     * @return void
     */
    protected function addTagConfig(LexerTagConfig $config): void
    {
        $this->tagConfigs[] = $config;
    }

    /**
     * Adds the arithmetic operator
     *
     * @param LexerOperatorConfig $operator
     *
     * @return void
     */
    protected function addOperator(LexerOperatorConfig $operator): void
    {
        $this->operatorConfigs[] = $operator;
    }

    /**
     * Adds the chain operator
     *
     * @param LexerSpecializedConfig $config
     *
     * @return void
     */
    protected function addSpecialized(LexerSpecializedConfig $config): void
    {
        $this->specializedConfigs[] = $config;
    }

    /**
     * Returns tag configs
     *
     * @return iterable|LexerTagConfig[]
     */
    public function getTagConfigs(): iterable
    {
        return $this->tagConfigs;
    }

    /**
     * Returns arithmetic operators
     *
     * @return iterable|LexerOperatorConfig[]
     */
    public function getOperatorConfigs(): iterable
    {
        return $this->operatorConfigs;
    }

    /**
     * Returns chain operators
     *
     * @return iterable|LexerSpecializedConfig[]
     */
    public function getSpecializedConfigs(): iterable
    {
        return $this->specializedConfigs;
    }
}