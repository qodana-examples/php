<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer;

use LessPlate\Config\Lexer\Operator\LexerOperatorConfig;
use LessPlate\Config\Lexer\Specialized\Accessor;
use LessPlate\Config\Lexer\Specialized\Group;
use LessPlate\Config\Lexer\Specialized\LexerSpecializedConfig;
use LessPlate\Config\Lexer\Tag\LexerTagBlockConfig;
use LessPlate\Config\Lexer\Tag\LexerTagConfig;
use LessPlate\Config\Lexer\Tag\LexerTagExpressionConfig;

/**
 * Default lexer config
 */
final class DefaultLexerConfig extends AbstractLexerConfig
{
    /**
     * DefaultLexerConfig constructor
     */
    public function __construct()
    {
        parent::__construct(
            self::getDefaultTagConfigs(),
            self::getDefaultOperatorConfigs(),
            self::getDefaultSpecializedConfigs()
        );
    }

    /**
     * Returns default tag configs
     *
     * @return iterable|LexerTagConfig[]
     */
    public static function getDefaultTagConfigs(): iterable
    {
        return [
            new LexerTagExpressionConfig(),
            new LexerTagBlockConfig(),
        ];
    }

    /**
     * Returns default operators
     *
     * @return iterable|LexerOperatorConfig[]
     */
    public static function getDefaultOperatorConfigs(): iterable
    {
        return [
            new Operator\Arithmetic\AdditionArithmeticLexerOperatorConfig(),
            new Operator\Arithmetic\DivideArithmeticLexerOperatorConfig(),
            new Operator\Arithmetic\ModuloArithmeticLexerOperatorConfig(),
            new Operator\Arithmetic\MultiplyArithmeticLexerOperatorConfig(),
            new Operator\Arithmetic\PowerArithmeticLexerOperatorConfig(),
            new Operator\Arithmetic\SubtractArithmeticLexerOperatorConfig(),

            new Operator\Chain\AndChainLexerOperatorConfig(),
            new Operator\Chain\OrChainLexerOperatorConfig(),

            new Operator\Logical\EqualsLogicalLexerOperatorConfig(),
            new Operator\Logical\GreaterThanLogicalLexerOperatorConfig(),
            new Operator\Logical\GreaterThanOrEqualsLogicalLexerOperatorConfig(),
            new Operator\Logical\ContainsLogicalLexerOperatorConfig(),
            new Operator\Logical\LowerThanLogicalLexerOperatorConfig(),
            new Operator\Logical\LowerThanOrEqualsLogicalLexerOperatorConfig(),
            new Operator\Logical\NotEqualsLogicalLexerOperatorConfig(),
            new Operator\Logical\NotContainsLogicalLexerOperatorConfig(),
        ];
    }

    /**
     * Returns default specialized config
     *
     * @return iterable|LexerSpecializedConfig[]
     */
    public static function getDefaultSpecializedConfigs(): iterable
    {
        return [
            new Accessor\LexerSpecializedAccessorCloseConfig(),
            new Accessor\LexerSpecializedAccessorConfig(),
            new Accessor\LexerSpecializedAccessorStartConfig(),

            new Group\LexerSpecializedGroupCloseConfig(),
            new Group\LexerSpecializedGroupStartConfig(),

            new Specialized\LexerSpecializedConcatConfig(),
            new Specialized\LexerSpecializedOptionalConfig(),
            new Specialized\LexerSpecializedTransformerConfig(),
        ];
    }
}
