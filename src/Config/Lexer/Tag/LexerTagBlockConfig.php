<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Tag;

use LessPlate\Lexer\Token\Tag\Block\TagBlockCloseToken;
use LessPlate\Lexer\Token\Tag\Block\TagBlockStartToken;
use LessPlate\Lexer\Token\Tag\TagCloseToken;
use LessPlate\Lexer\Token\Tag\TagStartToken;

/**
 * Config for block tag
 */
final class LexerTagBlockConfig extends AbstractLexerTagConfig
{
    public const START_SEQUENCE = '{%';
    public const CLOSE_SEQUENCE = '%}';

    /**
     * LexerTagBlockConfig constructor
     *
     * @param string $openSequence
     * @param string $closeSequence
     */
    public function __construct(?string $openSequence = null, ?string $closeSequence =  null)
    {
        parent::__construct(
            $openSequence ?? self::START_SEQUENCE,
            $closeSequence ?? self::CLOSE_SEQUENCE
        );
    }

    /**
     * Makes the block open token
     *
     * @param int $line
     * @param int $column
     *
     * @return TagStartToken
     */
    public function makeStartToken(int $line, int $column): TagStartToken
    {
        return new TagBlockStartToken($this->getStartSequence(), $line, $column);
    }

    /**
     * Makes the block close token
     *
     * @param int $line
     * @param int $column
     *
     * @return TagCloseToken
     */
    public function makeCloseToken(int $line, int $column): TagCloseToken
    {
        return new TagBlockCloseToken($this->getCloseSequence(), $line, $column);
    }
}