<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Tag;

use LessPlate\Lexer\Token\Tag\TagCloseToken;
use LessPlate\Lexer\Token\Tag\TagStartToken;

/**
 * Config for a lexer tag
 */
interface LexerTagConfig
{
    /**
     * Returns sequence for opening the tag
     *
     * @return string
     */
    public function getStartSequence(): string;

    /**
     * Make the open token
     *
     * @param int $line
     * @param int $column
     *
     * @return TagStartToken
     */
    public function makeStartToken(int $line, int $column): TagStartToken;

    /**
     * Returns sequence for closing the tag
     *
     * @return string
     */
    public function getCloseSequence(): string;

    /**
     * Make the close token
     *
     * @param int $line
     * @param int $column
     *
     * @return TagCloseToken
     */
    public function makeCloseToken(int $line, int $column): TagCloseToken;
}