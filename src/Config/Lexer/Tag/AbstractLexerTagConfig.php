<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Tag;

/**
 * Adds the sequence to the config
 */
abstract class AbstractLexerTagConfig implements LexerTagConfig
{
    /**
     * Open sequence
     *
     * @var string
     */
    private $openSequence;
    /**
     * Close sequence
     *
     * @var string
     */
    private $closeSequence;

    /**
     * AbstractLexerTagConfig constructor
     *
     * @param string $openSequence
     * @param string $closeSequence
     */
    public function __construct(string $openSequence, string $closeSequence)
    {
        $this->openSequence = $openSequence;
        $this->closeSequence = $closeSequence;
    }

    /**
     * Returns the open sequence
     *
     * @return string
     */
    public function getStartSequence(): string
    {
        return $this->openSequence;
    }

    /**
     * Returns the close sequence
     *
     * @return string
     */
    public function getCloseSequence(): string
    {
        return $this->closeSequence;
    }
}