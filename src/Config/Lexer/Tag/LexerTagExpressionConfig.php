<?php
declare(strict_types=1);

namespace LessPlate\Config\Lexer\Tag;

use LessPlate\Lexer\Token\Tag\Expression\TagExpressionCloseToken;
use LessPlate\Lexer\Token\Tag\Expression\TagExpressionStartToken;
use LessPlate\Lexer\Token\Tag\TagCloseToken;
use LessPlate\Lexer\Token\Tag\TagStartToken;

/**
 * Config for expression tag
 */
final class LexerTagExpressionConfig extends AbstractLexerTagConfig
{
    public const START_SEQUENCE = '{{';
    public const CLOSE_SEQUENCE = '}}';

    /**
     * LexerTagBlockConfig constructor
     *
     * @param string $openSequence
     * @param string $closeSequence
     */
    public function __construct(?string $openSequence = null, ?string $closeSequence =  null)
    {
        parent::__construct(
            $openSequence ?? self::START_SEQUENCE,
            $closeSequence ?? self::CLOSE_SEQUENCE
        );
    }

    /**
     * Makes the expression open token
     *
     * @param int $line
     * @param int $column
     *
     * @return TagStartToken
     */
    public function makeStartToken(int $line, int $column): TagStartToken
    {
        return new TagExpressionStartToken($this->getStartSequence(), $line, $column);
    }

    /**
     * Makes the expression close token
     *
     * @param int $line
     * @param int $column
     *
     * @return TagCloseToken
     */
    public function makeCloseToken(int $line, int $column): TagCloseToken
    {
        return new TagExpressionCloseToken($this->getCloseSequence(), $line, $column);
    }
}