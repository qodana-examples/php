<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Logical;

use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Expression\Logical\NotEqualsLogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Not equals config
 */
final class NotEqualsParserLogicalConfig implements ParserLogicalConfig
{
    /**
     * Returns sequence to use
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '!=';
    }

    /**
     * Make token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return LogicalExpressionToken
     */
    public function makeToken(Token $left, Token $right): LogicalExpressionToken
    {
        return new NotEqualsLogicalExpressionToken($left, $right);
    }
}