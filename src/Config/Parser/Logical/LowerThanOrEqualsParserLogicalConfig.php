<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Logical;

use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Expression\Logical\LowerThanOrEqualsLogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Lower than or equals config
 */
final class LowerThanOrEqualsParserLogicalConfig implements ParserLogicalConfig
{
    /**
     * Returns the sequence to match on
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '<=';
    }

    /**
     * Makes the token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return LogicalExpressionToken
     */
    public function makeToken(Token $left, Token $right): LogicalExpressionToken
    {
        return new LowerThanOrEqualsLogicalExpressionToken($left, $right);
    }
}