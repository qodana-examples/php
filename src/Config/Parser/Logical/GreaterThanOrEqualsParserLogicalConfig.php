<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Logical;

use LessPlate\Parser\Token\Expression\Logical\GreaterThanOrEqualsLogicalExpressionToken;
use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Greater than or equals config
 */
final class GreaterThanOrEqualsParserLogicalConfig implements ParserLogicalConfig
{
    /**
     * Sequence to match on
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '>=';
    }

    /**
     * Make token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return LogicalExpressionToken
     */
    public function makeToken(Token $left, Token $right): LogicalExpressionToken
    {
        return new GreaterThanOrEqualsLogicalExpressionToken($left, $right);
    }
}