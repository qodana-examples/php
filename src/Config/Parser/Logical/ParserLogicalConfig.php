<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Logical;

use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Config for logical
 */
interface ParserLogicalConfig
{
    /**
     * Returns sequence to match on
     *
     * @return string
     */
    public function getSequence(): string;

    /**
     * Makes the parser logical token
     *
     * @param Token $left
     * @param Token $right
     *
     * @return LogicalExpressionToken
     */
    public function makeToken(Token $left, Token $right): LogicalExpressionToken;
}