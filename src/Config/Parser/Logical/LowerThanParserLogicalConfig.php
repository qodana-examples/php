<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Logical;

use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Expression\Logical\LowerThanLogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Lower than config
 */
final class LowerThanParserLogicalConfig implements ParserLogicalConfig
{
    /**
     * Returns sequence to match
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '<';
    }

    /**
     * Makes token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return LogicalExpressionToken
     */
    public function makeToken(Token $left, Token $right): LogicalExpressionToken
    {
        return new LowerThanLogicalExpressionToken($left, $right);
    }
}