<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Logical;

use LessPlate\Parser\Token\Expression\Logical\GreaterThanLogicalExpressionToken;
use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Greater than config
 */
final class GreaterThanParserLogicalConfig implements ParserLogicalConfig
{
    /**
     * Returns sequence to match on
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '>';
    }

    /**
     * Makes token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return LogicalExpressionToken
     */
    public function makeToken(Token $left, Token $right): LogicalExpressionToken
    {
        return new GreaterThanLogicalExpressionToken($left, $right);
    }
}