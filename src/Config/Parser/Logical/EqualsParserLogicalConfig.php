<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Logical;

use LessPlate\Parser\Token\Expression\Logical\EqualsLogicalExpressionToken;
use LessPlate\Parser\Token\Expression\Logical\LogicalExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Equals config
 */
final class EqualsParserLogicalConfig implements ParserLogicalConfig
{
    /**
     * Returns sequence to use
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '==';
    }

    /**
     * Makes token
     *
     * @param Token $left
     * @param Token $right
     *
     * @return LogicalExpressionToken
     */
    public function makeToken(Token $left, Token $right): LogicalExpressionToken
    {
        return new EqualsLogicalExpressionToken($left, $right);
    }
}