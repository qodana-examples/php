<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Chain;

use LessPlate\Parser\Token\Expression\Chain\OrLogicalExpressionToken;

/**
 * Or logical chain config
 */
final class OrParserChainConfig extends AbstractParserChainConfig
{
    /**
     * OrChainLogicalConfig constructor
     */
    public function __construct()
    {
        parent::__construct(
            '||',
            OrLogicalExpressionToken::class,
            50
        );
    }
}