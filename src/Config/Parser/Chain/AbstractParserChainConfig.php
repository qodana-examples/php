<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Chain;

use LessPlate\Parser\Token\Expression\Chain\ChainExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Base setup
 */
abstract class AbstractParserChainConfig implements ParserChainConfig
{
    /**
     * Sequence to match on
     *
     * @var string
     */
    private $sequence;
    /**
     * Class for the token
     *
     * @var string
     */
    private $tokenClass;
    /**
     * Holds the priority
     *
     * @var int
     */
    private $priority;

    /**
     * AbstractChainLogicalConfig constructor
     *
     * @param string $sequence
     * @param string $tokenClass
     * @param int $priority
     */
    public function __construct(string $sequence, string $tokenClass, int $priority)
    {
        $this->sequence = $sequence;
        $this->tokenClass = $tokenClass;
        $this->priority = $priority;
    }

    /**
     * Returns sequence from AbstractChainLogicalConfig
     *
     * @return string
     */
    public function getSequence(): string
    {
        return $this->sequence;
    }

    /**
     * Makes token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return ChainExpressionToken
     */
    public function makeToken(Token $left, Token $right): ChainExpressionToken
    {
        $class = $this->tokenClass;

        return new $class($left, $right);
    }

    /**
     * Returns priority from AbstractChainLogicalConfig
     *
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
}