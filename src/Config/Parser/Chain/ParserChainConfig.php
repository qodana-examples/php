<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Chain;

use LessPlate\Parser\Token\Expression\Chain\ChainExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Config for chain logical
 */
interface ParserChainConfig
{
    /**
     * Returns sequence to match on
     *
     * @return string
     */
    public function getSequence(): string;

    /**
     * Makes token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return ChainExpressionToken
     */
    public function makeToken(Token $left, Token $right): ChainExpressionToken;

    /**
     * Returns priority of the operation
     *
     * @return int
     */
    public function getPriority(): int;
}