<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Chain;

use LessPlate\Parser\Token\Expression\Chain\AndLogicalExpressionToken;

/**
 * And logical chain config
 */
final class AndParserChainConfig extends AbstractParserChainConfig
{
    /**
     * AndChainLogicalConfig constructor
     */
    public function __construct()
    {
        parent::__construct(
            '&&',
            AndLogicalExpressionToken::class,
            100
        );
    }
}