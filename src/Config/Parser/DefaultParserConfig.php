<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser;

use LessPlate\Config\Parser\Chain\AndParserChainConfig;
use LessPlate\Config\Parser\Chain\OrParserChainConfig;
use LessPlate\Lexer\Token\Tag\Block\TagBlockStartToken;
use LessPlate\Lexer\Token\Tag\Expression\TagExpressionStartToken;
use LessPlate\Parser\Tag\Block\DelegateTagBlockParser;
use LessPlate\Parser\Tag\Block\ForInTagBlockParser;
use LessPlate\Parser\Tag\Block\IfTagBlockParser;
use LessPlate\Parser\Tag\Block\ScopeTagBlockParser;
use LessPlate\Parser\Tag\Expression\DefaultTagExpressionParser;
use LessPlate\Parser\Tag\TagParser;
use LessPlate\Parser\Util\ExpressionParser;
use LessPlate\Parser\Util\UtilParser;
use LessPlate\Parser\Util\ValueParser;

/**
 * Default parser config
 */
final class DefaultParserConfig implements ParserConfig
{
    /**
     * Returns default tag parsers
     *
     * @return array|TagParser[]
     */
    public function getTagParsers(): array
    {
        return [
            TagExpressionStartToken::NAME => new DefaultTagExpressionParser(),
            TagBlockStartToken::NAME => new DelegateTagBlockParser(
                [
                    'if' => new IfTagBlockParser(),
                    'for' => new ForInTagBlockParser(),
                    'scope' => new ScopeTagBlockParser(),
                ]
            ),
        ];
    }

    /**
     * Return parser for expressions
     *
     * @return UtilParser
     */
    public function getExpressionParser(): UtilParser
    {
        return new ExpressionParser(
            new ValueParser(),
            [
                new AndParserChainConfig(),
                new OrParserChainConfig(),
            ],
            [
                new Logical\ContainsParserLogicalConfig(),
                new Logical\EqualsParserLogicalConfig(),
                new Logical\GreaterThanParserLogicalConfig(),
                new Logical\GreaterThanOrEqualsParserLogicalConfig(),
                new Logical\LowerThanOrEqualsParserLogicalConfig(),
                new Logical\LowerThanParserLogicalConfig(),
                new Logical\NotEqualsParserLogicalConfig(),
                new Logical\NotContainsParserLogicalConfig(),
            ],
            [
                new Arithmetic\AdditionParserArithmeticConfig(),
                new Arithmetic\DivideParserArithmeticConfig(),
                new Arithmetic\ModuloParserArithmeticConfig(),
                new Arithmetic\MultiplyParserArithmeticConfig(),
                new Arithmetic\PowerParserArithmeticConfig(),
                new Arithmetic\SubtractParserArithmeticConfig(),
            ]
        );
    }
}
