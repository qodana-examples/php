<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser;

use LessPlate\Parser\Tag\TagParser;
use LessPlate\Parser\Util\UtilParser;

/**
 * Config for parser
 */
interface ParserConfig
{
    /**
     * Returns parsers to use for tags
     *
     * @return array|TagParser[]
     */
    public function getTagParsers(): array;

    /**
     * Returns parser to use for expression
     *
     * @return UtilParser
     */
    public function getExpressionParser(): UtilParser;
}