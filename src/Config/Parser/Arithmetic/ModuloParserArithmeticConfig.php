<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\ArithmeticExpressionToken;
use LessPlate\Parser\Token\Expression\Arithmetic\ModuloArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Modulo config
 */
final class ModuloParserArithmeticConfig implements ParserArithmeticConfig
{
    /**
     * Returns sequence to match on
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '%';
    }

    /**
     * Makes token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return ArithmeticExpressionToken
     */
    public function makeToken(Token $left, Token $right): ArithmeticExpressionToken
    {
        return new ModuloArithmeticExpressionToken($left, $right);
    }

    /**
     * Priority of operation
     *
     * @return int
     */
    public function getPriority(): int
    {
        return 75;
    }
}