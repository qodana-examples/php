<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\AdditionArithmeticExpressionToken;
use LessPlate\Parser\Token\Expression\Arithmetic\ArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Config for add
 */
final class AdditionParserArithmeticConfig implements ParserArithmeticConfig
{
    /**
     * Returns sequence to use
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '+';
    }

    /**
     * Makes token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return ArithmeticExpressionToken
     */
    public function makeToken(Token $left, Token $right): ArithmeticExpressionToken
    {
        return new AdditionArithmeticExpressionToken($left, $right);
    }

    /**
     * Returns priority
     *
     * @return int
     */
    public function getPriority(): int
    {
        return 25;
    }
}