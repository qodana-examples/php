<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\ArithmeticExpressionToken;
use LessPlate\Parser\Token\Expression\Arithmetic\PowerArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Power config
 */
final class PowerParserArithmeticConfig implements ParserArithmeticConfig
{
    /**
     * Returns sequence to match on
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '^';
    }

    /**
     * Creates token
     *
     * @param Token $left
     * @param Token $right
     *
     * @return ArithmeticExpressionToken
     */
    public function makeToken(Token $left, Token $right): ArithmeticExpressionToken
    {
        return new PowerArithmeticExpressionToken($left, $right);
    }

    /**
     * Returns priority of operation
     *
     * @return int
     */
    public function getPriority(): int
    {
        return 100;
    }
}