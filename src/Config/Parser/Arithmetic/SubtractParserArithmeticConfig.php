<?php
declare(strict_types=1);

namespace LessPlate\Config\Parser\Arithmetic;

use LessPlate\Parser\Token\Expression\Arithmetic\ArithmeticExpressionToken;
use LessPlate\Parser\Token\Expression\Arithmetic\SubtractArithmeticExpressionToken;
use LessPlate\Parser\Token\Token;

/**
 * Subtract config
 */
final class SubtractParserArithmeticConfig implements ParserArithmeticConfig
{
    /**
     * Sequence to match on
     *
     * @return string
     */
    public function getSequence(): string
    {
        return '-';
    }

    /**
     * Makes token to use
     *
     * @param Token $left
     * @param Token $right
     *
     * @return ArithmeticExpressionToken
     */
    public function makeToken(Token $left, Token $right): ArithmeticExpressionToken
    {
        return new SubtractArithmeticExpressionToken($left, $right);
    }

    /**
     * Arithmetic priority
     *
     * @return int
     */
    public function getPriority(): int
    {
        return 25;
    }
}