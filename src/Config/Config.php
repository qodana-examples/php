<?php
declare(strict_types=1);

namespace LessPlate\Config;

use LessPlate\Config\Executor\ExecutorConfig;
use LessPlate\Config\Lexer\LexerConfig;
use LessPlate\Config\Parser\ParserConfig;

/**
 * Base config
 */
interface Config
{
    /**
     * Returns executor config
     *
     * @return ExecutorConfig
     */
    public function getExecutorConfig(): ExecutorConfig;

    /**
     * Returns lexer config
     *
     * @return LexerConfig
     */
    public function getLexerConfig(): LexerConfig;

    /**
     * Returns parser config
     *
     * @return ParserConfig
     */
    public function getParserConfig(): ParserConfig;
}