<?php
declare(strict_types=1);

namespace LessPlate\Config;

use LessPlate\Config\Executor\ExecutorConfig;
use LessPlate\Config\Lexer\LexerConfig;
use LessPlate\Config\Parser\ParserConfig;

/**
 * Construct setup
 */
abstract class AbstractConfig implements Config
{
    /**
     * Executor config
     *
     * @var ExecutorConfig
     */
    private $executorConfig;
    /**
     * Lexer config
     *
     * @var LexerConfig
     */
    private $lexerConfig;
    /**
     * Parser config
     *
     * @var ParserConfig
     */
    private $parserConfig;

    /**
     * AbstractConfig constructor
     *
     * @param ExecutorConfig $executorConfig
     * @param LexerConfig $lexerConfig
     * @param ParserConfig $parserConfig
     */
    public function __construct(ExecutorConfig $executorConfig, LexerConfig $lexerConfig, ParserConfig $parserConfig)
    {
        $this->executorConfig = $executorConfig;
        $this->lexerConfig = $lexerConfig;
        $this->parserConfig = $parserConfig;
    }

    /**
     * Returns executorConfig from AbstractConfig
     *
     * @return ExecutorConfig
     */
    public function getExecutorConfig(): ExecutorConfig
    {
        return $this->executorConfig;
    }

    /**
     * Set executorConfig on AbstractConfig
     *
     * @param ExecutorConfig $executorConfig
     *
     * @return $this
     */
    protected function setExecutorConfig(ExecutorConfig $executorConfig)
    {
        $this->executorConfig = $executorConfig;

        return $this;
    }

    /**
     * Returns lexerConfig from AbstractConfig
     *
     * @return LexerConfig
     */
    public function getLexerConfig(): LexerConfig
    {
        return $this->lexerConfig;
    }

    /**
     * Set lexerConfig on AbstractConfig
     *
     * @param LexerConfig $lexerConfig
     *
     * @return $this
     */
    protected function setLexerConfig(LexerConfig $lexerConfig)
    {
        $this->lexerConfig = $lexerConfig;

        return $this;
    }

    /**
     * Returns parserConfig from AbstractConfig
     *
     * @return ParserConfig
     */
    public function getParserConfig(): ParserConfig
    {
        return $this->parserConfig;
    }

    /**
     * Set parserConfig on AbstractConfig
     *
     * @param ParserConfig $parserConfig
     *
     * @return $this
     */
    protected function setParserConfig(ParserConfig $parserConfig)
    {
        $this->parserConfig = $parserConfig;

        return $this;
    }
}