<?php
declare(strict_types=1);

namespace LessPlate;

use LessPlate\Context\Context;
use LessPlate\Input\Input;

/**
 * LessPlate Renderer
 */
interface Renderer
{
    /**
     * Renders the input with the given context
     *
     * @param Input $input
     * @param Context|null $context
     *
     * @return string
     */
    public function render(Input $input, ?Context $context = null): string;
}